/* Nandakumar Edamana
 * 15 Jul 2017
 * Restarted for libdecom on 07 May 2019
 */

// TODO rem unused
#include <errno.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

int sockfd;

// TODO declare in separate header file
void nancached_serve(int sockfd);
void nancached_sigkill_handler(int n);

int main(int argc, char **argv) {
	int portno = 9696;
	int newsockfd;

	/* TODO FIXME */
	if(argc == 2) {
		/* TODO FIXME validate */
		sscanf(argv[1], "%d", &portno);
	}

	struct sockaddr_in sockaddr = {
		AF_INET,
		htons(portno),
		INADDR_ANY
//		INADDR_LOOPBACK
	};
		//INADDR_LOOPBACK /* Only 127.0.0.1 is allowed as client */ // TODO FIXME this is what I need, but it causes bind() to fail, UNLESS I'm root (sometimes even I'm root?).

	struct sockaddr_in sockaddr_ret;
	socklen_t socklen_ret;

	pid_t pid;

	/* TODO FIXME use sigaction() instead? See the manpage of signal */
	signal(SIGINT, nancached_sigkill_handler);
	
	/* I found this on 2019-05-08 by reading the man pages */
	struct protoent *pent;
	pent = getprotobyname("tcp"); // TODO err

	/* Open the socket */
	sockfd = socket(AF_INET, SOCK_STREAM, pent->p_proto); /* TCP socket */
	if(sockfd == -1) {
		fprintf(stderr, "ERROR: socket() failed.\n");
		exit(-1);
	}

	printf("port = %d\n", portno); // TODO REM or debug-only
	
	/* Bind with the port */
	if(-1 == bind(sockfd, (struct sockaddr *) &sockaddr, sizeof(sockaddr))) {
		fprintf(stderr, "ERROR: bind() failed.\n");
		exit(-1);
	}

	/* Enable listening for connection requests */
	listen(sockfd, 5); /* TODO FIXME second param */

	while(1) {
		/* Extract the first request in the connection queue */
		newsockfd = accept(sockfd, (struct sockaddr *) &sockaddr_ret, &socklen_ret);
		if(newsockfd == -1) {
			fprintf(stderr, "ERROR: accpet() failed. errno = %d\n", errno);
			exit(-1);
		}

		printf("newsockfd = %d\n", newsockfd); // TODO REM or debug-only

		/* TODO FIXME use sigaction() instead? See the manpage of signal */
		signal(SIGCHLD, SIG_IGN); /* TODO FIXME move? */
		pid = fork();
		if(pid == 0) { /* Child */
			close(sockfd); // TODO why?
			nancached_serve(newsockfd);
			exit(0);
		}
		else {
			close(newsockfd);
		}
		
		// TODO sleep some microseconds?
	}

	return 0;
}

void nancached_serve(int sockfd) {
	// TODO
	write(sockfd, "Hello", 5);
}

void nancached_sigkill_handler(int n) {
printf("Kill!\n");
	/* If not closed properly, bind() will fail in the next execution after a Ctrl + C */
	/* TODO FIXME Do not close if the client is still waiting. See http://www.serverframework.com/asynchronousevents/2011/01/time-wait-and-its-design-implications-for-protocols-and-scalable-servers.html */
	close(sockfd);

	exit(0);
}
