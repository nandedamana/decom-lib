<?php
/* This file is part of libdecom.
 * Copyright (C) 2018, 2019 Nandakumar Edamana
 * Started on 2018-11-14
 */

/**
 * @h1 Person
 */

// TODO move this file to php/

require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/entity.php');

/**
 * @h2 DecomPerson class
 */
class DecomPerson extends DecomEntity {
	/**
	 * @notice This doesn't create a new person in the database. Use decom_create_entity() for that.
	 * @nonExisting
	 */
	function __construct($id, $nonExisting = false, $loadData = true) {
		parent::__construct('person', $id, $nonExisting, $loadData);
	}

	function getName() {
  	/* TODO load from the db if not pre-loaded? */
    return $this->getPropertyValue('name');
  }

  /* TODO */
}
?>
