<?php
/* This file is part of libdecom.
 * Copyright (C) 2018 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 * Started on 2018-12-31
 */

/**
 * @h1 Authentication
 */

require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/error.php');

class DecomAuthSuccess {
	public $ownerClass;
	public $ownerEid;
}

/**
 * @retobjerr DecomAuthSuccess
 * Use <code class="codeInline">is_a($retval, 'DecomError')</code> to check for error.
 */
function decom_auth($uname, $passwd) {
	global $DEDBLINK;

	$r = decom_db_query_single_row(
		'select * from login where uname='.$DEDBLINK->quote($uname));

	if(decom_is_errobj($r))
		return $r;

	if($r === false)
	  return new DecomError('No such user.');
  
  if($r['passwd'] == hash($r['halgo'], $passwd.$r['salt'])) {
		$retobj = new DecomAuthSuccess;
		$retobj->ownerClass = $r['owner_class'];
		$retobj->ownerEid   = $r['owner_eid'];
  
  	return $retobj;
  }
  else {
	  return new DecomError('Invalid credentials.');
	}
}
?>
