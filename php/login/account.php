<?php
/* This file is part of libdecom.
 * Copyright (C) 2018, 2019 Nandakumar Edamana
 * Moved from person.php on 2019-12-06
 */

/**
 * @h1 Login Account Management
 */

require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/entity.php');

/**
 * Functions
 */

/**
 * Assigns a username and password to an existing entity.
 * @param id Entity ID (currently only of the Person class)
 * @param uname Username
 * @param passwd Password (use plaintext; hashing happens internally)
 * @param className Which class the entity belongs to
 * @reterrtrue
 */
function decom_create_login($id, $uname, $passwd, $className = 'person') {
	global $DEDBLINK;
	
	// TODO set loadData = false?
	$obj = new DecomEntity($className, $id);
	
	$ret = $obj->hasLogin();
	if(decom_is_errobj($ret))
		return $ret;
	else if($ret === true)
		return new DecomError('Entity already has a login account.');
	
	// TODO make strong passwd required?
	$uname     = $DEDBLINK->quote($uname); // TODO validate uname
	$className = $DEDBLINK->quote($className);
	$id        = $DEDBLINK->quote($id);

	$ret = decom_db_exec(
		'insert into login(uname, owner_class, owner_eid, halgo) values('."$uname, $className, $id, 'ripemd160')"); // TODO default login algo - db level?
	if(decom_is_errobj($ret))
		return $ret;
	
	return decom_login_set_password($uname, $passwd, false, false);
}

// TODO remove login fun

// TODO doc quoteUname - for performance enhancement (lowlevel use only)
function decom_login_exists($uname, $quoteUname = true) {
	global $DEDBLINK;

	if($quoteUname)
		$uname = $DEDBLINK->quote($uname);

 	return (decom_db_query_single_field(
 		'select count(*) from login where uname='.$uname) != 0);
}

// TODO move?
// TODO doc: checkUnameExists, quoteUname - for performance enhancement (lowlevel use only)
function decom_login_set_password($uname, $passwd, $checkUnameExists = true, $quoteUname = true, $salt = null, $halgo = 'ripemd160') {
	global $DEDBLINK;

	// TODO make strong passwd required?

	$unameq = ($quoteUname)?
		$DEDBLINK->quote($uname):
		$uname;
	
	if($checkUnameExists)
		/* Interesting case: regardless of quoteUname given to this fun,
		 * quoteUname for decom_login_exists() could always be false (THINK).
		 */
		if(!decom_login_exists($uname, false))
			return new DecomError('Username does not exist');
	
	if($salt === null)
		$salt = substr(hash('md5', rand()), 0, 8);
	
	$passwdq = $DEDBLINK->quote(hash($halgo, $passwd.$salt));
	// TODO validate $halgo against a permitted list instead.
	$halgoq = $DEDBLINK->quote($halgo);
	$saltq  = $DEDBLINK->quote($salt);

	return decom_db_exec(
		"update login set halgo=$halgoq, passwd=$passwdq, salt=$saltq where uname=$unameq");
}
?>
