<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-16
 */

/**
 * @h1 Serve Private Files
 */

require_once($DELIBDIR.'/php/entity.php');

/**
 * Serve a private file of an entity, including the HTTP headers.
 * @notice This function does not perform any session check or authentication, although it disables cache.
 */
// TODO check the database entry?
// TODO use mime from the db? - XXX UNSAFE because the user may update the file without updating the DB
function decom_serve_private_file($className, $eid, $filename) {
	header('Cache-control: no-store');
	
	if(true !== decom_entity_private_file_exists($className, $eid, $filename)) {
		http_response_code(404);
		exit;
	}
	
	$fpath = decom_entity_get_private_file_real_path($className, $eid, $filename);
	if(decom_is_errobj($fpath)) {
		// TODO write the error info in log
	
		http_response_code(500); // 403 doesn't fit here, since this is an internal issue
		exit;
	}
	
	$mimeType = mime_content_type($fpath);
	if($mimeType === false) {
		// TODO write the reason info in log
	
		http_response_code(500);
		exit;	
	}
	
	header("Content-Type: $mimeType");
	
	$fp = fopen($fpath, 'r');
	if($fp === false) {
		// TODO write the reason info in log
	
		http_response_code(500); // 403 doesn't fit here, since this is an internal issue
		exit;
	}

	while( ($buf = fread($fp, 4096)) )
		print($buf); // XXX This works (even for binary); but is it always binary safe?
	
	fclose($fp);
}
?>
