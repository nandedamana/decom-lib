<?php
/* utility.php
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-05, with code moved from some of my other files
 */

/**
 * @h1 Nandakumar Utility Functions
 */

/**
 * Converts a Boolean input to Yes or No
 * @param useSpan Whether or not to wrap the Yes/No text in a span with class 'good' or 'bad' (for colours)
 * @param invert true means No is good and Yes is bad
 */
function nan_bool_to_yesno($bool, $useSpan = true, $invert = false) {
  if($useSpan) {
  	if($invert)
			return $bool? '<span class="bad">Yes</span>': '<span class="good">No</span>';
		else
			return $bool? '<span class="good">Yes</span>': '<span class="bad">No</span>';
	}
	else {
		return $bool? 'Yes': 'No';
	}
}

/**
 * Automatically generates a title-case label from code-like names (e.g.: of an attribute).
 * This can be used to make your forms more attractive if you generate them on-the-go.
 */
function nan_labelify($name) {
	// TODO check if I can use ucwords()

	$words    = explode('_', $name);
	$wordsNew = [];
	foreach($words as $w) {
		if(!in_array($w, ['a', 'an', 'and', 'at', 'in', 'of', 'on', 'the', 'with'])) // TODO make sure the list is complete
			$w = strtoupper(substr($w, 0, 1)).substr($w, 1); // Capitalize

		$wordsNew[] = $w;
	}
	
	return implode(' ', $wordsNew);
}


function nan_val_stringify($val, $onNull = '<b>NULL</b>', $onEmptyString = '<i>empty string</i>', $onFalse = '<b>FALSE</b>', $onTrue = '<b>TRUE</b>') {
	if($val === null)
		return $onNull;

	if($val === '')
		return $onEmptyString;

	if($val === true)
		return $onTrue;

	if($val === false)
		return $onFalse;
	
	return $val;
}

function nan_val_or_none($val, $onEmpty = '<b>None</b>', $onFalse = '<b>None</b>', $onTrue = '<b>TRUE</b>') {
	return nan_val_stringify($val, $onEmpty, $onEmpty, $onFalse, $onTrue);
}
?>
