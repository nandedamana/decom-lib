<?php
/* table.php
 * Copyright (C) 2018, 2019 Nandakumar Edamana
 * Started on 2018-12-30
 */

/**
 * @h1 Nandakumar HTML Table Generator Functions
 */

/**
 * Converts a PDO ResultSet to HTML table
 * @param callback If set, this function will be called for each row with the row as the argument (your function should accept it as reference so that the function can modify it).
 */
function nan_pdo_rs_to_html_table($rs, $colsToDisplay, $colHeadings = null, $callback = null) {
	$html = nan_table_start();

	if($colHeadings === null)
		$colHeadings = $colsToDisplay;

	$html .= nan_table_start();
	$html .= nan_table_array_to_th($colHeadings);

	foreach($rs as $r) {
		if($callback !== null)
			$callback($r);

		$html .= nan_table_assoc_array_to_td($r, $colsToDisplay);
	}

	$html .= nan_table_close();
	
	return $html;
}

function nan_table_array_to_tdth_list($arr, $tagOpen, $tagClose) {
	$html = '<tr>';
	foreach($arr as $i)
		$html .= $tagOpen.$i.$tagClose;

	return $html.'</tr>';
}

function nan_table_array_to_td($arr) {
	return nan_table_array_to_tdth_list($arr, '<td>', '</td>');
}

function nan_table_array_to_th($arr) {
	return nan_table_array_to_tdth_list($arr, '<th>', '</th>');
}

function nan_table_assoc_array_to_td($arr, $selKeys = null) {
	$vals = [];

	if($selKeys)
		foreach($selKeys as $k)
			$vals[] = $arr[$k];
	else
		foreach($arr as $k => $v)
			$vals[] = $arr[$v];

	return nan_table_array_to_tdth_list($vals, '<td>', '</td>');
}

function nan_table_close() {
	return '</table>';
}

function nan_table_start() {
	return '<table>';
}
?>
