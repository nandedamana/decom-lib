<?php
/* Nandakumar Edamana
 * Started on 20 Jul 2017
 * Some important changes:
 *  2019-12-12 - quote escaping of default text values
 */

/**
 * @h1 Nandakumar Form Generator
 */

/* TODO doc __submit */
function nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method = 'POST', $customInput = array(), $defaultValues = [], $extraFormAttribs = '', $prependFormContent = '', $appendFormContent = '') {
	$html = '';

	/* $customInput - array of fields having type = 'custom' (actual HTML form type given by the caller in $customInput[$customCount]
	 * $defaultValues - An associative array with $inputFields members as keys.
	                    For checkboxes, the value could only be '' or 'checked'
	 */
	$customCount = 0;

	$html .= "<form class=\"nanForm nanFormWideLabels\" action=\"$action\" method=\"$method\" $extraFormAttribs >$prependFormContent<div class=\"divTable\">";
	foreach($inputFields as $i => $inputField) {
		$inputFieldLabel = $inputFieldLabels[$i];
		$inputFieldType = $inputFieldTypes[$i];
		$inputFieldReq = $inputFieldsReq[$i];

		$req = '';
		if($inputFieldReq) {
			$req = ' required';
			$inputFieldLabel = $inputFieldLabel.'*';
		}

		$defaultValue = isset($defaultValues[$inputField])?
			$defaultValues[$inputField]:
			'';

		$hidattr = '';
		if($inputFieldType == 'hidden')
			$hidattr = ' style="display: none;"';

		$html .= "<div class=\"divTableRow\"$hidattr><div class=\"divTableCell\" style=\"vertical-align: top\">";
		if($inputFieldType != 'checkbox') {
			$html .= "<label for=\"in_$inputField\">$inputFieldLabel: </label>";
		}
		
		$html .= "</div><div class=\"divTableCell\" style=\"padding-bottom: 0.2em;\">";
		
		if($inputFieldType == 'custom') {
			$html .= $customInput[$customCount ++];
		}
		else if($inputFieldType == 'textarea') {
			$html .= "<textarea id=\"in_$inputField\" name=\"$inputField\" cols=50>".htmlspecialchars($defaultValue).'</textarea>';
		}
		else if($inputFieldType == 'ynrads') {
			$ychecked = ($defaultValue)  ? 'checked': '';
			$nchecked = (!$defaultValue) ? 'checked': '';
		
			$html .= "<input type=\"radio\" id=\"in_y_$inputField\" name=\"$inputField\" value=\"1\" $ychecked/>".
				"<label for=\"in_y_$inputField\">Yes</label>".
				"<input type=\"radio\" id=\"in_n_$inputField\" name=\"$inputField\" value=\"0\" $nchecked/>".
				"<label for=\"in_n_$inputField\">No</label>";
		}
		else {
			$html .= "<input id=\"in_$inputField\" name=\"$inputField\" type=$inputFieldType $req ".
			( ($inputFieldType != 'checkbox')?
				'value = "'.str_replace('"', '&quot', $defaultValue).'"':
				"value=\"1\" $defaultValue" ).
			' />';
				
			if($inputFieldType == 'checkbox') {
				$html .= "<label for=\"in_$inputField\">$inputFieldLabel</label>";
			}
		}
		$html .= '</div></div>';
	}
	
	$subval = 'Submit';
	if(isset($defaultValues['__submit']))
		$subval = htmlspecialchars($defaultValues['__submit']);
	
	$html .= "</div>$appendFormContent<hr/><input type=\"submit\" name=\"__submit\" value=\"$subval\">";
	$html .= '</form>';
	
	return $html;
}
?>
