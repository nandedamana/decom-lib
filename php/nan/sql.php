<?php
/* Nandakumar Edamana
 * Started on 11 Jun 2018
 */

/**
 * @h1 Nandakumar SQL Utility Functions
 */

/**
 * @param arr A 1D array having the elements colname, coltype, nullable, default (or null), unique
 */
function nan_sql_colspec_from_colrec($colr) {
	return $colr[0].' '.$colr[1].' '.
		(($colr[2] === false)? ' not null': '').
		((isset($colr[4]) && $colr[4] == true)? ' unique': '').
		(($colr[3] !== null)?  (' default '.$colr[3]): '');
}

/**
 * @param arr A 2D array in which each element is an array having the elements colname, coltype, nullable, default (or null), unique
 */
function nan_sql_collist_from_colarr($arr) {
	$colsqls = [];
	
	foreach($arr as $colr) // colr - column record
		$colsqls[] = nan_sql_colspec_from_colrec($colr);

	return implode(',', $colsqls);
}
?>
