<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-03-22
 */

/**
 * @h1 Database
 */

require_once($DELIBDIR.'/php/internal/flags.php');
require_once($DELIBDIR.'/php/error.php');

$DEDBNOCONNSTR = 'No database connection';

/**
 * Executes decom_db_query_single_row($sql) and returns the first element
 * @param sql An SQL SELECT query
 */
function decom_db_query_single_field($sql) {
	$row = decom_db_query_single_row($sql);
	if(decom_is_errobj($row) || $row === false)
		return $row;

	return $row[0];
}

// TODO doc for all funs

/**
 * @reterrtrue
 */
function decom_db_exec($sql) {
	global $DEDBLINK, $DEDBNOCONNSTR;

	if($DEDBLINK === null)
		return new DecomError($DEDBNOCONNSTR);

	$ret = $DEDBLINK->exec($sql);
	if($ret === false || $DEDBLINK->errorCode() != '00000') // ANSI SQL-92 SQLSTATE
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);

	return true;
}

/**
 * Returns the PDOStatement object
 * @reterr
 */
function decom_db_query($sql) {
	global $DEDBLINK, $DEDBNOCONNSTR;

	if($DEDBLINK === null)
		return new DecomError($DEDBNOCONNSTR);

	$rs = $DEDBLINK->query($sql);

	if($DEDBLINK->errorCode() != '00000') // ANSI SQL-92 SQLSTATE // TODO is this right?
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);

	return $rs;
}

/**
 * Executes the query and returns the first row from the result set as an array. Returns false if no row was selected or an object of DecomError if there was a query error.
 * @param sql An SQL SELECT query
 */
function decom_db_query_single_row($sql) {
	global $DEDBLINK, $DEDBNOCONNSTR;

	if($DEDBLINK === null)
		return new DecomError($DEDBNOCONNSTR);

	$rs = $DEDBLINK->query($sql);
	
	if($DEDBLINK->errorCode() != '00000') // ANSI SQL-92 SQLSTATE
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);
	
	if(false === $rs) {
		return false;
	}

	return $rs->fetch();
}

/**
 * Executes the query and returns a 2D array in case col = null, otherwise a 1D array. Returns false if no row was selected or an object of DecomError if there was a query error.
 * @param sql An SQL SELECT query
 */
// TODO doc: returns 2D array or vertical 1D array?
function decom_db_query_array($sql, $col = null) {
	global $DEDBLINK, $DEDBNOCONNSTR;

	if($DEDBLINK === null)
		return new DecomError($DEDBNOCONNSTR);
	
	$rs = $DEDBLINK->query($sql);
	
	if($DEDBLINK->errorCode() != '00000') // ANSI SQL-92 SQLSTATE
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);
	
	if(false === $rs) {
		return false;
	}
	
	$arr = array();
	foreach($rs as $r) {
		array_push($arr, ($col === null)? $r: $r[$col]);
	}
	
	return $arr;
}

/* TODO remove this as one can use decom_db_query_array() directly? */
function decom_db_query_single_col_as_array($sql, $col) {
	return decom_db_query_array($sql, $col);
}

/**
 * Returns an array in which is item is an assoc array with members
 * 'class' and 'parent'.
 */
function decom_db_get_classes_raw() {
	return decom_db_query_array('select class, parent from classes');
}

/**
 * Returns a list containing the names of all classes.
 */
function decom_db_get_class_names() {
	return decom_db_query_array('select distinct class from classes', 'class');
}

/**
 * Returns TRUE if the table exists, FALSE otherwise.
 * @notecompoptrTF
 * @reterr
 */
function decom_db_table_exists($tableName) {
	global $DEDBLINK, $DEDBNAME, $DEDBNOCONNSTR, $DEDBDRIVER;

	if($DEDBLINK === null)
		return new DecomError($DEDBNOCONNSTR);


	switch($DEDBDRIVER) {
	case DEDBDRV_MYSQL:
		$ret = decom_db_query_single_field(
			'select count(*) from information_schema.tables where table_schema='.$DEDBLINK->quote($DEDBNAME).
			' and table_name='.$DEDBLINK->quote($tableName));
		break;
	case DEDBDRV_SQLITE:
		$ret = decom_db_query_single_field(
			'select count(*) from sqlite_master where name='.
			$DEDBLINK->quote($tableName).
			' and type="table"');
		break;
	default:
		throw new Exception('TODO implement db driver support.'); // TODO common erro?
	}

	if(decom_is_errobj($ret))
		return $ret;

	return ($ret != 0);	
}

/* TODO function to check and reorder classes returned by decom_db_get_classes_raw() so that parents appear first and there are no orphan/dangling items. */

/**
 * Quotes the given eid checking flags.
 */
function decom_quote_eid_if_needed($eid) {
	global $DEDBLINK;

	if(deflag_check('noquote_eid'))
		return $eid;
	else
		return $DEDBLINK->quote($eid);
}
?>
