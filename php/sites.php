<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-22
 */

/**
 * @h1 Sites
 */

require_once($DELIBDIR.'/php/db.php');

/**
 * Returns an array of ids of all sites
 */
function decom_get_site_ids() {
	return decom_db_query_single_col_as_array('select distinct id from sites', 'id');
}
?>
