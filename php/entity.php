<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-03-29
 */

require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/limits.php');
require_once($DELIBDIR.'/php/internal/flags.php');
require_once($DELIBDIR.'/php/utility.php');

/**
 * @h1 Entity
 */

/**
 * @h2 DecomEntity Class
 */

// TODO make sure the construct param $nonExisting prevents DB reads
class DecomEntity {
	protected $class;
	protected $cobj;
	protected $id;
	protected $kvp = null; /* Attributes, Relationships, and Inverse relationships */

	/**
	 * Returns the ids of all instances of the specified class.
	 */
	public static function getIds($class) {
		return decom_db_query_single_col_as_array('select distinct id from eav_'.$class, 'id');
	}

	/**
	 * @nonExisting
	 * $ignoreOrphanAttribs has been doced for loadKvp()
	 */
	function __construct($class, $id = null, $nonExisting = false, $loadData = true, $ignoreOrphanAttribs = true) {
		global $DEDBLINK; /* TODO Remove after moving to prepared statement */

		// TODO validate classname

		if(!isset($class)) {
			throw new Exception('Entity class not specified'); // TODO return DecomError?
			// TODO check clas existence here?
		}
		
		$this->class = $class;
		$this->cobj  = new DecomClass($class);

		if($id !== null) {
			// TODO write and use a std. validation function
			if(is_object($id) || !preg_match('/^[0-9]+$/', $id))
				throw new Exception('Invalid object ID.');
			
			$this->id = intval($id);
		}
		else if(!$nonExisting) { // id is null, but $nonExisting is set to false
			throw new Exception('Object ID not given.');
		}

		if(!$nonExisting) {
			// TODO better way than count()?
			$ret = decom_db_query_single_field("select count(*) from eav_$class where id = ".$DEDBLINK->quote($this->id));

			if(is_a($ret, 'DecomError'))
				throw new Exception($ret->getMessageHtml());
			else if($ret == 0)
				throw new Exception('Object with ID '.$this->id.' doesn\'t exist for the class '.$class);
		}

		if($id !== null) {
			$this->id = $id; // TODO validate

			if($loadData)
				$this->loadKvp($ignoreOrphanAttribs);
		}
		
		/* TODO */
	}

	/* TODO REMOVE getAttrib* funs since they are handled by DecomClass? */
	function getAttribIdFromAttribName($name) {
		global $DEDBLINK;

		return decom_db_query_single_field(
			'select id from eav_ameta_'.$this->class.
			' where name = '.$DEDBLINK->quote($name));
	}
	
	function getAttribNameFromAttribId($id) {
		global $DEDBLINK;

		return decom_db_query_single_field(
			'select name from eav_ameta_'.$this->class.
			' where id = '.$DEDBLINK->quote($id));
	}
	
	/**
	 * Returns an array containing the IDs of all children.
	 */
	// TODO FIXME retval
	function getChildrenIds() {
		return decom_db_query_array(
			'select child from hier_'.$this->class.' where parent='.$this->id, 'child');
	}
	
	/**
	 * Returns an array containing the IDs of all children.
	 */	
	function getClassName() {
		return $this->class;
	}

	function getId() {
		return $this->id;
	}

	/**
	 * @reterr
	 * @retnullifnoval
	 */
	// TODO doc
	function getParentId($errorIfLoop = true) {
		return decom_entity_get_parent($this->class, $this->id, $errorIfLoop);
	}

	/**
	 * Returns a single value in case only one value is allowed for the given attribute,
	 * otherwise (multi-valued attribute) an array, even if only one value is set.
	 * One can check the type of the return value using the PHP built-in function is_array().
	 * @paramAttrib
	 * @paramAttribIsNameTrue
	 * @param depth If not found in this class, how many ancestors should be searched; 0 for none, negative values for infinite (default 0)
	 * @retfalseifnovalsincenullisval
	 */
  function getPropertyValue($attrib, $attribIsName = true, $depth = 0) {
		$key = decom_attrib_name($this, $attrib, $attribIsName);
		if(isset($this->kvp[$key])) {
	  	return $this->kvp[$key];
		}
		else { /* Not set for this object */
			if($depth == 0) {
				return false; // Returns FALSE because NULL can be a value
			}
			else if ( null !== ($parid = $this->getParentId()) ) { // XXX !== is a must
				$pobj = new DecomEntity($this->class, $parid);
				// XXX `$depth - 1` is nice, but may theoretically cause integer underflow for negative (infinite) $depth (not an issue for most practical cases)
				return $pobj->getPropertyValue($attrib, $attribIsName, $depth - 1);
			}
			else {
				return false; // Returns FALSE because NULL can be a value
			}
		}
  }
  
  /**
   * Returns the first value in case there are multiple values for a property.
   * It is safe to assume that the value returned by this method is not an array.
	 * @paramAttrib
	 * @paramAttribIsNameTrue
	 * @retfalseifnovalsincenullisval
   */
  function getPropertyValueFirst($attrib, $attribIsName = true) {
		$ret = $this->getPropertyValue($attrib, $attribIsName);

		if(is_array($ret))
	  	 $ret[0];
	  else
	  	return $ret;
  }

	// TODO why 'public'? what is 'public`?
  function getPublicPropertyNames() {
  	$pptys = array();

  	// TODO FIXME check which is public
  	
  	foreach($this->kvp as $key => $value) {
  		array_push($pptys, $key);
  	}
  	
  	return $pptys;
  }

  // TODO doc, move
  function getInverseRelative($rshipName) {
  	// TODO use attrib ID for efficiency?
  	
  	$irobj = $this->cobj->getInverseRelationship($rshipName);
  	$relatedClassName = $irobj->getFromClassName();
  	
		$relatedIds = decom_get_entity_ids_by_property_value(
			$relatedClassName, $irobj->toAttributeName(), $this->id);

  	return $relatedIds;
  }
  
  // TODO doc, move
  function getInverseRelativeFirst($rshipName) {
  	$ret = $this->getInverseRelative($rshipName);

  	if(is_array($ret))
  		return $ret[0];
  	else
  		return $ret;
  }

	/**
	 * Returns a single value in case only one value is allowed for the given relationship,
	 * otherwise an array, even if only one value is set.
	 * One can check the type of the return value using the PHP built-in function is_array().
	 * @param rshipName Name of the relationship
	 * @retnullifnoval
	 */
  function getRelative($rshipName) {
  	$rship = $this->cobj->getRelationship($rshipName);
  	if(decom_is_errobj($rship))
  		return $rship;
  	else if($rship === false)
  		return new DecomError('Invalid relationship name: '.$rshipName); // TODO details?
  
  	return $this->getPropertyValue($rship->toAttributeName());
  }

	/**
   * Returns the first value in case there are multiple values for a relationship.
   * It is safe to assume that the value returned by this method is not an array.
	 * @param rshipName Name of the relationship
	 * @retnullifnoval
	 */
  function getRelativeFirst($rshipName) {
  	$ret = $this->getRelative($rshipName);

  	if(is_array($ret))
  		return $ret[0];
  	else
  		return $ret;
  }

  function getUsername() {
		global $DEDBLINK;

  	return decom_db_query_single_field(
  		"select uname from login where owner_class=".
  		$DEDBLINK->quote($this->class).
  		" and owner_eid=".
  		$DEDBLINK->quote($this->id));
  }

  function hasLogin() {
		global $DEDBLINK;
  	
  	$ret = decom_db_query_single_field(
  		"select count(*) from login where owner_class=".
  		$DEDBLINK->quote($this->class).
  		" and owner_eid=".
  		$DEDBLINK->quote($this->id));
  	
  	if(decom_is_errobj($ret))
  		return $ret;
  	
  	return (intval($ret) > 0);
  }

  /**
   * Returns true if the object has value for the given attribute.
   * @param attrib Attribute name/ID or an object of DecomAttribute or DecomRelationship.
   * @param attribIsName Whether the parameter attrib was name (default: true)
   */
	function hasPropertyValue($attrib, $attribIsName = true) {
		// TODO esure the data has been loaded
		$key = decom_attrib_name($this, $attrib, $attribIsName);

		return isset($this->kvp[$key]);
		
		/* TODO FIXME the following code is to be used if the data is not loaded already, but it is not correct yet, I think.
		global $DEDBLINK;
		
		$aid = $attrib;
		if($attribIsName)
			$aid = $this->getAttribIdFromAttribName($attrib);

		return 0 != decom_db_query_single_field(
			'select count(*) from eav_'.$this->class.
			' where id = '.$DEDBLINK->quote($this->id).
			' and attrib = '.$DEDBLINK->quote($aid));
		*/
	}
	
	// TODO FIXME implement
  // function hasRelative($rshipName) {
		
	//}

	// TODO hasInverseRelative

	/**
	 * (Re)loads attribute values from the database.
	 * Returns TRUE on success, an object of DecomError on error, FALSE if the object has not properties set.
	 * @param ignoreOrphanAttribs If set true, orphan attributes (attributes which are present in the object, but not in the class) will not cause errors. Default is true.
	 */
	function loadKvp($ignoreOrphanAttribs = true) {
		// TODO cache these; no need to reload data for each object of a class BUT... caching attrib names from an entity won't do -- not all entities will have values for all attribs. So the attrib names should be retrieved from eav_ameta_...
		
		global $DEDBLINK;
		
		$rs = $DEDBLINK->query('select * from eav_'.$this->class.' where id = '.$DEDBLINK->quote($this->id));
		if(FALSE === $rs) {
			return FALSE;
		}

		$cobj = new DecomClass($this->class);

		$this->kvp = array();

// TODO FIXME DecomEntity: use attrib, rship names from DecomClass; do not reload and recalculate.
// TODO most of this processing is done in DecomClass.
		foreach($rs as $r) {
			$attrib = $cobj->getAttribute($r['attrib'], false); /* TODO cache? */
			if($attrib === false) { /* Class doesn't have such an attribute */
				if($ignoreOrphanAttribs)
					continue;
				else
					return new DecomError($this->class.' ('.$this->id.') has an attribute named '.$r['attrib'].' which its class doesn\'t have.');
			}
			
			$aname  = $attrib->toAttributeName();
			$val    = ($r['ival'] != null)? $r['ival']: $r['tval'];

			if($attrib->getMaxInstances() == 1) {
				$this->kvp[$aname] = $val;
			}
			else {
				if(isset($this->kvp[$aname])) // The array has already been initialized.
					$this->kvp[$aname][] = $val;
				else                             // This is the first instance
					$this->kvp[$aname] = [$val];
			}
		}
		
		return true;
	}
}

/**
 * @h2 Functions
 */

// TODO use stock doc for attribIsName
/**
 * Creates an object in the database for a given class.
 * @param className Name of the class whose object has to be created
 * @param props A two-dimensional array, in which each row represents a key-value pair
 * @param attribIsName Whether the key is attribute name (otherwise it can be attribute ID)
 * @param eid Explicit entity ID. Do not use unless you are sure what you are doing.
 * @param dryrun Do not make any real changes (helps to detect errors safely)
 * @note It is always recommended to use attribute name instead of ID. Although this can be an overhead, it is more consistent (think about concurrent updates).
 * Returns entity ID on success, an object of DecomError on failure.
 */
// TODO doc empty prop array not possible
function decom_create_entity($className, $props, $attribIsName = true, $eid = null, $dryrun = false) {
	global $DEDBLINK;
	global $DERELATIONAL;

	if(isset($DERELATIONAL))
		return redecom_create_entity($className, $props); // TODO support other params

	// TODO validate $className

	$propsOkay = is_array($props) && count($props);
	if($propsOkay) {
		foreach($props as $r)
			if(!is_array($r) || count($r) != 2)
				$propsOkay = false;
	}

	if(!$propsOkay)
		return new DecomError('props has to be a 2D array.');

	$cobj = new DecomClass($className); // TODO check retval? or there is exception?

	$sqlKvpListInt  = [];
	$sqlKvpListText = [];

	foreach($props as $r) {
		if($attribIsName)
			$k = $cobj->getAttribIdFromAttribName($r[0]);
		else
			$k = $r[0];

		if($k === null || $k === false) // TODO find which one
			return new DecomError('Non-existing attribute: '.$r[0]);

		$attrib = $cobj->getAttribute($k, false);

		$maxinst = $attrib->getMaxInstances();
		// TODO test all cases
		if($maxinst == 1) {
			if(is_array($r[1]))
				new DecomError('Multiple values given for the single-instance attribute \''.$r[0].'\''); // TODO doc
			else
				$varr = [$r[1]];
		}
		else { // Multi-instance attrib
			if(!is_array($r[1]))
				$varr = [$r[1]]; // Autoconvertion to array TODO doc this
			else
				$varr = $r[1];
			
			if($maxinst != 0 && sizeof($varr) > $maxinst)
				return new DecomError('More than '.$maxinst.' values given for the attribute \''.$r[0].'\'');
		}

		$vinst = 0; // Value instance
		foreach($varr as $v) {
			if($attrib->getType() == 'int') {
				if(trim($v) == '')
					/* I originally used the following for this case:
					 *   $v = 'NULL';
					 * I'm using a continue instead because this function is also called by for decom_edit_entity()
					 * (I gives null to delete an instance).
					 * Although object editing will be moved from a delete-add-again sequence, the following line of code
					 * remains better since there is no point in using NULL in a multi-instance field.
					 */
					continue;
				else if(!preg_match('/[0-9]+/', $v))
					return new DecomError('Integer value expected for the attribute "'.$cobj->getAttribNameFromAttribId($k).'"');
				else
					$v = $DEDBLINK->quote($v);

				$sqlKvpListInt[] = "(@newId, $k, $v, $vinst)";
			}
			else {
				$v = $DEDBLINK->quote($v);
				$sqlKvpListText[] = "(@newId, $k, $v, $vinst)";
			}

			$vinst += 1;
		}
	}

	if($dryrun)
		return true;

	// TODO FIXME
	/* XXX Multi-statement SQL executions doesn't return errors correctly.
	 * Perform in multiple steps.
	 */

	if($eid === null)
		$sql = 'select (select ifnull(max(id), -1) from eav_'.$className.') + 1 into @newId; ';
	else
		$sql = 'select '.$eid.' into @newId; ';

	if(sizeof($sqlKvpListInt))
		$sql .= 'insert into eav_'.$className.' (id, attrib, ival, value_instance) values '.implode(',', $sqlKvpListInt).'; ';

	if(sizeof($sqlKvpListText))
		$sql .= 'insert into eav_'.$className.' (id, attrib, tval, value_instance) values '.implode(',', $sqlKvpListText).';';

	$DEDBLINK->exec($sql);
	if($DEDBLINK->errorCode() != 0)
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);

	if($eid !== null) {
		return $eid;
	}
	else {
		$newId = decom_db_query_single_field('select @newId');
		if($newId === false) // TODO does decom_db_query_single_field() return DecomError?
			return new DecomError('Database Error: couldn\'t select @newId');

		return $newId;
	}
}

/**
 * @param eid Entity ID
 * Returns entity ID on success.
 * @reterr
 */
function decom_edit_entity($className, $eid, $props, $attribIsName = true) {
	// TODO FIXME this function just deletes the current object and adds new one. It is not the right way to do it. Think about integrity checks while deletion.
	// TODO valid input args

	/* Dry run */
	$ret = decom_create_entity($className, $props, $attribIsName = true, $eid, true);
	if($ret !== true)
		return $ret;

	$ret = decom_remove_entity($className, $eid);
	if(decom_is_errobj($ret))
		return $ret;
	
	return decom_create_entity($className, $props, $attribIsName = true, $eid);
}

// TODO doc filename cannot contain '..' even like in 'a..b'
// TODO doc filename cannot contain directory components yet
// TODO doc legal chars
// TODO $destFileName maxlen - currently 255
// TODO test symlink
/**
 * @param srcFileName Source filename; you can use $_FILES[$index]['tmp_name'] if you are using form-based file upload
 * @param destFileName The name to store the file with
 * @reterrtrue
 */
function decom_entity_add_private_file($className, $eid, $destFileName, $srcFileName, $overwrite = false, $symlink = false) {
	global $DEDBLINK;

	/* Efficient validations first */
	if( (false !== strpos($destFileName, '..')) ||
		  (1 != preg_match('/^[a-zA-Z0-9\.\-\_]+$/', $destFileName)) )
		return new DecomError('Illegal filename');

	if( strlen($destFileName) > DEMAXLEN_PRIVFILENAME)
		return new DecomError('Filename too long');

	if(!is_file($srcFileName) || !is_readable($srcFileName))
		return new DecomError('Source file does not exist or is unreadable');

	/* Slow validations next */
	$ret = decom_entity_exists($className, $eid); // Also validates className and eid
	if(decom_is_errobj($ret))
		return $ret;

	if(true !== $ret)
		return new DecomError('Entity does not exist');

	/* className and eid have been validated by decom_entity_exists() */

	$eidq = decom_quote_eid_if_needed($eid);

	deflag_set('novalidate_classname');
	deflag_set('novalidate_eid');
	deflag_set('noquote_eid');
	$exists = decom_entity_private_file_exists($className, $eid, $destFileName);
	deflag_rollback('noquote_eid');
	deflag_rollback('novalidate_eid');
	deflag_rollback('novalidate_classname');

	if($exists && !$overwrite)
		return new DecomError('File already exists');

	$relpath = decom_entity_get_private_files_directory($className, $eid); // XXX Safe because $className and $eid are already verified.

	$ret = decom_mkdir_r($relpath);
	if($ret !== true)
		return new DecomError('Error creating directory: '.$relpath);

	$destPath = $relpath.'/'.$destFileName;

	if(!$exists || $overwrite) {
		if($symlink) { // TODO perfrom platform check
			if(!symlink($destPath, $srcFileName))
				return new DecomError('Error symlinking file');
		}
		else {
			if(!copy($srcFileName, $destPath))
				return new DecomError('Error copying file');
		}
	}

	$ret = decom_db_create_class_implementation_table($className, 'privfiles', true);
	if(decom_is_errobj($ret)) {
		// TODO FIXME remove the file in this case
	
		return $ret;
	}

	$destFileNameQ = $DEDBLINK->quote($destFileName);

	$ret = decom_db_exec(
		"replace into privfiles_$className (eid, filename)".
		"values ($eidq, $destFileNameQ)");

	if(decom_is_errobj($ret)) {
		// TODO FIXME remove the file in this case
		return $ret;
	}

	return true;
}

/**
 * @reterr
 */
function decom_entity_exists($className, $eid) {
	global $DEDBLINK;

	if(deflag_is_false('novalidate_classname'))
		if(true !== decom_validate_class_name($className))
			return new DecomError('Invalid class name');

	if(deflag_is_false('novalidate_eid'))
		if(true !== decom_validate_entity_id($eid))
			return new DecomError('Invalid entity ID');

	$eidq = decom_quote_eid_if_needed($eid);

	$ret = decom_db_query_single_field('select count(*) from eav_'.$className.' where id='.$eidq);
	if($ret === false || decom_is_errobj($ret))
		return $ret;

	return true;
}

/**
 * @reterr
 * @retnullifnoval
 */
function decom_entity_get_parent($className, $eid, $errorIfLoop = true) {
	global $DEDBLINK;
	
	// TODO validate className and eid

	$eidq = $DEDBLINK->quote($eid); // TODO use prepared statement?

	$par = decom_db_query_single_field("select parent from hier_$className where child=$eidq");
	if(decom_is_errobj($par))
		return $par;
	else if($par === false)
		return null;

	if($errorIfLoop)
		if(intval($par) == intval($eid))
			return new DecomError('Object with ID '.$eid.' of the class '.$className.' has itself as its parent.');
	
	return $par;
}

// TODO fun to remove privfile
// TODO when removing, make sure only the link is removed if it's a symlink file

// TODO doc not bothering to validate class name and eid because (1) this is mostly a low-level function and (2) this function does not perform any writes
function decom_entity_get_private_file_real_path($className, $eid, $filename) {
	global $DEPROJDIR;
	
	return
		decom_entity_get_private_files_directory($className, $eid).'/'.$filename;
}

// TODO doc not bothering to validate class name and eid because (1) this is mostly a low-level function and (2) this function does not perform any writes
function decom_entity_get_private_files_directory($className, $eid) {
	global $DEPROJDIR;
	
	return decom_project_subdir_get_fullpath(DEPROJ_RELPATH_PRIVFILES.'/'.$className.'/'.$eid);
}

/**
 * Returns true if the file exists and false if not. Returns an object of DecomError if there is a database error or there is an anomaly (like a file is pointed by the database, but not found on the dis).
 * @reterr
 */
function decom_entity_private_file_exists($className, $eid, $filename, $checkInDisk = true) {
	if(deflag_is_false('novalidate_classname'))
		if(true !== decom_validate_class_name($className))
			return new DecomError('Invalid class name');
	
	if(deflag_is_false('novalidate_eid'))
		if(true !== decom_validate_entity_id($eid))
			return new DecomError('Invalid entity ID');

	// TODO check in the DB
	// $eidq = decom_quote_eid_if_needed($eid);

	if($checkInDisk) {
		$fpath =
			decom_entity_get_private_file_real_path($className, $eid, $filename);

		/* XXX file_exists() instead of is_file() for safety (in case the user is permitted to store directories in future). */
		if(!file_exists($fpath))
			return false;
	}
	
	return true;
}

// TODO doc $parentId = null to remove parent
function decom_entity_set_parent($className, $eid, $parentId) {
	// TODO check object existence
	// TODO validate className
	global $DEDBLINK;
	
	// XXX nullcheck is important (think about $eid = 0)
	if(($parentId !== null) && (intval($eid) == intval($parentId)))
		return new DecomError('Cannot set one object as its own parent');
	
	foreach([$eid, $parentId] as $id) {
		$ret = decom_entity_exists($className, $id);
		if($ret === false)
			return new DecomError('Object doesn\'t exist');
		else if(decom_is_errobj($ret))
			return $ret;
	}

	/* For safety in case the col child was not set primary key. */
	$err = decom_db_exec(
		'delete from hier_'.$className.' where child='.
		$DEDBLINK->quote($eid));

	if(decom_is_errobj($err))
		return $err;

	if($parentId !== null)
		return decom_db_exec(
			'replace into hier_'.$className.' (parent, child) values('.
			$DEDBLINK->quote($parentId).
			', '.$DEDBLINK->quote($eid).')');
	else
		return decom_db_exec(
			'delete from hier_'.$className.' where child='.
			$DEDBLINK->quote($eid));
}

/**
 * @param attrib Attribute name
 * @param value An array for multi-valued attributes, even if only there is only one value.
 * @notice NULL as value will not unset the property. Use decom_entity_unset_property() for that.
 * @reterrtrue
 */
function decom_entity_set_property($className, $eid, $attrib, $value) {
	global $DEDBLINK;

	$sql = '';

	// TODO constraint checks
	// TODO check flag and avoid re-check

	if(false === decom_validate_class_name($className))
		return new DecomError('Invalid class name');	

	$cobj = new DecomClass($className);
	$aobj = $cobj->getAttribute($attrib);
	if(decom_is_errobj($aobj))
		return $aobj;
	
	if(!decom_is_value($aobj))
		return new DecomError('Invalid attribute');

	$aid  = $aobj->getId();
	$aidq = $DEDBLINK->quote($aid);
	$eidq = $DEDBLINK->quote($eid);

	if($aobj->getMaxInstances() != 1) { // Multivalued
		if(!is_array($value))
			return new DecomError('Array expected for multivalued attribute');
	
		$sql = "delete from eav_$className where id=$eidq and attrib=$aidq;";
		
		$sql .= 'insert into eav_'.$className.' (id, attrib, ';
		$sql .= ($aobj->getType() == 'int')? 'ival': 'tval';
		$sql .= ', value_instance) values ';
		
		$varrs = [];
		
		$vinst = 0;
		foreach($value as $v) {
			$vq = $DEDBLINK->quote($v);
			$varrs[] = "($eidq, $aidq, $vq, $vinst)";
			
			$vinst++;
		}
		
		$sql .= implode(',', $varrs).';';
	}
	else { // Single valued
		$vq = $DEDBLINK->quote($value);
		if($aobj->getType() == 'int') // Checking before the loop for efficiency
			$valset = "$vq, NULL";
		else
			$valset = "NULL, $vq";
		
		$sql .= "replace into eav_$className".
			'(id, attrib, ival, tval) values'.
			"($eidq, $aidq, $valset);";
	}

	return decom_db_exec($sql);
}

/**
 * @reterr
 */
// TODO doc attrib - name (TODO allow ID?)
function decom_entity_unset_property($className, $eid, $attrib) {
	global $DEDBLINK;

	// TODO constraint checks
	// TODO check flag and avoid re-check

	if(false === decom_validate_class_name($className))
		return new DecomError('Invalid class name');	

	$cobj = new DecomClass($className);
	$aobj = $cobj->getAttribute($attrib);
	if(decom_is_errobj($aobj))
		return $aobj;
	
	if(!decom_is_value($aobj))
		return new DecomError('Invalid attribute');

	$aid  = $aobj->getId();
	$aidq = $DEDBLINK->quote($aid);
	$eidq = $DEDBLINK->quote($eid);

	$sql = "delete from eav_$className where id=$eidq and attrib=$aidq;";

	return decom_db_exec($sql);
}

// TODO limit
// TODO move and rename as decom_class_get_entity_ids()?
/**
 * Returns an array of ids of all objects of the given class.
 * @reterr
 */
// TODO * @param $whereClause Array of components to be used for filtering. TODO FIXME still unimplemented.
// function decom_get_entity_ids($className, $whereClause = null) {
function decom_get_entity_ids($className) {
	global $DERELATIONAL;

	if(isset($DERELATIONAL))
		return redecom_get_entity_ids($className);

	// TODO validate $className
	
	$ids = decom_db_query_array('select distinct id from eav_'.$className, 'id');
	if($ids === false)
		return [];
	else if(decom_is_errobj($ids))
		return $ids;
	
	return $ids;
}

// TODO use stock doc for $attribIsName
function decom_get_entity_ids_by_property_value($className, $attrib, $value, $attribIsName = true) {
	$ids = decom_get_entity_ids($className); // TODO use whereClause once it is implemented.

	// TODO this is inefficient; can I query directly?

	if(is_array($ids) && sizeof($ids) > 0) {
		$finalIds = [];
		
		foreach($ids as $id) {
			$eobj = new DecomEntity($className, $id);

			if($eobj->hasPropertyValue($attrib, $attribIsName))
				if($eobj->getPropertyValue($attrib, $attribIsName) == $value)
					$finalIds[] = $id;
		}
		
		$ids = $finalIds;
	}
	
	return $ids;
}

/**
 * @param eid Entity ID
 * @reterrtrue
 */
function decom_remove_entity($className, $eid) {
	// TODO FIXME take care of constraints, keys, relationships, etc.
	global $DEDBLINK;
	
	// TODO valid input args
	// TODO use: $eidq = decom_quote_eid_if_needed($eid);
	$eid = $DEDBLINK->quote($eid);
	
	$sql = 'set foreign_key_checks=0;'.
		'delete from eav_'.$className.' where id = '.$eid.';'.
		'set foreign_key_checks=1;';
	$DEDBLINK->exec($sql);
	if($DEDBLINK->errorCode() != 0)
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);

	return true;
}

afteroodb:

// XXX Be really careful while changing the rules for entity ID. This function is used to prevent XSS, filename injection, etc.
function decom_validate_entity_id($eid) {
	return (1 == preg_match('/[0-9]+/', $eid)); // TODO allow -ve?
}
?>
