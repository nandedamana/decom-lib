<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-03
 */

/**
 * @h1 Navigator
 */

// TODO doc

class DecomNavigator {
	private $baseUrl;
	private $params;

	/* TODO doc you can pass $_GET to $params */
	function __construct($baseUrl = '', $params = array()) {	
		$this->baseUrl = $baseUrl;
		$this->params  = $params;
	}

	function setBaseUrl($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	/* TODO test TODO doc you can pass $_GET to $params */
	function setParameterArray($params) {
		$this->params = $params;
	}

	function setParameter($name, $value) {
		$this->params[$name] = $value;
	}

	function toUrl() {
		$url = $this->baseUrl;
		
		foreach($this->params as $key => $value) {
			$url .= ($url == '')? '?': '&';
			
			/* TODO urlencode */
			$url .= "$key=$value";
		}
		
		return $url;
	}
}
?>
