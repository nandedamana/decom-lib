<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-16
 */

/**
 * @h1 libdecom Internal Flags
 * @internal
 */

/*
 * Legal subscripts:
 *   novalidate_classname
 *   noquote_classname
 *   noquote_eid
 */

/* Stacks to hold the values so that they can be reset */
$_deflagStacks = [];

/**
 * @notice This function does not validate the flagname; this is not a security issue, but a pitfall.
 */
function deflag_check($flagname) {
	if(!isset($_deflagStacks[$flagname][0]))
		return false;
	
	return $_deflagStacks[$flagname][0];
}

function deflag_is_false($flagname) {
	return (false === deflag_check($flagname));
}

function deflag_is_true($flagname) {
	return (true === deflag_check($flagname));
}

/**
 * @notice This function does not validate the flagname; this is not a security issue, but a pitfall.
 */
function deflag_set($flagname, $value = true) {
	if(!isset($_deflagStacks[$flagname]))
		$_deflagStacks[$flagname] = [];
	
	array_unshift($_deflagStacks[$flagname], $value);
}

/**
 * @notice This function does not validate the flagname; this is not a security issue, but a pitfall.
 */
// XXX Not using the name 'reset' because the new value need not be false.
function deflag_rollback($flagname) {
	if(isset($_deflagStacks[$flagname]) && count($_deflagStacks[$flagname]) > 0)
		array_shift($_deflagStacks[$flagname]);
}
?>
