<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-01
 */

/**
 * @h1 libdecom Warnings
 * @internal
 */

function delib_warn_use_instead($newfeature, $oldfeature, $thing, $do = 'use') {
	$newfeature = htmlspecialchars($newfeature);
	$oldfeature = htmlspecialchars($oldfeature);
	
	echo "<p><b>libdecom Warning</b>: The $thing <i>$oldfeature</i> is deprecated. Please $do <i>$newfeature</i> instead. The old $thing could be removed from any future release. More details might be available in the documentation.</p>";
}

/**
 * @param newfile Path of the new file relative to $DELIBDIR/
 * @param oldfile Path of the depricated file relative to $DELIBDIR/
 */
function delib_warn_include_instead($newfile, $oldfile) {
	delib_warn_use_instead('$DELIBDIR/'.$newfile, '$DELIBDIR/'.$oldfile, 'file');
}
?>
