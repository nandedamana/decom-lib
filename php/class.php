<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-28
 * Code moved from schema.php: Started on 2019-04-16
 */

/**
 * @h1 Class
 */

require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/dbgen.php'); // For $DE_eavAmetaCols TODO large file? split?
require_once($DELIBDIR.'/php/error.php');
require_once($DELIBDIR.'/php/internal/warn.php');
require_once($DELIBDIR.'/php/nan/utility.php');

// TODO doc

abstract class DecomAttribConstraints {
	const None       = 0;
	const PrimaryKey = 1; // TODO primary attribute should be specified in the class, not in the attrib.
	/* TODO */
}

abstract class DecomAttribLockLevels {
	const Hardwired = 0; /* Can be changed by direct DB manipulation only */
	const Admin     = 1; /* Can be changed by admin using this API */
	/* TODO */
}

// TODO doc
class DecomAttribute {
	protected $className;
	protected $description;
	protected $displayLabel = null;
	protected $displayOrder = 0; // TODO make sure this defaults to a common default; use a global constant
	protected $id;
	protected $maxInstances;
	protected $name;
	protected $type; // Datatype
	protected $uniqueGroup = 0; // TODO move unique groups info to class?

	protected $requiredness;
	// TODO

	/**
	 * @param requireness 0 - can be unset, can be null; 1 - must be set, but can be null; 2 - must be set and cannot be null
	 */
	function __construct($id, $name, $type, $desc = '', $maxInstances = 1, $requiredness = 0, $className = null) {
		$this->id   = $id;
		$this->maxInstances = $maxInstances;
		$this->name = $name;
		$this->type = $type;
		$this->description = $desc;
		$this->requiredness = $requiredness;
		$this->className = $className;
	}
	
	function getClassName() {
		return $this->className;
	}

	function getConstraintUnique() {
		return $this->unique;
	}
	
	function getDescription() {
		return $this->desc;
	}
	
	function getId() {
		return $this->id;
	}

	function getDisplayLabel($autoLabel = false) {
		if($this->displayLabel !== null)
			return $this->displayLabel;
		
		if($autoLabel)
			return nan_labelify($this->name);
		else
			return $this->name;
	}

	function getDisplayOrder() {
		return $this->displayOrder;
	}

	function getMaxInstances() {
		return $this->maxInstances;
	}
	
	function getName() {
		return $this->name;
	}

	/**
	 * This is a low-level function mainly used by the Control Panel.
	 */
	function getPropertyValue($prop) {
		return $this->$prop;
	}

	function getIsRequired() {
		return ($this->requiredness > 1);
	}
	
	/**
	 * @experimental
	 */
	function getCanBeNull() {
		return ($this->requiredness < 2);
	}

	/**
	 * @experimental
	 */	
	function getCanBeUnset() {
		return ($this->requiredness < 1);
	}

	function getRequiredness() {
		return $this->requiredness;
	}

	function getType() {
		return $this->type;
	}
	
	/**
	 * @depricated
	 */
	function labelify() {
		delib_warn_use_instead('DecomAttribute::getDisplayLabel()', 'DecomAttribute::labelify()', 'method');
		return $this->getDisplayLabel(true);
	}
	
	// TODO doc doesn't persist
	function setConstraintUnique($uniqueGroup) {
		$this->unique = $uniqueGroup;
	}

	function setDisplayLabel($label) {
		$this->displayLabel = $label;
	}
	
	function setDisplayOrder($order) {
		$this->displayOrder = $order;
	}
	
	/**
	 * Returns the low-level attribute name, which is same as name for a regular attribute
	 */	
	function toAttributeName() {
		return $this->name;
	}
}

// TODO doc: Cardinality X..Y means $required = (X == 1) and $maxInstances = Y.
class DecomRelationship extends DecomAttribute {
	private $toClassName;
	
	function __construct($id, $name, $desc = '', $maxInstances = 1, $requiredness = 0, $fromClassName = null, $toClassName = null) {
		parent::__construct($id, $name, 'int', $desc, $maxInstances, $requiredness, $fromClassName);
		$this->toClassName = $toClassName;
	}

	function getFromClassName() {
		return $this->className;
	}
	
	function getToClassName() {
		return $this->toClassName;
	}

	/**
	 * Returns the low-level attribute name
	 */	
	function toAttributeName() {
		return '__r_'.$this->name.'-'.$this->toClassName;
	}
}

// TODO doc: compared to DecomEntity, DecomClass is much low-level and general (DecomEntity is more like object, not class)
class DecomClass {
	protected $class;
	
	/* XXX There is storage duplication. But I think this is simple and faster.      */
	protected $attribs    = null; /* Includes inherited attributes                   */
	protected $rships     = null; /* Doesn't include inherited relationships         */
	protected $irships    = null; /* Doesn't include inherited inverse relationships */
	protected $ownAttribs = null; /* Attributes that are not inherited               */

	function __construct($class, $loadData = true) {
		global $DEDBLINK; /* TODO Remove after moving to prepared statement */

		if(!isset($class))
			throw new Exception('Entity class not specified');
		
		if(false === decom_validate_class_name($class))
			throw new Exception('Invalid class name');
		
		$this->class = $class;
		
		if($loadData) {
			$ret = $this->loadAttributes();
			if($ret !== true)
				throw new Exception($ret->getMessageHtml());
		}
	}

	/* TODO rem? */
	function getAttribIdFromAttribName($name) {
		if(substr($name, 0, 2) == '__') {        // Special attribute
			if(substr($name, 2, 2) == 'r_')        // Relationship
				$attribs = $this->getAttributes(false);
			else if(substr($name, 2, 3) == 'ir_')
				$attribs = $this->getAttributes(true, false);
			else
				throw new Exception('Unknown special attribute: '.$rname);
		}
		else {
			$attribs = $this->getAttributes();
		}

		foreach($attribs as $a)
			if($a->toAttributeName() == $name)
				return $a->getId();

		return false;
	}
	
	/* TODO rem? */
	function getAttribNameFromAttribId($id) {
		$attribs = $this->getAttributes(false, false);

		foreach($attribs as $a)
			if($a->getId() == $id)
				return $a->toAttributeName();

		return false;
	}

	// TODO test this method
	// TODO FIXME remove this; manipulation shouldn't be possible via OO methods.
	function addAttribute($name, $type = 'text') {
		decom_class_add_attribute($this->class, $name, $type);
	}
	
	function getAttributeIds() {
		// TODO FIXME do not query here. query in the constructor? OR ADD A PARAM TO CONTROL IT?
		return decom_db_query_array('select id from eav_ameta_'.$this->class.' order by display_order', 'id');
	}

	/**
	 * @paramAttrib
	 * @paramAttribIsNameTrue
	 * @retfalseobj DecomAttribute
	 */
	function getAttribute($attrib, $attribIsName = true) {
		if($attribIsName) { // Comparison put here for efficiency.
			foreach($this->getAttributes(false, false) as $a)
				if($a->toAttributeName() == $attrib)
					return $a;
		}
		else {
			foreach($this->getAttributes(false, false) as $a)
				if($a->getId() == $attrib)
					return $a;
		}

		return false;
	}
	
	/**
	 * @retobjarr DecomAttribute
	 */
	function getAttributes($excludeRelationships = true, $excludeInverseRelationships = true) {
		$attribs = $this->attribs;

		if(!$excludeRelationships) {
			foreach($this->rships as $rship) {
				$rnew = clone $rship;
				$attribs[] = $rnew;
			}
		}
		
		if(!$excludeInverseRelationships) {
			foreach($this->irships as $rship) {
				$rnew = clone $rship;
				$attribs[] = $rnew;
			}
		}

		return $attribs;
	}
	
	function getAttributeNames($excludeRelationships = true, $excludeInverseRelationships = true) {
		$anames = [];
	
		foreach($this->attribs as $a) {
			$anames[] = $a->toAttributeName();
		}

		if(!$excludeRelationships) {
			foreach($this->rships as $a) {
				$anames[] = $a->toAttributeName();
			}
		}
		
		if(!$excludeInverseRelationships) {
			foreach($this->irships as $a) {
				$anames[] = $a->toAttributeName();
			}
		}

		return $anames;
	}
	
	function getChildren() { // TODO test
		return decom_class_get_children($this->class);
	}
	
	/**
	 * @retfalseobj DecomRelationship
	 */
	function getInverseRelationship($name) {
		foreach($this->irships as $irship)
			if($irship->getName() == $name)
				return $irship;

		return false;
	}
	
	/**
	 * Get the incoming relationships
	 * @retobjarr DecomRelationship
	 */
	function getInverseRelationships() {
		return $this->irships;
	}
	
	function getName() {
		return $this->class;
	}
	
	/**
	 * Return the list of non-inherited attributes.
	 * @retobjarr DecomAttribute
	 */
	function getOwnAttributes() {
		return $this->ownAttribs;
	}
	
	function getParent() { // TODO test
		return decom_class_get_parent($this->class);
	}

	/**
	 * @retfalseobj DecomRelationship
	 */
	function getRelationship($name) {
		foreach($this->rships as $rship)
			if($rship->getName() == $name)
				return $rship;

		return false;
	}
	
	/**
	 * @retobjarr DecomRelationship
	 */
	function getRelationships() {
		return $this->rships;
	}
	
	/**
	 * @reterrtrue
	 */
	protected function loadAttributes() {
		$this->attribs    = [];
		$this->rships     = [];
		$this->irships    = [];
		$this->ownAttribs = [];
		$ownAttribsLoaded = false;
		$curClass = $this->class;

		$rships = decom_db_query_array('select * from eav_ameta_'.$this->class.' where name like "\_\_r\_%" or name like "\_\_ir\_%" order by display_order');
		if(decom_is_errobj($rships))
			return $rships;

		foreach($rships as $r) {
			$anamex   = explode('-', $r['name']);  // Before hyphen - __r_RSHIP or __ir_RSHIP; After hyphen - toClass

			if(substr($r['name'], 2, 2) == 'r_') { // Relationship
				$name  = substr($anamex[0], 4);
				$store = &$this->rships;
				$from  = $curClass;
				$to    = $anamex[1];
			}
			else {                                 // Inverse relationship
				$name  = substr($anamex[0], 5);
				$store = &$this->irships;
				$from  = $anamex[1];
				$to    = $curClass;
			}
			
				// TODO change the code after completing the migration
				$req = isset($r['requiredness'])? ($r['requiredness'] > 0): 0;			
				// $req = ($r['requiredness'] > 0);


			$attrib = new DecomRelationship(
				$r['id'], $name, $r['description'], $r['max_instances'], $req, $from, $to);

			$store[] = $attrib;
		}

		while($curClass) {
			$arr = decom_db_query_array('select * from eav_ameta_'.$curClass.' where name not like "\_\_r\_%" and name not like "\_\_ir\_%" order by display_order');
			if(decom_is_errobj($arr))
				return $arr;

			foreach($arr as $r) {
				/* Already present in attribs[] means the attrib has been overridden. */
				if(in_array($r['name'], $this->attribs))
					continue;

				// TODO change the code after completing the migration
				$req = isset($r['requiredness'])? $r['requiredness']: 0;			
				// $req = $r['requiredness'];
		
				$attrib = new DecomAttribute(
					$r['id'], $r['name'], $r['type'], $r['description'], $r['max_instances'], $req, $curClass);
				$attrib->setDisplayLabel($r['display_label']);
				$attrib->setDisplayOrder($r['display_order']);
				$attrib->setConstraintUnique($r['cons_uniq']);

				$this->attribs[] = $attrib;

				if(!$ownAttribsLoaded)
					$this->ownAttribs[] = $attrib;
			}
			
			$ownAttribsLoaded = true; // Yes, this becomes true in the very first iteration.
			$curClass = decom_class_get_parent($curClass);
		}
		
		/* Now remove the overloaded attributes */
		foreach($this->ownAttribs as $oa) {
			$oaName = $oa->getName();
			$oaId   = $oa->getId();

			foreach($this->attribs as $i => $a)
				if($oa->getName() == $oaName && $oa->getId() != $oaId) // ID checking is important because $attribs contains both inherited and self attribs.
					unset($this->attribs[$i]);
		}
		
		$this->attribs = array_values($this->attribs); // Re-index after removing unset elements.

		return true;
	}
}

/**
 * @h2 Functions
 */

// TODO move?
// TODO doc Double Underscore Prefix - e.g.: __r_ATTRIBNAME, __ir_ATTRIBNAME (relationship and inverse relationship)
function decom_validate_attribute_name($name, $allowLowLevelNames = false) {
	if(substr($name, 0, 2) == '__') {
		if($allowLowLevelNames) {
			if(substr($name, 2, 2) != 'r_' && substr($name, 2, 3) != 'ir_')
				return false;
			// else okay; do nothing
		}
		else {
			return false;
		}
	}
	
	/* XXX The regex also ensures the name is at least one character long. */
	$regex = $allowLowLevelNames? '/^[a-zA-Z0-9_][a-zA-Z0-9_\-]*$/': '/^[a-zA-Z0-9_]+$/';
	return (1 == preg_match($regex, $name)); // XXX php.net doc says "preg_match() returns 1 if the pattern matches given subject, 0 if it does not, or FALSE if an error occurred."
}

function decom_validate_class_name($name) {
	return (1 == preg_match('/^[a-zA-Z0-9_]+$/', $name));
}

// (TODO make stock doc)
/**
 * Any checks for duplication should be performed by the caller if attrId is given explicitly
 */
function decom_class_add_attribute($className, $name, $type = 'text', $description = '', $maxInstances = 1, $requiredness = 0, $uniqueGroup = 0, $displayParams = null, $attrId = null) {
	if(false === decom_validate_attribute_name($name, false))
		return new DecomError('Illegal attribute name.');
	
	return decom_class_add_attribute_without_name_check($className, $name, $type, $description, $maxInstances, $requiredness, $uniqueGroup, $displayParams, $attrId);
}

/*
 * TODO FIXME remove-add sequence is not safe. Perform checks and dry runs.
 * I've improved the working on 2019-12-07; but more to be done.
 * TODO accept attrib as ID also?
 * TODO should I use a lock mechanism?
 */
/**
 * @notice As of now, a remove-add-again sequence is performed for attribute editing. This may not be safe. Perform check on your own.
 */
function decom_class_edit_attribute($className, $oldName, $newName, $type = 'text', $description = '', $maxInstances = 1, $requiredness = 0, $uniqueGroup = 0, $displayParams) {
	global $DEDBLINK;

	if(false === decom_validate_attribute_name($newName, false))
		return new DecomError('Illegal attribute name.');

	$cobj   = new DecomClass($className);
	$attrId = $cobj->getAttribIdFromAttribName($oldName);
	if(decom_is_errobj($attrId))
		return $attrId;
	
	$ret = decom_class_add_attribute_without_name_check($className, '__tmp_'.$newName, $type, $description, $maxInstances, $requiredness, $uniqueGroup, $displayParams);
	if(decom_is_errobj($ret)) {
		decom_class_remove_attribute($className, '__tmp_'.$newName);
		return $ret;
	}
	
	$ret = decom_class_remove_attribute($className, $oldName, true, false);
	if(decom_is_errobj($ret)) {
		decom_class_remove_attribute($className, '__tmp_'.$newName);
		return $ret;
	}
	
	$ret = decom_class_rename_attribute($className, '__tmp_'.$newName, $newName);
	if(decom_is_errobj($ret))
		return $ret;
	
	$anameqNew = $DEDBLINK->quote($newName);
	$attrIdq = $DEDBLINK->quote($attrId);
	$ret = decom_db_exec("update eav_ameta_$className set id=$attrIdq where name=$anameqNew");
	
	return $ret;
}

// (TODO make stock doc)
// TODO doc no error if the parent classes have the same attribute
/**
 * @paramAttrib
 * @paramAttribIsNameTrue
 * @reterr
 */
function decom_class_rename_attribute($className, $attrib, $newName, $attribIsName = true) {
	global $DEDBLINK;

	// TODO lock eav_ameta?

	if(false === decom_validate_class_name($className))
		return new DecomError('Illegal class name.');
	// TODO turn off the same check in decom_class_attribute_exists()

	if(false === decom_validate_attribute_name($newName, false))
		return new DecomError('Illegal attribute name.');

	$aext = decom_class_attribute_exists($className, $newName);
	if(true === $aext)
		return new DecomError('Attribute already exists.');
	elseif(decom_is_errobj($aext))
		return $aext;

	// TODO EFFICIENCY quoting this before calling decom_class_attribute_exists() and turn off the quote in it.

	$anameqNew = $DEDBLINK->quote($newName);
	$attrq = $DEDBLINK->quote($attrib);

	if($attribIsName)
		$dbret = decom_db_exec("update eav_ameta_$className set name=$anameqNew where name=$attrq");
	else
		$dbret = decom_db_exec("update eav_ameta_$className set name=$anameqNew where id=$attrq");

	return $dbret;
}

/**
 * @paramAttrib
 * @paramAttribIsNameTrue
 * @reterr
 */
function decom_class_attribute_exists($className, $attrib, $attribIsName = true) {
	global $DEDBLINK;

	if(false === decom_validate_class_name($className))
		return new DecomError('Illegal class name.');

	$attrq = $DEDBLINK->quote($attrib);

	if($attribIsName)
		$dbret = decom_db_query_single_field(
			"select count(*) from eav_ameta_$className where name=$attrq");
	else
		$dbret = decom_db_query_single_field(
			"select count(*) from eav_ameta_$className where id=$attrq");	

	if(decom_is_errobj($dbret))
		return $dbret;

	return ($dbret != 0);
}

/**
 * @reterr
 */
function decom_class_exists($className) {
	global $DEDBLINK;

	$nq = $DEDBLINK->quote($className);

	$dbret = decom_db_query_single_field("select count(*) from classes where class=$nq");
	if(decom_is_errobj($dbret))
		return $dbret;

	return ($dbret != 0);
}

// (TODO make stock doc)
/**
 * Any checks for duplication should be performed by the caller if attrId is given explicitly
 */
function decom_class_add_attribute_lowlevel($className, $name, $type = 'text', $description = '', $maxInstances = 1, $requiredness = 0, $uniqueGroup = 0, $displayParams = null, $attrId = null) {
	if(false === decom_validate_attribute_name($name, true))
		return new DecomError('Illegal attribute name.');

	return decom_class_add_attribute_without_name_check($className, $name, $type, $description, $maxInstances, $requiredness, $uniqueGroup, $displayParams, $attrId);
}

/* TODO prevent injection (incomplete)? */
// TODO constraints, lock levels
// TODO doc this is low-level fun; do not use directly.
/**
 * @param maxInstances 0 for infinite
 * @reterrtrue
 */
// (TODO make stock doc)
/**
 * Any checks for duplication should be performed by the caller if attrId is given explicitly
 */
function decom_class_add_attribute_without_name_check($className, $name, $type = 'text', $description = '', $maxInstances = 1, $requiredness = 0, $uniqueGroup = 0, $displayParams = null, $attrId = null) {
	global $DEDBLINK;

	// TODO validate className
	// TODO $description

	/* This was how I calculated newId until 2019-05-10:
	 *
	 * $lastid = decom_db_query_single_field('select max(id) from eav_ameta_'.$className);
	 * $newId  = ($lastid !== null)? ($lastid + 1): 0;
	 *
	 * Why query like this?
	 *  1) MySQL doesn't allow references to the same table in subqueries.
	 *  2) 'Every derived table must have its own alias' (hence as tmp)
	 *
	 * Why I'm using a new inefficient method? Because I've started supporting inheritance.
	 */

	// TODO use database locking to prevent inconsistency. TODO use locking in other places also.

	$cobj = new DecomClass($className);

	/* RDBMS PK check is not sufficient (think about inheritance) */
	if($cobj->getAttribute($name) !== false)
		return new DecomError('Attribute name already in use.');
	
	$children     = decom_class_get_children($className);
	
	/* Now I have to find children at the leaf nodes.
	 * getAttributes() on a leaf child will return the attribs of the parents also.
	 * TODO move this to a separate function? Is there a need for reuse?
	 */
	$stack        = $children;
	$lastChildren = [];
	
	while(sizeof($stack) > 0) {
		$c    = array_pop($stack);
		$kids = decom_class_get_children($c);
		
		if($kids !== null && sizeof($kids) > 0) // TODO $kids !== null check is redundant?
			$lastChildren = array_merge($lastChildren, $kids);
		else // No kids
			array_push($lastChildren, $c);
	}

	// TODO See if I can come up with a better algorithm.
	$attribs = array_merge(
		$cobj->getAttributes(), // Includes attribs from the parent.
		$cobj->getRelationships(),
		$cobj->getInverseRelationships());
	
	if($attrId !== null) {
		$newId = $attrId;
	}
	else {
		$newId = 0;
		do {
			$finished = true;

			foreach($attribs as $a) {
				if($a->getId() == $newId) {
					$newId++;
					$finished = false;
					break;
				}
			}

			if($finished) {
				/* Parent check says it's settled; but I've to check with the kids, too. */
				// TODO TEST make sure this works.
				foreach($lastChildren as $child) {
					foreach($attribs as $a) {
						if($a->getId() == $newId) {
							$newId++;
							$finished = false;
							break 2; // Break foreach($lastChildren as $child)
						}
					}
				}
			}
		} while(!$finished);
	}

	foreach(['name', 'type', 'description', 'maxInstances'] as $f)
		 $$f = $DEDBLINK->quote($$f);

	$requiredness = intval($requiredness);

	if($displayParams) {
		$dLabel = $DEDBLINK->quote($displayParams['label']);

		/* XXX $DEDBLINK->quote() is redundant, but good for safety. */
		$dOrder = $DEDBLINK->quote(intval($displayParams['order']));
	}
	else {
		$dLabel = 'NULL';
		$dOrder = 0;
	}

	$uniqueGroup = $DEDBLINK->quote(intval($uniqueGroup));
	
	$rowsAffected = $DEDBLINK->exec("insert into eav_ameta_$className (name, id, type, description, max_instances, requiredness, cons_uniq, display_label, display_order) values ($name, $newId, $type, $description, $maxInstances, $requiredness, $uniqueGroup, $dLabel, $dOrder)");
	/* $DEDBLINK->errorCode() returned 0 in this case:
	 * ERROR 1366 (HY000): Incorrect integer value: '' for column 'display_order' at row 1  
	 * That's why I'm using $rows_affected in addition.
	 */
	// TODO make similar changes whereever needed.
	if($rowsAffected !== false && $DEDBLINK->errorCode() == 0)
		return true;
	else
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);
}

// TODO move
// TODO doc allowed cardinality values
function decom_add_relationship($name, $fromClassName, $toClassName, $leftCardinality, $rightCardinality) {
	// TODO valid attrib name, exceprt for `__` check

	// TODO validate class names

	// TODO same for relationship?
	if(false === decom_validate_attribute_name($name, false))
		return new DecomError('Illegal relationship name.');
	
	// TODO make robust, undo on errors
	
	$fromAttrib = "__r_$name-$toClassName";
	$cardx = decom_rship_cardinality_explode($leftCardinality); // TODO err
	$req = ($cardx['required']? 2: 0); // 2 means must be set and cannot be null
	$ret = decom_class_add_attribute_lowlevel($fromClassName, $fromAttrib, 'int', '', $cardx['maxInstances'], $req); // TODO desc
	if(is_a($ret, 'DecomError'))
		return $ret;

	$cardx = decom_rship_cardinality_explode($rightCardinality); // TODO err
	$req = ($cardx['required']? 2: 0); // 2 means must be set and cannot be null
	$ret = decom_class_add_attribute_lowlevel($toClassName, "__ir_$name-$fromClassName", 'int', '', $cardx['maxInstances'], $req); // TODO desc
	if(is_a($ret, 'DecomError')) { 
		decom_class_remove_attribute($fromClassName, "__r_$name-$toClassName");
		return $ret;
	}

	return true;
}

function decom_attrib_id($cOrEObj, $attrib, $attribIsName = true) {
	// TODO FIXME
}

// TODO stock doc for $attribIsName
function decom_attrib_name($cOrEObj, $attrib, $attribIsName = true) {
	if(is_a($attrib, 'DecomAttribute')) // includes DecomRelationship
		return $attrib->toAttributeName();
	else if($attribIsName) // Name given as a string
		return $attrib;
	else                   // ID given as a string or an integer
		return $cOrEObj->getAttribNameFromAttribId($attrib);
}

// TODO decom_remove_relationship()

function decom_class_get_children($className) {
	global $DEDBLINK;

	// Class name validation is not a must here

	$classNameQ = $DEDBLINK->quote($className);

	return decom_db_query_array('select class from classes where parent='.$classNameQ, 'class');
}

/**
 * @reterr
 */
function decom_class_get_entity_count($className) {
	global $DEDBLINK;

	if(false === decom_validate_class_name($className))
		return new DecomError('Invalid class name');

	return decom_db_query_single_field('select count(id) from eav_'.$className);
}

function decom_class_get_parent($className) {
	global $DEDBLINK;

	// Class name validation is not a must here

	$classNameQ = $DEDBLINK->quote($className);

	return decom_db_query_single_field('select parent from classes where class='.$classNameQ);
}

/**
 * @paramAttribIsNameTrue
 * @reterrtrue
 */
function decom_class_remove_attribute($className, $attrib, $attribIsName = true, $preventIfInUse = true) {
	global $DEDBLINK;

	// TODO validate className

	if($attribIsName) {
		// TODO move to a fun
		$attribQ = $DEDBLINK->quote($attrib);
		$ret = decom_db_query_single_field("select id from eav_ameta_$className where name=$attribQ");
		if(decom_is_errobj($ret))
			return $ret;
		else if($ret === false)
			return new DecomError('Couldn\'t find the attribute. Make sure you are not trying to delete an attribute from a parent class or an incoming relationship.');
		
		$attribId = $ret;
	}
	else {
		$attribId = $attrib;
	}
	
	$attribId = $DEDBLINK->quote($attribId);

	if($preventIfInUse) {
		$usageCount = decom_db_query_single_field("select count(*) from eav_$className where attrib=$attribId");
		if($usageCount != 0)
			return new DecomError('Cannot delete an attribute that is in use.');
	}

	// exec() returns the no. of affected rows, or FALSE for error
	$ret = $DEDBLINK->exec("delete from eav_ameta_$className where id=$attribId");
	if($DEDBLINK->errorCode() != 0)
		return new DecomError('Database error: '.$DEDBLINK->errorInfo()[2]);
	else if($ret != 1)
		return new DecomError('Either you are trying to delete an attribute from a parent class or there is a database inconsistency.');
	else		
		return true;
}

/**
 * Creates a class in the database.
 * @param name Name of the new class
 * @param parent Name of the parent class or null
 * @reterrtrue
 */
function decom_create_class($name, $parent = null) {
	global $DEDBLINK, $DE_eavAmetaCols;

	// @algotitle Algorithm to Create a new Class
	// @algodesc  Inputs - name, parent
	// @algostart

	// @algostep Validate the input arguments
	
	if(false === decom_validate_class_name($name))
		return new DecomError('Invalid class name');	

	if($parent === null) {
		$parent = 'NULL';
	}
	else {
		if(false === decom_class_exists($parent))
			return new DecomError('Invalid parent class');	

		$parent = $DEDBLINK->quote($parent);
	}

	$nq = $DEDBLINK->quote($name);

	// @algostep Return error if the class already exists
	/* Check if class already exists in 'classes'. This is important.
	 * Although I can find duplicate entry by checking the retval of `insert into classes`,
	 * the undo query will remove the old entry. That's why duplicate should be detected first.
	 */
	// TODO use decom_class_exists() ?
	if(decom_db_query_single_field("select count(*) from classes where class=$nq") != 0)
		return new DecomError('Class '.$name.' already exists.');

	// TODO move ?
	global $DELIBDIR;
	require_once($DELIBDIR.'/php/nan/sql.php');

	// @algostep Insert name and parent into the table `classes'
	// @algostep Create the table eav_ameta_CLASSNAME, eav_CLASSNAME and hier_CLASSNAME, where CLASSNAME is the name of the class to be created
	// @algostep Undo everything if error occurs while performing the above two steps

	$qs = [ "insert into classes values($nq, $parent)",
  	null, // Handled by another function
		null,
		null ];

	/* To be used with the table creation function */
	$ttypes = [null, // Since direct SQL
		'eavAmeta',
		'eav',
		'hier'];

	$qsUndo = [ "delete from classes where class=$nq and parent=$parent",
		"drop table eav_ameta_$name",
		"drop table eav_$name",
		"drop table hier_$name" ];

	$skipUndoSteps = [];

	for($i = 0; $i < sizeof($qs); $i++) {
		if($qs[$i] === null) {
			/* This checking and is really important; otherwise the existing table will be removed by the upcoming undo SQL statements. */
			$err = decom_db_class_implementation_table_exists($name, $ttypes[$i]);
			if($err === false) {
				$err = decom_db_create_class_implementation_table($name, $ttypes[$i]);
			}
			else {
				$skipUndoSteps[] = $i;

				if(!decom_is_errobj($err))
					$err = new DecomError('An implementation table for the class already exists.');
			}
		}
		else {
			$err = decom_db_exec($qs[$i]);
		}

		if(decom_is_errobj($err)) {
			/* Undo all queries till this point. */
			for($j = 0; $j <= $i; $j++) {
				if(!in_array($j, $skipUndoSteps))
					$DEDBLINK->exec($qsUndo[$j]);
			}

			return $err;
		}
	}
	
	// @algostep Return TRUE
	return true;
	// @algoend
}

function decom_get_accepted_rship_cardinalities() {
	return [
		'0..1', '0..n', '1..1', '1..n'
	];
}

/**
 * @reterrtrue
 */
function decom_remove_class($className, $dropObjects = false) {
	global $DEDBLINK;

	if(!decom_class_exists($className))
		return new DecomError('Class doesn\'t exist');
	
	$cobj = new DecomClass($className);

	if(sizeof($cobj->getChildren())) // TODO more efficient way?
		return new DecomError('Class cannot be deleted while it has child classes');

	if(sizeof($cobj->getRelationships())) // TODO more efficient way?
		return new DecomError('Class cannot be deleted while it has outgoing relationships');
		
	if(sizeof($cobj->getInverseRelationships())) // TODO more efficient way?
		return new DecomError('Class cannot be deleted while it has incoming relationships');

	if(!$dropObjects) {
		$ec = decom_class_get_entity_count($className);
		if(decom_is_errobj($ec))
			return $ec;
		else if($ec > 0)
			return new DecomError('Class cannot be deleted while it has objects, unless you specify.');
	}

	// TODO make sure no more checks are left

	$classNameQ = $DEDBLINK->quote($className);
	
	return decom_db_exec("delete from classes where class=$classNameQ;
    drop table if exists eav_ameta_$className;
    drop table if exists eav_$className;
    drop table if exists hier_$className");
}

function decom_rship_cardinality_explode($card) {
	// TODO validate $card
	
	$ret = [];
	
	$ends = explode('..', $card);
	$ret['required'] = ($ends[0] != 0); // true for 1..1 and 1..n
	$ret['maxInstances'] = ($ends[1] == '1')? 1: 0; // 1 for 0..1 and 1..1; 0 (infinite) for 0..n and 1..n
	
	return $ret;
}
?>
