<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-29
 */

/**
 * @h1 Error Handling
 */

// TODO doc - made a class instead of simple string so that additional data can be added in the future.
class DecomError {
	private $msg;

	function __construct($msg) {
		global $DEDIEONERR, $DEDEBUG;
		
		if(isset($DEDEBUG)) {
			/* This looks easier than debug_backtrace(); thanks to the comment by bishop on https://www.php.net/manual/en/function.debug-print-backtrace.php */
			$e = new Exception();
			$msg .= "\nStack trace:\n".$e->getTraceAsString();
		}
		
		if($DEDIEONERR)
			die($msg);
		
		$this->msg = $msg;
	}

	function getMessage() {
		return $this->msg;
	}

	function getMessageHtml() {
		return str_replace("\n", '<br/>', htmlspecialchars($this->msg));
	}
}

/**
 * @h2 Functions
 */

function decom_is_errobj($obj) {
	return is_a($obj, 'DecomError');
}
?>
