<?php
/* This file is part of libdecom.
 * Copyright (C) 2018 Nandakumar Edamana <nandakumar@nandakumar.co.in>
 * Started on 2018-11-28
 */

/**
 * @h1 Testing and Debugging
 */

require_once($DELIBDIR.'/php/nan/table.php');
require_once($DELIBDIR.'/php/nan/utility.php');

/* Wrapper for a moved function */
function nb($bool, $useSpan = true) {
	return nan_bool_to_yesno($bool, $useSpan);
}

/**
 * This function is for testing purpose so that you can make sure that libdecom
 * works with your module.
 */
function decom_say_hello() {
  echo 'Hello, this is libdecom.';
}

/**
 * A function analogous to phpinfo(), except that it returns the HTML code instead of printing it.
 * delibinfo() is implemented using this.
 */
function delibinfo_no_echo() {
  global $DELIBDIR, $DEDBDONTCONNECT, $DEDBCONFFILE, $DEDBHOST, $DEDBUNAME,
    $DEDBPWD, $DEDBNAME, $DEDBLINK, $DEDBCONNERR,
    $DECSSBASE, $DEJSBASE, $DEPROJDIR;
  
  $html = '<style>span.good, span.bad { font-weight: bold }
    span.good { color: #060 }
    span.bad { color: maroon }
    table { border-collapse: collapse }
    table td { border: 1px solid #888; padding: 0.5em } </style>';

  $html .= '<h1>libdecom Status</h1>';

  $html .= '<h2>Basic Configuration</h2>';
  $html .= nan_table_start();
  $html .= nan_table_array_to_td(['$DELIBDIR', $DELIBDIR]);
  
  $tmp = is_dir($DELIBDIR);
  $html .= nan_table_array_to_td(['$DELIBDIR exists and is a directory', nb($tmp)]);
  if($tmp) {
    $html .= nan_table_array_to_td(['$DELIBDIR is readable',
      nb(is_readable($DELIBDIR))]);
  }

  $html .= '<h2>File Storage</h2>';
  $html .= nan_table_array_to_td(['$DEPROJDIR is set', nb(isset($DEPROJDIR))]);
  if(isset($DEPROJDIR)) {
	  $html .= nan_table_array_to_td(['$DEPROJDIR', $DEPROJDIR]);

  	$isdir = is_dir($DEPROJDIR);
	  $html .= nan_table_array_to_td(['$DEPROJDIR exists and is a directory',
	  	nb($isdir)]);
	  	
	  if($isdir)
	  	$html .= nan_table_array_to_td(['$DEPROJDIR is writeable',
	  		nb(is_writeable($DEPROJDIR))]);
	  unset($isdir);
  }
  
  $html .= nan_table_close();
  
  $html .= '<h2>Database Connectivity</h2>';
  $html .= nan_table_start();
 
  foreach(['DEDBHOST', 'DEDBUNAME', 'DEDBNAME'] as $v) {
    $html .= nan_table_array_to_td(['$'.$v,
      isset($$v)? $$v: '<span class="bad">Undefined</span>']);
  }
  
  $html .= nan_table_array_to_td(['$DEDBPWD is set', nb(isset($DEDBPWD))]);

  $tmp = !isset($DEDBDONTCONNECT) || $DEDBDONTCONNECT !== true;
  $html .= nan_table_array_to_td(['$DEDBDONTCONNECT is off', nb($tmp)]);
  if($tmp) {
    $html .= nan_table_array_to_td(['Database connection established',
      nb(isset($DEDBLINK) && $DEDBLINK !== false)]);
    if($DEDBLINK == false) {
      $html .= nan_table_array_to_td(['Database connection error', $DEDBCONNERR]);
    }
  }
  
  $html .= nan_table_close();

  $html .= '<h2>CSS and JS</h2>';
  $html .= nan_table_start();

  $html .= nan_table_array_to_td(['$DECSSBASE', $DECSSBASE]);
  $html .= nan_table_array_to_td(['$DEJSBASE', $DEJSBASE]);

  /* CSS serve-ability is found by comparing the content of
   * decom-lib/css/test.txt with that of the output of file_get_contents()
   * called with a URL that containes $DECSSBASE. Both should be the same if
   * the mapping between http[s]://DOMAIN/css/ and decom-lib/css/ is correct.
   * Same procedure for JS.
   */

  /* If $DECSSBASE starts with http:// or https://,
   * no need to prepend the domain.
   */
  if(strpos($DECSSBASE, 'http://') === 0 ||
    strpos($DECSSBASE, 'https://') === 0)
  {
    $csstesturl = $DECSSBASE.'/test.txt';
  }
  else {
    $csstesturl = $_SERVER['REQUEST_SCHEME'].'://'.
      $_SERVER['HTTP_HOST'].$DECSSBASE.'/test.txt';

    $servedcss = null;
    if(in_array('200', explode(get_headers($csstesturl)[0], ' ')))
      $servedcss = file_get_contents($csstesturl);
  }

  $html .= nan_table_array_to_td(['Can serve CSS?', nb(
    is_file($DELIBDIR.'/css/test.txt') &&
    is_readable($DELIBDIR.'/css/test.txt') &&
    /* content from the codebase is same as the content served by
     * the web server.
     */
    file_get_contents($DELIBDIR.'/css/test.txt') == 
      $servedcss
    )]);
  
  /* Now the same to test JS */  
  if(strpos($DEJSBASE, 'http://') === 0 ||
    strpos($DEJSBASE, 'https://') === 0)
  {
    $jstesturl = $DEJSBASE.'/test.txt';
  }
  else {
    $jstesturl = $_SERVER['REQUEST_SCHEME'].'://'.
      $_SERVER['HTTP_HOST'].$DEJSBASE.'/test.txt';
      
    $servedjs = null;
    if(in_array('200', explode(get_headers($jstesturl)[0], ' ')))
      $servedjs = file_get_contents($jstesturl);
  }

  $html .= nan_table_array_to_td(['Can serve JS?', nb(
    is_file($DELIBDIR.'/js/test.txt') &&
    is_readable($DELIBDIR.'/js/test.txt') &&
    /* content from the codebase is same as the content served by
     * the web server.
     */
    file_get_contents($DELIBDIR.'/js/test.txt') == 
      $servedjs
    )]);

  $html .= nan_table_close();
  
  return $html;
}

/**
 * A function analogous to phpinfo()
 */
function delibinfo() {
	echo delibinfo_no_echo();
}
?>
