<?php
/* This file is part of libdecom.
 * Copyright (C) 2018, 2019 Nandakumar Edamana
 * Code moved from page.php on 2019-07-01, which was started on 2018-12-31
 */

/**
 * @h1 Breadbar View
 */

class DecomBreadbarView {
	protected $crumbs = []; // 2D array; each elem - [$label, $link]
	
	function appendCrumb($label, $link) {
		$this->crumbs[] = [$label, $link];
	}
	
	/* TODO doc index can be -ve */
	function removeCrumb($index) {
		$this->crumbs = array_splice($this->crumbs, $index);
	}
	
	function toHtml() {
		$html = '';

		foreach($this->crumbs as $crumb)
			$html .= '<a href="'.$crumb[1].'">'.$crumb[0].'</a> » '; // TODO do better?

		return $html;
	}
}
?>
