<?php
/* This file is part of libdecom.
 * Copyright (C) 2018, 2019 Nandakumar Edamana
 * Code moved from page.php on 2019-07-01, which was started on 2018-12-31
 */

/**
 * @h1 Notebook View
 */

/**
 * @h2 DecomNotebookView class
 */
class DecomNotebookView {
	protected $tabs = [];
	
	function __construct($id = null) {
		$this->id = $id;
	}
	
	/**
	 * @param tab Instance of DecomNotebookViewTab
	 */
	function appendTab($tab) {
		$this->tabs[] = $tab;
	}
	
	function toHtml() {
		$hdr     = '<ul class="_deUlTabSelector">';
		$tabArea = '';

		foreach($this->tabs as $tab) {
			$hdr     .= '<li><a class="_deTabSelector" href="#'.$tab->getId().'" data-nan-tabid="'.$tab->getId().'" data-nan-ntbkid="'.$this->id.'">'.$tab->getTitle().'</a></li>';
			$tabArea .= $tab->toHtml('_dentbk_tabof_'.$this->id);
		}
		
		$hdr .= '</ul>';
		
// TODO move
// TODO FIXME only once per page (regardless of the no. of notebooks)
		$script = '<script>
// TODO REM if not of much use or defined earlier
function $(id) {
	return document.getElementById(id);
}

function $c(className) {
	return document.getElementsByClassName(className);
}

function onSelectorClick(selector) {
	selectors = $c("_deTabSelector");
	ntbkid = selector.getAttribute("data-nan-ntbkid");
	panes  = $c("_dentbk_tabof_" + ntbkid);

	/* Selector styling */
	for(var j = 0; j < selectors.length; j++) {
		selectors[j].setAttribute("data-nan-selected", "0");
		panes[j].style.display = "none";
	}

	selector.setAttribute("data-nan-selected", "1");
	
	tabid = selector.getAttribute("data-nan-tabid");
	$(tabid).style.display = "block";
	nanOnWinResize();
}

document.addEventListener("DOMContentLoaded", function() {
	selectors = $c("_deTabSelector");

	for(var i = 0; i < selectors.length; i++) {
		/* Add onclick listener to display the selected pane */
		selectors[i].addEventListener("click", function() {
			onSelectorClick(this);
		});
	}
	
	for(var i = 0; i < selectors.length; i++) {
		hadHash = false;
	
		/* If the URL specifies a tab, display it right away. */
		if("#" + selectors[i].getAttribute("data-nan-tabid") == window.location.hash) {
			hadHash = true;
			onSelectorClick(selectors[i]); // Safer than selectors[i].click(), I think.
			
			break;
		}
	}
	
	if(selectors.length > 0 && !hadHash) {
		onSelectorClick(selectors[0]);
	}
});
</script>';
		
		return $script.$hdr.$tabArea;
	}
}

/**
 * @h2 DecomNotebookViewTab class
 */
class DecomNotebookViewTab {
	protected $id;
	protected $innerTitleTag;
	protected $title;
	
	function __construct($title, $html, $innerTitleTag = 'h2', $id = null) {
		$this->title   = $title;
		$this->content = $html;
		$this->id      = $id;
		$this->innerTitleTag = $innerTitleTag;
	}

	function getId() {
		return $this->id;
	}

	function getTitle() {
		return $this->title;
	}
	
	function toHtml($className = '') {
		$itt   = &$this->innerTitleTag;
		$cattr = 'class="'.$className.' _deNotebookTabPane"';

		return "<div id=\"".$this->id."\"$cattr><$itt>".$this->title."</$itt>".$this->content.'</div>';
	}
}
?>
