<?php
/* This file is part of libdecom.
 * Copyright (C) 2018, 2019 Nandakumar Edamana
 * Started on 2018-12-31
 */

/**
 * @h1 Page View
 */

require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/views/breadbar.php');

// TODO doc
abstract class DecomPageViewMessageType {
	const DEPAGE_MSGTYPE_MSG  = 0;
	const DEPAGE_MSGTYPE_WARN = 1;
	const DEPAGE_MSGTYPE_ERR  = 2;
	
	function constToText($type) {
		return ['message', 'warning', 'error'][$type];
	}
}

class DecomPageViewFooter {
	protected $attribution = '';
	protected $menu        = null;
	protected $customHtml  = null;
	
	/**
	 * Outputs the HTML code of the footer, without wrapping with the footer tag.
	 */
	function toHtml() {
		if($this->customHtml === null) {
			$html = '';

			if($this->menu)
				$html = $this->menu->toHtml('_deFooterMenu');

			$html .= '<div>'.$this->attribution.'</div>';
		}
		else {
			$html = $this->customHtml;
		}
		
		return $html;
	}
	
	function setAttributionHtml($html) {
		$this->attribution = $html;
	}

  /**
   * @param menu Instance of DecomMenu
   */	
	function setMenu($menu) {
		$this->menu = $menu;
	}
	
	/**
	 * To override the automatic output.
	 */
	function setCustomHtml($html) {
		$this->customHtml = $html;
	}
}

/**
 * @h2 DecomPageView class
 */

/**
 * Created a class for future tasks like a script generating new pages and
 * saving them to the disk without displaying. Another benefit is inheritance.
 */
class DecomPageView {
  protected $body     = null;
  protected $content  = null;
  protected $breadbar = null;
  protected $footer   = null;
  protected $headerLogoUrl      = null;
	protected $htmlBeforeHeaderH1 = null; // TODO rename to something other than H1
  protected $msgs     = [];
  protected $navbar   = null;
  protected $sideboxHtml = null;
  protected $sideMenu = null;
  
  /* In the absence of one, the others may be used. Set them to empty string to prevent this. */
  // TODO doc the above in setter functions.
  protected $title    = null;     /* Browser window title */
  protected $mastheadText = null; /* Site title */
  protected $heading  = null;     /* Article heading */

	// TODO MOVE
	// TODO DOC - FOR INTERNAL USE
	// $expected can be - 'title', 'mastheadText', 'heading'
	function getUsableTitle($expected) {
		$ttl = '';
		
		if(isset($this->$expected)) {
			return $this->$expected;
		}
		else {
			foreach(['title', 'heading', 'mastheadText'] as $v)
				if(isset($this->$v))
					return $this->$v;
		}
		
		return '';
	}

	/* TODO doc: title not rendered if it is '' or null - doc in other funs aslo. */
	protected function addMessageAbstract($html, $title, $type) {
		$this->msgs[] = [$html, $title, $type];
	}

  function addErrorMessage($html, $title = 'Error') {
  	$this->addMessageAbstract($html, $title, DecomPageViewMessageType::DEPAGE_MSGTYPE_ERR);
  }

  function addMessage($html, $title = '') {
  	$this->addMessageAbstract($html, $title, DecomPageViewMessageType::DEPAGE_MSGTYPE_MSG);
  }
  
  function addWarning($html, $title = 'Warning') {
  	$this->addMessageAbstract($html, $title, DecomPageViewMessageType::DEPAGE_MSGTYPE_WARN);
  }
  
  function clearMessages() {
	  $this->msgs = [];
  }

  function display() {
		echo $this->toHtml();
  }

  function toHtml() {
  	global $DECSSBASE, $DETHEME;
  
    return '<!DOCTYPE HTML>
      <html>
        <head>
          <meta charset="UTF-8" />
          <title>'.$this->getUsableTitle('title').'</title>
          <link rel="stylesheet" href="'.$DECSSBASE.'/'.$DETHEME.'/main.css" />
        </head>
        <body>
         '.$this->getBody().'
        </body>
      </html>';
  }

	/* TODO introduce a variable bakedBody for cache?
	 * But I think it's of no use since this function will be called
	 * only once.
	 */
  private function getBody() {
  	if($this->body) {
  		return $this->body;
  	}
  	
  	/* TODO move */
  	/* TODO FIXME prevent the following JS function from appearing in the libdecom PHP doc */
  	$body = "<script>
function $0(tag) {
	return document.getElementsByTagName(tag)[0];
}

function nanOnWinResize() {
	if(window.innerWidth > 1024) {
		elems = document.getElementsByClassName('pillarbox');
		for(i = 0; i < elems.length; i++)
			elems[i].style.maxWidth = '1200px';
	}

	/* Make the footer stick to the window bottom if there is free space */
	$0('main').style.height = '';
	hhfh = $0('header').offsetHeight + $0('footer').offsetHeight;
	if(window.innerHeight > $0('main').offsetHeight + hhfh) {
		$0('main').style.height = (window.innerHeight - hhfh) + 'px';
	}
}

window.addEventListener('resize', nanOnWinResize);
document.addEventListener('DOMContentLoaded', nanOnWinResize);
</script>";
  	
	  /* TODO elaborate */
  	$body .= '<header><div class="headerInner pillarbox">';

  	if($this->htmlBeforeHeaderH1)
	  	$body .= $this->htmlBeforeHeaderH1;
  	
  	if($this->headerLogoUrl)
  		$body .= '<img src="'.$this->headerLogoUrl.'" alt="Logo"/>';

  	$body .= '<span class="mastheadText">'.$this->getUsableTitle('mastheadText').'</span>';
  	
  	if($this->navbar)
	  	$body .= '<nav id="navbarMain">'.$this->navbar->toHtml().'</nav>';
	  
	  $body .= '</div></header>';
	  
  	$body .= '<main><div class="middleInner pillarbox">';

		$cont = '';

		if($this->breadbar)
			$cont .= '<div class="_deBreadbar _deBreadbarMain">'.$this->breadbar->toHtml().'</div>';

		foreach($this->msgs as $msg) {
			$cont .= '<div class="msgbox '.DecomPageViewMessageType::constToText($msg[2]).'">';

			if($msg[1] !== null && $msg[1] != '')
				$cont .= '<span style="font-weight: bold">'.$msg[1].': </span>';

			$cont .= '<p>'.$msg[0].'</p></div>';
		}

		if($this->heading !== null)
			$cont .= '<h1>'.$this->heading.'</h1>';

  	$cont .= $this->content;

  	if($this->sideMenu || $this->sideboxHtml) {
  		$sidehtml = ($this->sideMenu)? $this->sideMenu->toHtml(): $this->sideboxHtml;
  	
			$body .= '<div style="display: table; width: 100%;">';
			$body .= '<div style="display: table-row">';
			$body .= '<div style="display: table-cell" class="sideMenu">'.$sidehtml.'</div>';
			$body .= '<div style="display: table-cell" class="content">'.$cont.'</div>';
			$body .= '</div>';
			$body .= '</div>';
		}
		else {
			$body .= '<div class="content">'.$cont.'</div>';
		}
  	$body .= '</div></main>';

  	$body .= '<footer><div class="footerInner pillarbox">';
  	if($this->footer)
  		$body .= $this->footer->toHtml();
  	$body .= '<footer></div>';

    return $body;
  }

  function setContent($content) {
	  $this->content = $content;
  	$this->body = null;
  }

  function setBody($body) {
    $this->body = $body;
  	$this->content = null;
  }
  
  function setBreadbar($breadbar) {
    $this->breadbar = $breadbar;
  }

	// TODO doc
	function setHeading($heading) {
		$this->heading = $heading;
	}
 
  function setHeaderLogoUrl($url) {
  	$this->headerLogoUrl = $url;
  }
  
  // TODO rename to something other than H1
  function setHtmlBeforeHeaderH1($html) {
  	$this->htmlBeforeHeaderH1 = $html;
  }

  /**
   * @param menu Instance of DecomPageViewFooter
   */
  function setFooter($footer) {
    $this->footer = $footer;
  }

	// TODO doc
	function setMastheadText($text) {
		$this->mastheadText = $text;
	}
  
  /**
   * @param menu Instance of DecomMenu
   */
  function setNavbar($menu) {
    $this->navbar = $menu;
  }

  function setSideboxHtml($html) {
    $this->sideboxHtml = $html;
  }

	/**
	 * @param menu Instance of DecomMenu
	 */
  function setSideMenu($menu) {
    $this->sideMenu = $menu;
  }

  function setTitle($title) {
    $this->title = $title;
  }

  /* TODO */
}

/**
 * @h2 Convenience functions for the current page
 */

function decom_get_current_page() {
	global $_DECOM_RESERVED;

	return $_DECOM_RESERVED['curpage'];
}

/**
 * Following are the procedural solutions to handle DecomPageView.
 */

function decom_page_add_error_message($html, $title = 'Error') {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->addErrorMessage($html, $title);
}

function decom_page_add_message($html, $title = '') {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->addMessage($html, $title);
}

function decom_page_add_warning($html, $title = 'Warning') {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->addWarning($html, $title);
}

function decom_page_clear_messages() {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->clearMessages();
}

/**
 * This function dumps the HTML code so that the page you've been constructing
 * becomes visible to the visitor.
 */
function decom_page_display() {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->display();
}

/**
 * This should be called once (and only once) before you call any other of the
 * decom_page_*() functions.
 */
function decom_page_init() {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage'] = new DecomPageView();
}

/**
 * Sets the content inside the HTML body tag.
 * Use decom_page_set_content() instead if you want the standard header and
 * footer.
 */
function decom_page_set_body($body) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setBody($body);
}

/**
 * @param breadbar Instance of DecomBreadbarView
 */
function decom_page_set_breadbar($breadbar) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setBreadbar($breadbar);
}

/**
 * Sets the content without removing the standard page header and footer.
 * Use decom_page_set_body() to override everything and set the content inside
 * the HTML body tag.
 */
function decom_page_set_content($content) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setContent($content);
}

/**
 * @param menu Instance of DecomPageViewFooter
 */
function decom_page_set_footer($footer) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setFooter($footer);
}

function decom_page_set_header_logo_url($url) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setHeaderLogoUrl($url);
}

/**
 * Sets the page heading (not masthead)
 */
function decom_page_set_heading($heading) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setHeading($heading);
}

/**
 * Sets the masthead text (site title, usually)
 */
function decom_page_set_masthead_text($text) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setMastheadText($text);
}

/**
 * @param menu Instance of DecomMenu
 */
function decom_page_set_navbar($menu) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setNavbar($menu);
}

function decom_page_set_sidebox_html($html) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setSideboxHtml($html);
}

/**
 * @param menu Instance of DecomMenu
 */
function decom_page_set_side_menu($menu) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setSideMenu($menu);
}

/**
 * Sets the value inside the HTML title tag. The same title may get reused in
 * various other places, say the page header.
 */
function decom_page_set_title($title) {
  global $_DECOM_RESERVED;

  $_DECOM_RESERVED['curpage']->setTitle($title);
}
?>
