<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-11
 */

/**
 * @h1 Entity Edit View
 */

require_once($DELIBDIR.'/php/views/entity.php');
require_once($DELIBDIR.'/php/views/entitylistdropdown.php');
require_once($DELIBDIR.'/php/nan/form.php');

/**
 * @h1 Entity Add/Edit Views
 */

/**
 * @h2 DecomEntityAddEditView Class
 */
// TODO doc lowlevel
class DecomEntityAddEditView extends DecomEntityView {
	private $formAction  = '';
	private $formMethod  = 'POST';
	private $formId      = null;
	private $formType;
	private $showParentObjectId = false;
	private $customFields = [];
	private $formAppends = [];
	
	// See setRelationshipDropdownSize() for doc; XXX update doc when changing the value
	private $rshipSelectMax = 200;
	
	/**
	 * @param eid Needed only for entity editing
	 */
	function __construct($type = 'add', $className, $eid = null) { // another possible value: edit
		$this->formType = $type;
		
		if($type == 'add')
			parent::loadClassData($className);
		else
			parent::load($className, $eid);			
	}
	
	// TODO doc
	// TODO currently this is only to override existing attribs; accept new fields and apply order in rendering
	/**
	 * @param $field Name of an existing attribute or a new one
	 */
	function addCustomField($name, $html, $order = 65535) {
		$this->customFields[$name] = [ 'html' => $html, 'order' => $order ];
	}
	
	function clearFormAppends() {
		$this->formAppends = [];
	}
	
	function setFormAction($action) {
		$this->formAction = $action;
	}

	/* TODO doc: key can be 'inputFields', 'inputFieldTypes', etc.
	 * value must be an array.
	 */
	function setFormAppends($assoc) {
		$this->formAppends = $assoc;
	}
	
	// TODO doc: currently used as an attribute with the + buttons for multi-valued attribs. Not in <form id=>
	function setFormId($id) {
		$this->formId = $id;
	}
	
	function setFormMethod($method) {
		$this->formMethod = $method;
	}

	function setshowParentObjectId($show) {
		$this->showParentObjectId = $show;
	}

	/**
	 * Set the maximum no. of elements allowed in dropdown lists displayed for relationships.
	 * If the no. of elements in the target class is larger than this value, just a plain
	 * input box will be displayed instead of the dropdown list.
	 */
	function setRelationshipDropdownSize($size) {
		$this->rshipSelectMax = $size;
	}

	function render($renderError = true) {
		$dropdownNoneLabel = _('-- None --'); // 'None' instead of 'Select' will let users know that this option can be used for instance deletion.

		$content = '';
	
		// TODO move
		$content .= '<script>
var addInstBtnHandlersSet = false;
// TODO REM if not of much use or defined earlier
function $(id) {
return document.getElementById(id);
}

function $c(className) {
return document.getElementsByClassName(className);
}

document.addEventListener("DOMContentLoaded", function() {
if(addInstBtnHandlersSet)
	return;

elems = $c("btnMulInstNew");

for(var i = 0; i < elems.length; i++) {
	elems[i].addEventListener("click", function() {
		formId    = this.getAttribute("data-nan-frmid");
		attrName  = this.getAttribute("data-nan-attrname");
		container = $("divMulInstContainer_" + formId + "_" + attrName);

		dtype = this.getAttribute("data-nan-attrdtype");
		if(dtype != "_copy") {
			inp = document.createElement("input");
			inp.setAttribute("name", attrName + "[]");
			inp.setAttribute("type", dtype);
		}
		else {	// Just copy the dummy element
			dummyid = this.getAttribute("data-nan-dummyid");
			inp = $(dummyid).cloneNode(true);
			inp.removeAttribute("id");
			inp.style.display = ""; // Make visible
		}

		container.appendChild(inp);
		container.appendChild(document.createElement("br"));
	});
}

addInstBtnHandlersSet = true;
});
</script>';

		$attribs = $this->cobj->getAttributes(false, true);

		$inputFields      = [];
		$inputFieldLabels = [];
		$inputFieldTypes  = [];
		$inputFieldsReq   = [];
		$customInput      = [];
		$defaultValues    = [];

		if($this->showParentObjectId) {
			$inputFields[]      = '__parent';
			$inputFieldLabels[] = _('Parent Object');
			$inputFieldsReq[]   = false;
			
			$className = $this->cobj->getName();
			
			$ec = decom_class_get_entity_count($className);
			if(!decom_is_errobj($ec)) {
				if($ec == 0) { // Has no entities
					$customInput[] = '<span>'._('No objects present.').'</span>';
					$inputFieldTypes[]  = 'custom';
				}
				else if($ec > $this->rshipSelectMax) { // TODO rename $this->rshipSelectMax to suit all purpose?
					$inputFieldTypes[]  = 'int';
				}
				else {
					$dropdown = new DecomEntityListDropDownView($this->className);
					$inputFieldTypes[]  = 'custom';

					$default = '';
					if($this->formType == 'edit') {
						$par = $this->eobj->getParentId(false);
						if(decom_is_errobj($par)) {
							if($renderError) // TODO check if page exists
								decom_page_add_warning($par->getMessageHtml());
							else
								return $par;
						}

						if($par !== null)
							$default = $par;
					}

					$customInput[] = $dropdown->render(null,
						['name' => '__parent'],
						$dropdownNoneLabel,
						'',
						$default);;
				}
			}
			else {
				if($renderError) // TODO check if page exists
					decom_page_add_warning($ec->getMessageHtml());
				else
					return $ec;
			}
		}

		foreach($attribs as $a) {
			if(in_array($a->getName(), $this->getHiddenAttribs()))
				continue;
		
			$dropdown  = null;
			$custField = null;

			if(isset($this->customFields[$a->getName()])) {
				$custField = $this->customFields[$a->getName()];
			}

			if(is_a($a, 'DecomRelationship')) {
				$attrName  = $a->toAttributeName();
				$targClass = $a->getToClassName();
				$inputFieldLabels[] = 'Relationship <i>'.$a->getName().'</i> to the class <i>'.$targClass.'</i>';

				if(!$custField) {
					$ec = decom_class_get_entity_count($targClass);
					if(!decom_is_errobj($ec)) {
						if($ec <= $this->rshipSelectMax) {
							$dropdown = new DecomEntityListDropDownView($targClass);
						}
					}
					else {
						decom_page_add_warning($ec->getMessageHtml());
					}
				}
			}
			else {
				$attrName = $a->getName();
				$inputFieldLabels[] = $a->getDisplayLabel();
			}
	
			$anameLow = $a->toAttributeName();
			
			if($dropdown || $custField)
				$type = 'custom';
			else
				$type = ($a->getType() == 'int')? 'number': 'text';

			if($a->getMaxInstances() == 1) {
				$inputFields[]      = $anameLow;
				$inputFieldTypes[]  = $type;
				if($this->formType == 'edit' && $this->eobj->hasPropertyValue($anameLow))
						$defaultValues[$anameLow] = $this->eobj->getPropertyValue($anameLow);

				if($custField)
					$customInput[] = $custField['html'];
				else if($dropdown)
					$customInput[] = $dropdown->render(null,
						['name' => $attrName],
						$dropdownNoneLabel,
						'',
						isset($defaultValues[$anameLow])? $defaultValues[$anameLow]: '');
			}
			else { // Multi-instance attribute
				if($this->formId === null)
					throw new Exception('formId should be set if the class has multi-valued attributes.');
			
				// TODO FIXME multi-instance relationship
				$mulInstCurValues = '';
				if($this->formType == 'edit' && $this->eobj->hasPropertyValue($anameLow)) {
					$vals = $this->eobj->getPropertyValue($anameLow);

					foreach($vals as $v) {
						if($custField) {
							$customInput[] = $custField['html'];
						}
						else if(!$dropdown) {
							$mulInstCurValues .= '<input name="'.$attrName.'[]" type="'.$type.'" value="'.$v.'" /><br/>';
						}
						else {
							$mulInstCurValues .= $dropdown->render(null,
								['name' => $attrName.'[]'],
								$dropdownNoneLabel,
								'',
								$v).'<br/>';
						}
					}
				}

				$inputFields[]     = $anameLow.'[]';
				$inputFieldTypes[] = 'custom';
				
				$dummyelem  = '';
				$mulValArea = '<noscript>JavaScript is required to handle multi-valued attributes.</noscript>'.
					'<div id="divMulInstContainer_'.$this->formId.'_'.$attrName.'">'.
					$mulInstCurValues.
					'</div>'.
					'<button type="button" id="btnMulInstNew_'.$attrName.'" '.
						'class="btnMulInstNew" data-nan-frmid="'.$this->formId.'" '.
						'data-nan-attrname="'.$attrName.'" ';

				if($dropdown) {
					$dummyid     = 'dummyMulInstNew_'.$attrName;
					$mulValArea .= 'data-nan-attrdtype="_copy"'.
						' data-nan-dummyid="'.$dummyid.'"';
					$dummyelem   = $dropdown->render($dummyid,
						['name' => $attrName.'[]', 'style' => 'display: none'],
						$dropdownNoneLabel,
						'');
				}
				else {
					$mulValArea .= 'data-nan-attrdtype="'.$type.'"';
				}
				
				$mulValArea .= '>+</button>'; // type="button" is important (otherwise it'll be treated as the Submit button) TODO FIXME
				// TODO defaultValues for formType='edit'
				$customInput[] = $dummyelem.$mulValArea;
			}

			$inputFieldsReq[] = !$a->getCanBeNull(); /* Might be a required field, but still the value can be NULL */
		}

		/* Hidden values */
		foreach($this->getHiddenFormValues() as $k => $v) {
			$inputFields[]      = $k;
			$inputFieldTypes[]  = 'hidden';
			$inputFieldsReq[]   = true; // TODO is there a choice?
			$inputFieldLabels[] = '';
			$defaultValues[$k]  = $v;
		}

		/* TODO XXX only permit valid keys like inputFields, inputFieldTypes, etc. */
		foreach($this->formAppends as $key => $value)
			if(isset($$key))
				$$key = array_merge($$key, $value);

		$content .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $this->formAction, $this->formMethod, $customInput, $defaultValues);

		return $content;		
	}
}

/**
 * @h2 DecomEntityAddView Class
 */

class DecomEntityAddView extends DecomEntityAddEditView {
	function __construct($className) {
		parent::__construct('add', $className);
	}
}

/**
 * @h2 DecomEntityEditView Class
 */

class DecomEntityEditView extends DecomEntityAddEditView {
	function __construct($className, $eid) {
		parent::__construct('edit', $className, $eid);
	}
}
?>
