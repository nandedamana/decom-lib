<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Made a separate file on 2019-07-02
 */

/**
 * @h1 Entity List Table View
 */

require_once($DELIBDIR.'/php/views/entitylist.php');

// TODO start index, end index
class DecomEntityListTableView extends DecomEntityListView {
	private $customLabels;
	private $eidVisible;
	private $hiddenAttribs = [];
	
	// TODO doc - assoc: key - aname, value - label
	function setCustomLabels($assoc) {
		$this->customLabels = $assoc;
	}
	
	function setEidVisible($visible) {
		$this->eidVisible = $visible;
	}
	
	function setHiddenAttribs($anames) {
		$this->hiddenAttribs = $anames;
	}

	// TODO $additionalHeaders - each elem can be assoc so that they can contain additional info like order (but if one is assoc, all other elems should also be assoc - else there can be issues
	// TODO will return DecomError if $renderError is false
	// TODO doc first arg of the callback will be filled with the ref to the row of the table
	// TODO doc $fieldsToReturn - what fields should be returned to the second arg callback for every row ([$FIELDi]); if null, the arg will be filled with $eid (not array), otherwise, ['__id' => $eid, FIELDi => VALi]
	// TODO doc if no val for a field in $fieldsToReturn is found in eobj, the field is ignored (i.e., no err)
	function render($additionalHeaders = [], $rowcallback = null, $fieldsToReturn = null, $renderError = true) {
		$ids = $this->getEntityIds(); // TODO do not collect each time?
		if(decom_is_errobj($ids)) {
			if($renderError)
				return $ids->getMessageHtml();
			else
				return $ids;
		}

		$attribNamesOriginal = $this->cobj->getAttributeNames();
	
		if($this->cols)
			$attribNames = array_intersect($attribNamesOriginal, $this->cols); // Because the caller may give non-existing attribnames.
		else
			$attribNames = $attribNamesOriginal;

		$attribNamesVisible = [];
		foreach($attribNames as $aname) {
			if(in_array($aname, $this->hiddenAttribs))
					continue;
			
			$attribNamesVisible[] = $aname;
		}
		$attribNames = $attribNamesVisible;
		
		/* Ordering */
		for($i = 1; $i < count($attribNames); $i++) {
			$j = $i;
			while($j > 0) {
				$aobj1 = $this->cobj->getAttribute($attribNames[$j - 1]);
				$aobj2 = $this->cobj->getAttribute($attribNames[$j]);

				if( $aobj1->getDisplayOrder() > $aobj2->getDisplayOrder() ) {
					$tmp                 = $attribNames[$j - 1];
					$attribNames[$j - 1] = $attribNames[$j];
					$attribNames[$j]     = $tmp;
				}
				else {
					break;	
				}
				
				$j--;
			}
		}
		
		$attribLabels = [];
		$nextCustHead = 0;
		
		foreach($attribNames as $aname) {
			$aobj = $this->cobj->getAttribute($aname);

			/* $additionalHeaders can or cannot be an associative array */
			if(isset($additionalHeaders[$nextCustHead]['order'])) {
				while(isset($additionalHeaders[$nextCustHead]['order']) &&
					$additionalHeaders[$nextCustHead]['order'] < $aobj->getDisplayOrder())
				{
					$attribLabels[] = $additionalHeaders[$nextCustHead]['label'];
					$nextCustHead++;
				}
			}
		
			if(isset($this->customLabels[$aname])) {
				$attribLabels[] = $this->customLabels[$aname];
			}
			else {
				$attribLabels[] = $aobj->getDisplayLabel();
			}
		}

		/* Append the remaining custom headings */
		if($nextCustHead < count($additionalHeaders)) {
			for(; $nextCustHead < count($additionalHeaders); $nextCustHead++) {
				if(is_array($additionalHeaders[$nextCustHead]))
					$attribLabels[] = $additionalHeaders[$nextCustHead]['label'];
				else
					$attribLabels[] = $additionalHeaders[$nextCustHead];
			}
		}

		// TODO prepend id col
		$attribLabels = array_merge(($this->eidVisible)? ['ID']: [], $attribLabels);

		$html  = nan_table_start(); // tc - Tab Content
		$html .= nan_table_array_to_th($attribLabels); // TODO HTML-escape each element
		
		foreach($ids as $id) {
			$row = [];

			if($this->eidVisible)
				$row[] = $id;

			foreach($attribNames as $aname) {
				$e = new DecomEntity($this->className, $id);
			
				if($e->hasPropertyValue($aname)) {
					$val = $e->getPropertyValue($aname);
					if(is_array($val)) {
						$cellHtml = '';
					
						foreach($val as $v)
							$cellHtml .= htmlspecialchars($v).'<hr/>'; // TODO use li

						$row[] = $cellHtml;
					}
					else {
						$row[] = htmlspecialchars($val);
					}
				}
				else {
					$row[] = '<b>NULL</b>';
				}
			}
			
			if($rowcallback !== null) {
				if($fieldsToReturn === null) {
					$vals = $id;
				}
				else {
					$vals = ['__id' => $id];
					$eobj = new DecomEntity($this->className, $id);

					foreach($fieldsToReturn as $aname) {
						if($eobj->hasPropertyValue($aname))
							$vals[$aname] = $eobj->getPropertyValue($aname);
					}
				}
				
				$rowcallback($row, $vals);
			}
			
			$html .= nan_table_array_to_td($row);
		}
		
		$html .= nan_table_close();
		
		return $html;
	}
}

?>
