<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Made a separate file on 2019-07-07
 */

/**
 * @h1 Entity List View
 */

require_once($DELIBDIR.'/php/view.php');
require_once($DELIBDIR.'/php/query.php');

class DecomEntityListView extends DecomView {
	protected $cols = null;
	protected $condition = null;

	function __construct($className) {
		parent::load('class', $className);
	}
	
	function getEntityIds() {
		if($this->condition === null) {
			$ids = decom_get_entity_ids($this->className);
		}
		else {
			$q = new DecomQuery($this->className);
			$q->setCondition($this->condition);
			$q->setExpectedMatchingIds();
			$ids = $q->query();
		}
		
		return $ids;
	}
	
	// TODO doc can include non-existing attribs, which will be ignored.
	function setColumns($attribNames) {
		$this->cols = $attribNames;
	}
	
	/**
	 * @param condition An object of DecomQueryCondition
	 */
	function setCondition($condition) {
		// TODO validate?
		$this->condition = $condition;
	}
}

?>
