<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Made a separate file on 2019-07-02
 */

/**
 * @h1 Entity View
 */

require_once($DELIBDIR.'/php/view.php');

class DecomEntityView extends DecomView {
	// TODO uncomment once the migration is complete
	//protected $className;
	protected $eid;
	protected $autoLabel;
	protected $loadedClassData = false;
	protected $loadedObjData = false;
	//protected $cobj = null;
	//protected $eobj = null;
	protected $outfmt = 'html';
	protected $skipUnset; // Skip unset attributes (unset, not NULLs)
	protected $useLabels;
	protected $hiddenAttribs = [];
	protected $hiddenFormValues = []; /* Associative array */

	function __construct($className = null, $eid = null) {
		$this->load($className, $eid);
		
		/* Sets the default behaviour */
		$this->setUseLabels();
		$this->setSkipUnsetAttributes();
	}
	
	function getHiddenAttribs() {
		return $this->hiddenAttribs;
	}
	
	function getHiddenFormValues() {
		return $this->hiddenFormValues;
	}
	
	function getOutputFormat($fmt) {
		return $this->outfmt;
	}
	
	/**
	 * $className Set NULL to unload
	 * $eid Set NULL to unload
	 * $forceReloadClass Set TRUE to reload the data even if the class name has not changed since the last loading.
	 * $forceReloadEntity Set TRUE to reload the data even if the entity id has not changed since the last loading.
	 * @note Class reloading automatically force-reloads the entity, if any.
	 * @reterrtrue
	 */
	function load($className = null, $eid = null, $forceReloadClass = false, $forceReloadEntity = false) {
		$ret = $this->loadClassData($className, $forceReloadClass);
		if(decom_is_errobj($ret))
			return $ret;
		
		$ret = $this->loadEntityData($eid, $forceReloadEntity);
		if(decom_is_errobj($ret))
			return $ret;

		return true;
	}

	/**
	 * $className Set NULL to unload
	 * $forceReload Set TRUE to reload the data even if the class name has not changed since the last loading.
	 * @note Class reloading automatically force-reloads the entity, if any.
	 */
	function loadClassData($className = null, $forceReload = false) {
		if($className === null) {
			$this->unloadClassData();
		}
		else {
			if(($this->className !== $className) || $forceReload) {
				$this->cobj      = new DecomClass($className);
				$this->className = $className;

				$this->loadEntityData($this->eid, true);
			}
		}

		return true;
	}

	/**
	 * $forceReload Set TRUE to reload the data even if the class name and entity ID has not changed since the last loading.
	 */
	function loadEntityData($eid = null, $forceReload = false) {
		if($eid === null) {
			$this->unloadEntityData();
		}
		else {		
			if( ($this->eid !== null) && ($className === null))
				return new DecomError('Entity ID given without specifying the class name');
			
			/* $eobj->getClassName() means the current object's class, not that of the one that's going to be created */
			$isNewClass = ($this->eobj !== null) &&
				($this->eobj->getClassName !== $className);
			
			if($isNewClass || ($this->eid !== $eid) || $forceReload) {
				$this->eobj = new DecomEntity($this->className, $eid);
				$this->eid  = $eid;
			}
		}

		return true;
	}

	/**
	 * @reterr
	 */
	function render($outfmt = null) {
		// TODO consider $hiddenAttribs
	
		if($outfmt === null)
			$outfmt = $this->outfmt;

		if($this->eobj === null) {
			if($this->eid === null)
				return new DecomError('Entity ID not specified');
			else
				$this->load();
		}

		switch($outfmt) {
			case 'html': // TODO mix txt/csv with this when they are introduced in future. Common code.
				$html = '';

				// TODO div table
				$html .= '<div style="display: table">'; // TODO use class
				$cobj = new DecomClass($this->eobj->getClassName());
				$attribs = $cobj->getAttributes(); // TODO public only?
				foreach($attribs as $a) {
					$aname = $a->getName();
					if($this->useLabels)
						$alabel = $a->getDisplayLabel($this->autoLabel);
					else
						$alabel = $aname;
			
					if($this->eobj->hasPropertyValue($aname)) {
						$v = $this->eobj->getPropertyValue($aname);

						if(decom_is_errobj($v))
							return new DecomError($v);
						
						if($v === null) {
							// TODO check if this works
							$v = '<b>NULL</b>'; // TODO think how to show this in non-html mode?
						}
						else {
							if(is_array($v))
								foreach($v as $i => $val)
									$v[$i] = htmlspecialchars($val);
			
							if($a->getMaxInstances() != 1)
								$v = implode('<br/>', $v);
						}
					}
					else if($this->skipUnset === false) {
						$v = '<b>UNSET</b>'; // TODO think how to show this in non-html mode?
					}
					else {
						continue; // or the previous value will be shown
					}

					$html .= '<div style="display: table-row">';
					$html .= '<div style="display: table-cell">'.htmlspecialchars($alabel).': </div>'. // TODO enable customize ':'
						'<div style="display: table-cell">'.$v.'</div>';
					$html .= '</div>';
				}
				// TODO

				$html .= '</div>';

				return $html;
				break;
		}
	}
	
	function renderHtml() {
		/* TODO direct render */
		return $this->render('html');
	}

	function setHiddenAttribs($anames) {
		$this->hiddenAttribs = $anames;
	}
	
	function setHiddenFormValues($assoc) {
		$this->hiddenFormValues = $assoc;
	}

	/**
	 * @reterr
	 */	
	function setOutputFormat($fmt) {
		if(!in_array($fmt, ['html']))
			return new DecomError('Unknown rendering output format');
		
		$this->outfmt = $fmt;
	}

	/**
	 * Sets whether or not to skip attributes that are not set (NOTE: unset doesn't mean set to null).
	 * This function is called automatically be the constructor with the default parameters.
	 * You can call again to change the display behaviour.
	 * @param skip Set true to skip attributes that are not set
	 *
	 */
	function setSkipUnsetAttributes($skip = true) {
		$this->skipUnset = $skip;
	}

	/**
	 * Sets whether or not to display attribute labels instead of attribute names.
	 * This function is called automatically be the constructor with the default parameters.
	 * You can call again to change the display behaviour.
	 * @param useLabels Set true to display the field labels instead of field names
	 * @param autoLabel Set true to automatically labelify field names in case a label is not defined
	 *
	 */
	function setUseLabels($useLabels = true, $autoLabel = true) {
		$this->useLabels = $useLabels;
		$this->autoLabel = $autoLabel;
	}

	/*
	 * @note Class unloading automatically force-unloads the entity, if any.
	 */
	function unloadClassData() {
		if($this->eobj !== null)
			$this->unloadEntityData();
		
		$this->cobj = null;
		$this->className = null;
	}
	
	function unloadEntityData() {
		$this->eobj = null;
		$this->eid  = null;
	}
}

// TODO

?>
