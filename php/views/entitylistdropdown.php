<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Made a separate file on 2019-07-02
 */

/**
 * @h1 Entity List Dropdown View
 */

require_once($DELIBDIR.'/php/views/entitylist.php');

// TODO start index, end index
// TODO display parameters - render id? which field to show, ...
class DecomEntityListDropDownView extends DecomEntityListView {
	protected $showLabels = false; // TODO option to turn on?
	protected $name       = null;
	
	// TODO doc
	function setName($name) {
		$this->name = $name;
	}

	// TODO doc
	/**
	 * @reterr
	 */
	function render($htmlid = null, $htmlattribs = [], $nonelabel = null, $nonevalue = '', $default = null, $renderError = false) {
		$ids = $this->getEntityIds(); // TODO do not collect each time?
		if(decom_is_errobj($ids)) {
			if($renderError)
				return $ids->getMessageHtml();
			else
				return $ids;
		}

		if($this->cols) {
			$colsPossible = $this->cols;
			$anyonecol    = false;
		}
		else {
			$colsPossible = ['name', 'code', 'title'];
			$anyonecol    = true;
		}
		
		/* TODO feature to switch off labels */
		$cols      = [];
		$colLabels = [];
		foreach($colsPossible as $aname) {
			$aobj = $this->cobj->getAttribute($aname);
			if($aobj !== false && $aobj !== null) {
				$cols[] = $aname;
				$colLabels[$aname] = $aobj->getDisplayLabel();
				
				if($anyonecol)
					break;
			}
		}

		$opts = [];

		if($nonelabel) {
			$sel    = ($default && $default == $nonevalue)? ' selected': '';
			$opts[] = '<option value="'.$nonevalue.'"'.$sel.'>'.$nonelabel.'</option>';
		}

		foreach($ids as $id) {
			$sel = ($default !== null && $default == $id)? ' selected': '';

			$lbl = $id;			
			if(count($cols)) {
				$lbl = $id.' -';
			
				$eobj = new DecomEntity($this->className, $id);
				
				foreach($cols as $col) {
					if($eobj->hasPropertyValue($col)) {
						$colLabel = ($this->showLabels)? $colLabels[$col].': ': '';
					
						$lbl .= ' '.$colLabel.$eobj->getPropertyValueFirst($col); // TODO Is 'First' an overkill since unique attribs won't be multiinstance?  XXX but need not be unique or singleinstance always
					}
				}
			}
			
			$opts[] = '<option value="'.$id.'"'.$sel.'>'.$lbl.'</option>';
		}
		
		$html = '<select';
		if($htmlid !== null)
			$html .= ' id="'.$htmlid.'"';

		if($this->name !== null)
			$html .= ' name="'.$this->name.'"';

		foreach($htmlattribs as $a => $v) {
			$html .= ' '.$a.'="'.str_replace('"', '\"', $v).'"';
		}

		$html .= '>';
		$html .= implode('', $opts);
		$html .= '</select>';
		
		return $html;
	}
}

?>
