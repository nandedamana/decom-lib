<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-18
 */

require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
require_once($DELIBDIR.'/php/nan/table.php');

/**
 * @h1 View
 */

/**
 * @h2 DecomView Class
 */

class DecomView {
	protected $outfmt; // TODO doc permvalues: html, csv
	protected $className = null;
	protected $cobj = null;
	protected $eobj = null;

	function __construct($outfmt = 'html') {
		$this->outfmt = $outfmt;
	}
	
	/**
	 * @param type 'entity' or 'class'
	 * @param eid Required only for type='entity'
	 */
	function load($type = 'entity', $className = null, $eid = null) {
		/* This resetting is needed each time this method is called. */
		$this->className = $className;
		$this->cobj = null;
		$this->eobj = null;
		
		if($eid) {
			// TODO complete the migration
			global $DELIBDIR;
			require_once($DELIBDIR.'/php/internal/warn.php');
			delib_warn_use_instead("DecomEntityView(\$className, \$eid)", "DecomView::load('entity', \$className, \$eid)", 'usage');
		}
		
		switch($type) {
		case 'class':
			$this->cobj = new DecomClass($className);
			break;
		case 'entity':
			$this->cobj = new DecomClass($className);
			$this->eobj = new DecomEntity($className, $eid);
			break;
		default:
			throw new Exception('Invalid type in DecomView::load()'); // TODO make method name not hardcoded.
		}
		// TODO
	}
	
	// TODO FIXME
	// abstract function render();
}
?>
