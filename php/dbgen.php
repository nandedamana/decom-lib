<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-09
 */

/**
 * @h1 Database Self-generation and Healing
 */

require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/error.php');
require_once($DELIBDIR.'/php/limits.php');
require_once($DELIBDIR.'/php/nan/sql.php');

// Made an array so that this can be used while db health check.
// TODO Why eav_ameta? It should be named ameta_
$DE_eavAmetaCols = [
//[ name             dtype                     nullable  default]
	[ 'name',          'varchar(32)',            false,       null  ],
	[ 'id',            'int',                true,       'NULL' ],
// XXX enum disabled in favour of SQLite. But I need some sort of standardization for `type`.
//	[ 'type',          'enum(\'int\',\'text\')', false,       null  ],
	[ 'type',          'text',                   false,       null  ],
	[ 'description',   'varchar(1024)',          true,       'NULL' ],
	[ 'cons_pk',       'tinyint(1)',             true,       'NULL' ], // TODO rem?
	[ 'cons_uniq',     'tinyint(1)',             true,       'NULL' ],
	[ 'cons_notnull',  'tinyint(1)',             true,       'NULL' ], // TODO rem
	[ 'requiredness',  'tinyint',                true,       '0'    ],
	[ 'max_instances', 'int',                    false,      '1'    ],
	[ 'display_label', 'varchar(256)',           true,       'NULL' ],
	[ 'display_order', 'int',                    false,      '0'    ],
	[ 'display_type',  'CHECK( display_type IN(\'bool\',\'multiline\') )',
	                                             true,       'NULL' ]
];

$DECLASSES_BUILT_IN = [
	'site' => [],
	'sitepage' => [],
];

/* TODO move based on alpha order */
// TODO what to return?
function decom_db_table_add_missing_columns($tableName, $cols) {
	global $DEDBLINK, $DEDBNAME;

	foreach($cols as $colr) {
		$colname = &$colr[0];
	
		if(!decom_db_table_has_column($tableName, $colname)) {
			decom_db_exec('alter table '.$tableName.' add column '.nan_sql_colspec_from_colrec($colr));
			// TODO err
		}
	}
}

// TODO function to check table keys

/* TODO move based on alpha order */
function decom_db_table_has_column($tableName, $colName) {
	global $DEDBLINK, $DEDBNAME;

	$ret = decom_db_query_array(
		'select * from information_schema.columns where table_schema='.$DEDBLINK->quote($DEDBNAME).
		' and table_name='.$DEDBLINK->quote($tableName).
		' and column_name='.$DEDBLINK->quote($colName));
	
	// TODO err
	
	if($ret === null || sizeof($ret) == 0)
		return false;
	else
		return true;
}

/* TODO move based on alpha order */
function decom_db_table_has_columns($tableName, $cols) {
	global $DEDBLINK, $DEDBNAME;

	foreach($cols as $colr) {
		$colname = &$colr[0];
	
		if(!decom_db_table_has_column($tableName, $colname))
			return false;
	}

	return true;
}

// TODO move to top
// TODO limits from global variabels/constants; enforce these limits in the adminpanel.
// XXX An auto-indentation program can cause issues here if it converts spaces to tabs.
$DE_essentialTablesSql = [ 'classes' => 'CREATE TABLE `classes` (
    `class`  varchar(64) NOT NULL,
    `parent` varchar(64) DEFAULT NULL,
    PRIMARY KEY(`class`))',

	/* XXX This is the output of MySQL SHOW CREATE TABLE modified to be compatible with SQLite3. */
	'login' => "CREATE TABLE `login` (
    `uname` varchar(32) NOT NULL,
    `halgo` varchar(16) NOT NULL,
    `salt` varchar(16) NOT NULL DEFAULT '',
    `passwd` varchar(512) DEFAULT NULL,
    `owner_eid` int DEFAULT NULL,
    `owner_class` varchar(64) DEFAULT NULL,
    PRIMARY KEY (`uname`),
    UNIQUE (`owner_class`,`owner_eid`),
    FOREIGN KEY (`owner_class`) REFERENCES `classes` (`class`))"
];

// TODO doc lowlevel, className not validated
// TODO doc returns true, false, DecomError
function decom_db_class_implementation_table_exists($className, $tableType) {
	switch($tableType) {
		case 'eavAmeta':
			return decom_db_table_exists('eav_ameta_'.$className);
			break; // explicit break is good to prevent future mistakes when the return statement is removed
		case 'eav':
		case 'privfiles':
		case 'hier':
			return decom_db_table_exists($tableType.'_'.$className);
			break;
		default:
			return new DecomError('Unknown tableType given to check the existence of a class implementation table');
	}
}

// TODO doc lowlevel, className not validated
function decom_db_create_class_implementation_table($className, $tableType, $ifNotExists = false) {
	global $DEDBLINK, $DE_eavAmetaCols;

	switch($tableType) {
		case 'eavAmeta':
			$tablePrefix = 'eav_ameta';
			break;
		case 'eav':
		case 'hier':
		case 'privfiles':
			$tablePrefix = $tableType;
			break;
		default:
			return new DecomError('Unknown tableType given for the creation of a class implementation table');
	}

	/* I could've used 'CREATE TABLE IF NOT EXISTS' SQL, but it isn't that portable. */
	if($ifNotExists) {
		$ret = decom_db_class_implementation_table_exists($className, $tableType);
		if(decom_is_errobj($ret) || $ret === true) // Error or table exists
			return $ret;
	}

	switch($tableType) {
		case 'eavAmeta':
			$retval = decom_db_exec("CREATE TABLE `eav_ameta_$className` (
          ".nan_sql_collist_from_colarr($DE_eavAmetaCols).",
          PRIMARY KEY (`name`),
          UNIQUE (`id`)
        )");
			break;
		case 'eav':
			$retval = decom_db_exec("CREATE TABLE `eav_$className` (
          `id` int NOT NULL,
          `attrib` int NOT NULL,
          `ival` int DEFAULT NULL,
          `tval` text,
          `value_instance` int DEFAULT 0,
          PRIMARY KEY (`id`, `attrib`, `value_instance`)
        )");
			break;
		case 'hier':
			$retval = decom_db_exec("CREATE TABLE `hier_$className` (
          `parent` int DEFAULT NULL,
          `child` int,
          PRIMARY KEY (`child`)
        )");
			break;
		case 'privfiles':
			$retval = decom_db_exec("CREATE TABLE `privfiles_$className` (
          `eid` int,
          `filename` varchar(".DEMAXLEN_PRIVFILENAME."),
          PRIMARY KEY (`eid`, `filename`)
        )");
			break;
		// XXX No default case since checked already
	}

	return $retval;
}

function decom_db_create_essential_table($tableName) {
	global $DE_essentialTablesSql;

	if(!isset($DE_essentialTablesSql[$tableName]))
		return new DecomError('Unknown essential table.');
	
	return decom_db_exec($DE_essentialTablesSql[$tableName]);
}

function decom_db_create_built_in_class($className) {
	global $DECLASSES_BUILT_IN;

	if(!isset($DECLASSES_BUILT_IN[$className]))
		return new DecomError('Unknown built-in class.');
	
	// TODO take parent and attribs from $DECLASSES_BUILT_IN
	return decom_create_class($className);
}

/**
 * @experimental
 */
function decom_db_fix_all() {
	global $DE_eavAmetaCols;

	$classes = decom_db_get_class_names();
	
	// TODO essential tables
	
	foreach($classes as $c) {
		$ret = decom_db_table_add_missing_columns('eav_ameta_'.$c, $DE_eavAmetaCols);
		if(decom_is_errobj($ret))
			return $ret;
	
		foreach(['eav', 'eavAmeta', 'hier'] as $ttype) { /* TODO make the type array global */
			$ret = decom_db_create_class_implementation_table($c, $ttype, true);
			if(decom_is_errobj($ret))
				return $ret;
		}
	}
// TODO

	return true;
}

/**
 * Returns the names of built-in tables as an array
 */
function decom_db_get_builtin_classes() {
	global $DECLASSES_BUILT_IN;
	
	return array_keys($DECLASSES_BUILT_IN);
}

/**
 * Returns the names of essential tables as an array
 */
function decom_db_get_essential_tables() {
	global $DE_essentialTablesSql;

	return array_keys($DE_essentialTablesSql);
}
?>
