<?php
/* querycondition.php
 * Copyright (C) 2019 Nandakumar Edamana
 * Code copied from query.php which was started on 2019-07-12
 */

/**
 * @h1 Query Condition
 */

// TODO rem unwanted
require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/class.php');

// TODO doc != select unset instances also.
class DecomQueryCondition {
	protected $conditions;
	
	// TODO doc
	// Each element of the array $conditions is of the form [ATTRNAME, OPTR, VALUE] or a non-array element like 'and', 'or'.
	function __construct($conditions = null) {
		$this->conditions = $conditions;
	}
	
	function toSql($cobj) {
		global $DEDBLINK;
	
		$sql = '';
		// TODO
		if(count($this->conditions)) {
			if(count($this->conditions) > 1) {
				die('Multiple conditions still unsupported.');
				// TODO FIXME
			}
			
			foreach($this->conditions as $cond) {
				if(is_a($cond, 'DecomQueryCondition')) {
					$sql .= ' '.$cond->toSql();
				}
				else if(in_array($cond, ['and', 'or'])) { // TODO support in
					$sql .= ' '.$cond.' ';
				}
				else if(is_array($cond) && count($cond) == 3) {
					$aname = &$cond[0]; // TODO make sure a string, a valid aname
					$optr  = &$cond[1];
					$value = &$cond[2];
					
					// TODO make sure $optr is str
					if(!in_array($optr, ['=', '!=', '<', '<=', '>', '>=', 'like'])) {
						throw new Exception('Unsupported query operator.'); // TODO
					}
					
					// TODO lock db or assign this to SQL?
					$aobj = $cobj->getAttribute($aname);
					$aid  = $aobj->getId();
					$avfield = ($aobj->getType() == 'int')? 'ival': 'tval';
					
					$aidq = $DEDBLINK->quote($aid);
					$valq = $DEDBLINK->quote($value); // TODO make sure $value is a str
					
					/* XXX EAV will have zero entries for an unset attribute, which means != should be handled in a different manner. */
					if($optr == '!=') {
						$cname = $cobj->getName();

						// TODO FIXME does this work for all query types?
						$sql .= "id not in (select distinct id from eav_$cname where attrib=$aidq) or id in (select distinct id from eav_$cname where attrib=$aidq and $avfield != $valq)";
					}
					else {
						$sql .= "attrib=$aidq and $avfield $optr $valq";
					}
				}
				else {
					throw new Exception('Unsupported query conditions.');
				}
			}
		}
		
		return $sql;
	}
}
?>
