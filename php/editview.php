<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-11
 */

/**
 * @h1 Entity Edit View (deprecated file)
 */

/**
 * @deprecated
 */

require_once($DELIBDIR.'/php/internal/warn.php');
require_once($DELIBDIR.'/php/views/entityedit.php');

delib_warn_include_instead('php/views/entityedit.php', 'php/editview.php');
?>
