<?php
/* utility.php
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-06
 */

/**
 * @h1 Utility Functions
 */

require_once($DELIBDIR.'/php/error.php');

/**
 * Shorthand for ($val !== null && $val !== false && !decom_is_errobj($val))
 */
function decom_is_value($val) {
	return ($val !== null && $val !== false && !decom_is_errobj($val));
}
?>
