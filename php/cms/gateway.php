<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-18
 */

/**
 * @h1 Single-entry Page Serving
 */

require_once($DELIBDIR.'/php/error.php');

/**
 * This function is useful if you are planning to serve all pages in your site via
 * a single file (say, index.php), based on some parameter (say, $_GET['page']).
 * @param pageParam Identifier of the page/file to serve (e.g.: $_GET['page']). This is accepted as reference.
 * @param delimiter This character/string in $pageParam gets replaced by slash to convert it into a filepath
 * @param incext Extension of the include files
 * @param incdir Where the include files are stored. Be sure not to save such files inside your document root.
 * @param injectionFilter Array of substrings that indicate an injection attack
 * @returns Array of paths, in which each element can be used with include(), include_once(), etc. TODO doc why array
 * @note This function doesn't execute include() by itself because if it does so, the included code will have a scope local to this function.
 * @warning TODO incdir and pathParam shouldn't contain double delimiters?
 */
 // TODO doc _top.php and _bottom.php
 // TODO check the presence of json file to improve security?
 // TODO doc delimiter can be array; but for safety, each element should be a single char (because of str_replace($del, $del[0], $page))
 // TODO doc param breadbar
 // TODO return values to title and breadbar
 // TODO doc $breadUrlPrefix
 // TODO doc stockindexf
 // TODO doc _forbidden (_forbidden can be a file or dir, buf file preferred)
 // TODO doc pageid cannot contain .. at all (e.g. 'a..b' is not allowed even if it is not an injection attack) if $injectionFilter = ['..'] (['../', '/..'] won't be a solution since one can try use attack string 'something..' or '..something' then)
 // TODO rem unused param $includeOnce (also others if any)
 // TODO check why the not in the comment of this function doesn't make into the doc.
function decom_autoinclude(&$pageParam, $incdir = '../include', $delimiter = '/', $incext = '.php', $injectionFilter = ['..'], $includeOnce = false, &$title = null, &$breadbar = null, $breadUrlPrefix = '') {
	if(!isset($pageParam))
		return new DecomError('Page parameter not set.');

	// TODO check if I can detect the presence more efficiently.
	foreach($injectionFilter as $is)
		if(strpos($pageParam, $is) !== false)
			return new DecomError('Injection detected.');

	// TODO doc: recursive and non-recursive common tops and bottoms.
	// TODO test recursive and non-recursive common tops and bottoms.

	$retPathsTop    = [];
	$retPathsBottom = [];

	if(is_array($delimiter)) {
		// Replace all delimiters with the first one.
		$pageRep = str_replace($delimiter, $delimiter[0], $pageParam);

		// Now explode
		$pathParts = explode($delimiter[0], $pageRep);
		
		$delimFirst = $delimiter[0]; // For later use
	}
	else {
		$pathParts = explode($delimiter, $pageParam);
		$delimFirst = $delimiter;
	}

	$lastPath = $incdir.'/'.implode('/', $pathParts);
	if(is_dir($lastPath))
		array_push($pathParts, 'index');

	if($breadbar !== null)
		$urlUntil = $breadUrlPrefix;

	/* The following loop is also responsible for including common tops and bottoms from each path component */
	$pathUntil = $incdir;
	foreach($pathParts as $i => $part) {
		if(file_exists($pathUntil.'/_forbidden'))
			throw new Exception('Trying to access a forbidden path.');
	
		$rtPath = $pathUntil.'/__top'.$incext; // Recursive common top
		if(file_exists($rtPath))
			$retPathsTop[] = $rtPath;

		if($i == sizeof($pathParts) - 1) { // This is the final iteration
			$fpath   = $pathUntil.'/'.$part.$incext;
			$nrtPath = $pathUntil.'/_top.php';    // Non-recursive common top
			$nrbPath = $pathUntil.'/_bottom.php'; // Non-recursive common top
			
			$ffound = true;
			if(!is_file($fpath) || !is_readable($fpath)) {
				if($part == 'index') {
					$stockindexf = "$incdir/__stockindex$incext";
					if(!is_file($stockindexf) || !is_readable($stockindexf))
						$ffound = false;
					else
						$fpath = $stockindexf;
				}
				else {
					$ffound = false;
				}
			}

			if(!$ffound)
				return new DecomError('File does not exist or is not accessible.');

			if(is_file($nrtPath) && is_readable($nrtPath))
				$retPathsTop[] = $nrtPath; // Append

			if(is_file($nrbPath) && is_readable($nrbPath))
				array_unshift($retPathsBottom, $nrbPath); // Prepend
			
			$jsonfpath = $pathUntil.'/'.$part.'.json';
			if(file_exists($jsonfpath)) {
				$jsonarr = json_decode(file_get_contents($jsonfpath), true);
				if($title !== null)
					$title = $jsonarr['title'];
			}
		}

		$rbPath = $pathUntil.'/__bottom'.$incext; // Recursive common bottom
		if(file_exists($rbPath))
			array_unshift($retPathsBottom, $rbPath); // Prepend

		if($breadbar !== null && $part != 'index') {
			if($urlUntil == $breadUrlPrefix) // First part
				$urlUntil .= $part;
			else
				$urlUntil .= $delimFirst.$part;

			$pathUntilDOTpart = $pathUntil.'/'.$part;

			if(is_dir($pathUntilDOTpart))
				$jsonfpath = $pathUntil.'/'.$part.'/index.json';
			else
				$jsonfpath = $pathUntil.'/'.$part.'.json';


			if(is_file($jsonfpath) && is_readable($jsonfpath)) {
				$jsonarr = json_decode(file_get_contents($jsonfpath), true);
				$stitle = isset($jsonarr['shortTitle'])?
					$jsonarr['shortTitle']:
					$jsonarr['title'];
				
				if(!isset($jsonarr['skipmenu']) || $jsonarr['skipmenu'] == false)
					$breadbar->appendCrumb(_($stitle), $urlUntil); // TODO FIXME
			}
		}

		$pathUntil .= '/'.$part;
	}

	return array_merge($retPathsTop, [$fpath], $retPathsBottom);	
}

/* TODO move to another file? */
// TODO doc
// TODO warn may return null
function decom_menu_from_incdir($incdir, $urlprefix = '?p=', $delimiter = '/', $useShortTitle = true) {// TODO rem $i
	global $DELIBDIR;
	require_once($DELIBDIR.'/php/menu.php'); // TODO move or remove

	$mitems = [];

	$dir = opendir($incdir);
	while( ($f = readdir($dir)) ) {
		if(in_array($f, ['.', '..']))
			continue;
	
		$fpath  = $incdir.'/'.$f;
		$fisdir = is_dir($fpath);

		if($fisdir)
			$jsonfpath = $fpath.'/index.json';
		else if(preg_match('/\.json$/', $f) && $f != 'index.json')
			$jsonfpath = $fpath;
		else
			continue;

		if(is_file($jsonfpath) && is_readable($jsonfpath)) {
			$fcode = $fisdir?
				$f:
				substr($f, 0, strlen($f) - 5); // Drop the extension;

			$url  = $urlprefix.$fcode;

			$subm = null;
			if($fisdir)
				$subm = decom_menu_from_incdir($fpath, $url.$delimiter, $delimiter); // TODO if the menu contains no items, make $subm null

			$jsonarr = json_decode(file_get_contents($jsonfpath), true);
			if(json_last_error() != JSON_ERROR_NONE)
				return new DecomError('Could not decode page metadata JSON');

			if(isset($jsonarr['skipmenu']) && $jsonarr['skipmenu'] == true)
				continue;

			$index = isset($jsonarr['index'])? $jsonarr['index']: 0;
			while(isset($mitems[$index]))
				$index++;

			if($useShortTitle)
				$stitle = isset($jsonarr['shortTitle'])?
					$jsonarr['shortTitle']:
					$jsonarr['title'];
			else
				$stitle = $jsonarr['title'];

			$mitems[$index] =
				new DecomMenuItem(
					_($stitle), $url, _($jsonarr['description']), $subm);
		}
	}

	ksort($mitems);

	if(sizeof($mitems) > 0) {
		$mnu = new DecomMenu();

		foreach($mitems as $item)
			$mnu->addItem($item);
			
		return $mnu;
	}
	else {
		return null;
	}
}
?>
