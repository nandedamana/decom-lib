<?php
/* query.php
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-12
 */

/**
 * @h1 Querying
 */

// TODO rem unwanted
require_once($DELIBDIR.'/php/db.php');
require_once($DELIBDIR.'/php/class.php');

class DecomQuery {
	protected $cobj;
	protected $expected = null;
	protected $given    = null;
	
	function __construct($className) {
		$this->cobj = new DecomClass($className); // This will take care of errors with $className
	}
	
	/**
	 * @param expected An array of the expected return fields; each element an array in the form of [$type, $name] where $type can be 'a' for attribute and 'g' for aggregation.
	 */
	function setExpected($expected) {
		// TODO validate
		$this->expected = $expected;
	}
	
	function setExpectedAllAttributes() {
		$this->expected = '*';
	}
	
	/**
	 * @param attribNames Array of names of the expected attributes
	 */
	function setExpectedAttributes($attribNames) {
		$this->expected = [];

		foreach($attribNames as $aname) {
			// TODO validate attribute name (throw or err?)
		
			$this->expected[] = ['a', $aname];
		}
		
		return true;
	}
	
	/**
	 * If called, the value returned by query() will be an array of IDs of all matching entities.
	 */
	function setExpectedMatchingIds() {
		$this->expected = '#';
	}
	
	/**
	 * @param condition An object of DecomQueryCondition
	 */
	function setCondition($condition) {
		// TODO validate
		$this->given = $condition;
	}
	
	function query() {
		$exp = ($this->expected !== null)? $this->expected: '#';
		
		$sql = 'select ';
		
		if($exp == '#') {
			$sql .= 'distinct id';
		}
		else {
			die('Unsupported query type.');
			// TODO FIXME
		}
		
		$sql .= ' from eav_'.$this->cobj->getName();
		
		if($this->given)
			$sql .= ' where '.$this->given->toSql($this->cobj);

		$rs = decom_db_query($sql);
		if(decom_is_errobj($rs))
			return $rs;
			
		$res = [];
		if($exp = '#') {
			foreach($rs as $r)
				$res[] = $r['id'];
		}
		else {
			// TODO FIXME
			die('Implement non-id-selection queries');
		}

		return $res; // TODO return results
	}
}
?>
