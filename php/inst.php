<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-25
 */

/**
 * @h1 Institute
 */

require_once($DELIBDIR.'/php/entity.php');
require_once($DELIBDIR.'/php/db.php'); /* TODO rem if included via previous includes */

class DecomInstitute extends DecomEntity {
	/**
	 * @notice This doesn't create a new institute in the database. Use decom_create_entity() for that.
	 * @nonExisting
	 */
	function __construct($id, $nonExisting = false, $loadData = true) {
		parent::__construct('institute', $id, $nonExisting, $loadData);
	}
  /* TODO */
}

/**
 * @h2 Functions
 */

/**
 * Returns an array of ids of all institutes
 */
function decom_get_institute_ids() {
	return DecomEntity::getIds('institute');
}
?>
