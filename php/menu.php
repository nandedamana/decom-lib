<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-03-26
 */

/**
 * @h1 Menu
 */

/**
 * @h2 DecomMenuItem class
 */
class DecomMenuItem {
	protected $hint;
	protected $href;
	protected $submenu;
	protected $text;

	function DecomMenuItem($text, $href, $hint, $submenu = null) {
		$this->text = $text;
		$this->href = $href;
		$this->hint = $hint;
		$this->submenu = $submenu;
	}
	
	function getHint() {
		return $this->hint;
	}
	
	function getHref() {
		return $this->href;
	}
	
	function getSubmenu() {
		return $this->submenu;
	}
	
	function getText() {
		return $this->text;
	}
	
	/**
	 * @param menu Instance of DecomMenu
	 */
	function setSubmenu($menu) {
		$this->submenu = $menu;
	}
}

/**
 * @h2 DecomMenu class
 */
class DecomMenu {
  protected $items = array();

  function toHtml($ulclass = null, $level = 0) {
  	$cattr = $ulclass? $ulclass.' ': '';
  	$cattr .= '_deMenuLevel_'.$level;
  	$html = "<ul class = \"$cattr\">";
    
    foreach($this->items as $item) {
    	$html .= '<li><a href="'.$item->getHref().'" title="'.$item->getHint().'">'.$item->getText().'</a>';

    	if( null !== ($subm = $item->getSubmenu()) )
    		$html .= $subm->toHtml('submenu', $level + 1);
    	
    	$html .= '</li>';
    }
    
    $html .= '</ul>';
    return $html;
  }

	/**
	 * @param item Instance of DecomMenuItem
	 */
  function addItem($item) {
		array_push($this->items, $item);
  }

	function getItemCount() {
		return sizeof($this->items);
	}

  /* TODO */
}
?>
