<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-05
 */

/**
 * @h1 Project Management
 */

if(!isset($DELIBDIR))
  die('decom-lib Error: You haven\'t set $DELIBDIR');

const DEDBDRV_MYSQL  = 0;
const DEDBDRV_SQLITE = 1;
  
require_once($DELIBDIR.'/php/error.php');

if(!isset($DEDIEONERR))
	$DEDIEONERR = true; /* Will be set to false once the project is loaded. */

const DEPROJNAME_PATTERN = '/^[a-zA-Z0-9\_\-]+$/';
/* Description kept in the library so that the changes will reflect in applications. */
const DEPROJNAME_PATTERN_DESC = 'English alphabets, digits, hyphen and underscore';

const DEPROJFILE_BASENAME = 'deproject.json';

/* Paths relative to the project directory */
const DEPROJ_RELPATH_PRIVFILES = 'defiles-private';

/* Absolute URL Prefix */
$DECSSBASE = '/css';
$DEJSBASE  = '/js';
$DETHEME   = 'lalitham';

class DecomProject {
	private $name;
	private $projdir;	
	private $uuid;
	
	function __construct($name, $uuid, $projdir = null) {
		$this->name    = $name;
		$this->uuid    = $uuid;
		$this->projdir = $projdir;
	}
	
	function toJson() {
		return json_encode( [
			'name' => $this->name,
			'uuid' => $this->uuid
		] );
	}
}

/**
 * Checks if the give path might be a libdecom project directory.
 * Returns TRUE or FALSE.
 */
function decom_is_project_dir($path) {
	$deprojfileGuess = $path.'/'.DEPROJFILE_BASENAME;

	return (is_file($deprojfileGuess));
}

// TODO move to utility.php
/**
 * Creates a directory and returns true after verifying it exists and is readable. The suffix 'r' stands for 'recursive'.
 * @notice This function does not return any error if the directory already exists.
 */
function decom_mkdir_r($path, $mode = 0700) {
	if(!file_exists($path))
		mkdir($path, $mode, true);

	return (is_dir($path) && is_readable($path));
}

function decom_project_subdir_get_fullpath($subdir) {
	global $DEPROJDIR;
	
	return $DEPROJDIR.'/'.$subdir;
}

/* Set the path variables given the project root. */
/* TODO doc this will reset $DEDIEONERR */
function decom_select_project($projdir, $dieOnError = true) {
	global $DEPROJDIR, $DEPROJDIR_SITEDATA, $DEPROJFILE,
		$DEPROJNAME, $DEPROJUUID,
		$DEDBLINK,
		$DEDBDRIVER,
		$DEDBHOST, $DEDBUNAME, $DEDBPWD, $DEDBNAME,
		$DEDBFILE,
		$DEDIEONERR;

	$DEDIEONERR = $dieOnError;

	if(false === decom_is_project_dir($projdir))
		return new DecomError('Invalid project directory.');

	$DEPROJDIR  = $projdir;
	$DEPROJFILE = $DEPROJDIR.'/'.DEPROJFILE_BASENAME;
	$DEPROJDIR_SITEDATA = $DEPROJDIR.'/sitedata';
	
	if(!is_file($DEPROJFILE) || !is_readable($DEPROJFILE))
		return new DecomError('Project metadata file does not exist: '.$DEPROJFILE);

	$jsonarr = json_decode(file_get_contents($DEPROJFILE), true);
	if(!is_array($jsonarr))
		return new DecomError('Project metadata file corrupt: '.$DEPROJFILE);

	$params = [
		'name'   => 'DEPROJNAME',
		'uuid'   => 'DEPROJUUID',
		'dbhost' => 'DEDBHOST',
		'dbuser' => 'DEDBUNAME',
		'dbpass' => 'DEDBPWD',
		'dbname' => 'DEDBNAME',
		'dbfile' => 'DEDBFILE'
	];

	foreach($params as $srckey => $destvar)
		if(isset($jsonarr[$srckey]))
			$$destvar = $jsonarr[$srckey];
		else
			unset($$destvar);

	if(!isset($DEPROJNAME, $DEPROJUUID))
		return new DecomError('Project name or UUID not set');

	if(!isset($DEDBDONTCONNECT) || $DEDBDONTCONNECT !== true) {
		if(isset($DEDBFILE) || isset($DEDBHOST, $DEDBUNAME, $DEDBPWD, $DEDBNAME)) {
		  try {
		  	if(isset($DEDBFILE)) {
		  		if($DEDBFILE[0] != '/')
						$DEDBFILE = dirname($DEPROJFILE).'/'.$DEDBFILE;

				  $DEDBLINK = new PDO('sqlite:'.$DEDBFILE);
				  $DEDBDRIVER = DEDBDRV_SQLITE;
				}
		  	else {
			    $DEDBLINK = new PDO('mysql:host='.$DEDBHOST.';dbname='.$DEDBNAME,
			      $DEDBUNAME,
			      $DEDBPWD);
				  $DEDBDRIVER = DEDBDRV_MYSQL;
			  }
		  }
		  catch(Exception $e) {
		    unset($DEDBLINK);
		    $DEDBCONNERR = $e->getMessage();
		  }
		}
	}

	$DEDIEONERR = false;
	
	return true;
}

function decom_this_project_get_dir() {
	global $DEPROJDIR;
	
	return $DEPROJDIR;
}

function decom_this_project_get_sitedatadir() {
	global $DEPROJDIR_SITEDATA;
	
	return $DEPROJDIR_SITEDATA;
}

function decom_validate_project_name($name) {

	return (0 != preg_match(DEPROJNAME_PATTERN, $name));
}
?>
