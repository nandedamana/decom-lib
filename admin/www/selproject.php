<?php
/* selproject.php - Select which project to manage
 * This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27
 */

require_once('../include/www-commontop.php');
require_once($DELIBDIR.'/php/nan/form.php');

if(isset($_POST['projdir']) && (trim($_POST['projdir']) != '')) {
	$_SESSION = [];
	session_start();

	$_SESSION['projdir'] = 
		str_replace('..', '', $_POST['projdir']);
	
	header('Location: index.php');
	exit;
}

session_start();

$content .= '<h2>'._('Project Selection').'</h2>';
$content .= '<p>'._('Please enter the absolute path of your libdecom project directory.').'</p>';

if(isset($_SESSION['projdir']) && !decom_is_project_dir($_SESSION['projdir']))
	$content .= '<p>'._('The project directory that is currently set in the session is probably not a libdecom project directory:').'<br/>'.htmlspecialchars($_SESSION['projdir']).'</p>';

$content .= '<p>'._('NOTE: In case this page wasn\'t intentionally opened by you, it was because you didn\'t set $DEPROJDIR, which means libdecom Control Panel doesn\'t know which project you are trying to manage. You can set it permanently in your source code (or configuration files) to avoid entering the path each time.').'</p>';

$inputFields      = ['projdir'];
$inputFieldLabels = [_('Project Directory')];
$inputFieldTypes  = ['text'];
$inputFieldsReq   = [true];
$action = '';
$method = 'POST';

$content .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method);

require_once('../include/www-commonbottom.php');
?>
