// TODO REM if not of much use or defined earlier
function $(id) {
	return document.getElementById(id);
}

function $c(className) {
	return document.getElementsByClassName(className);
}

var txtOut       = null;
var outCodeLines = []; // Made line array to control indentation easily

function show_code() {
	code = '';

	for(i = 0; i < outCodeLines.length; i++)
		code += outCodeLines[i] + "\n";

	txtOut.value = code;
}

document.addEventListener('DOMContentLoaded', function() {
	txtOut = $('txtOut');
	
	incFiles = [
		['Authentication', 'people/auth.php'],
		['Class', 'class.php'],
		['Entity', 'entity.php'],
		['Person', 'people/person.php']]; // TODO more

	for(i = 0; i < incFiles.length; i++) {
		var chkid = 'chk_incFile_' + incFiles[i][0];

		var chk = document.createElement('input');
		chk.setAttribute('id', chkid);
		chk.type = 'checkbox';

		var lbl = document.createElement('label');
		lbl.innerHTML = incFiles[i][0];
		lbl.setAttribute('for', chkid);

		$('incList').appendChild(chk);
		$('incList').appendChild(lbl);
		$('incList').appendChild(document.createElement('br'));
	}

	$('selCodeType').addEventListener('change', function() {
		var selval = $('selCodeType').value;

		var panes = $c('codeOptGroupHideable');
		for(i = 0; i < panes.length; i++)
			panes[i].style.display = 'none';

		if(selval != '')
			$('codeOptGroup_' + selval).style.display = '';
	});

	$('btnSelectAll').addEventListener('click', function() {
		txtOut.select();
	});
	
	$('btnCopy').addEventListener('click', function() {
		document.execCommand('copy');
	});

	$('btnClear').addEventListener('click', function() {
		outCodeLines = [];
		show_code();
	});

	$('btnGen').addEventListener('click', function() {
		outCodeLines = [];
		
		if($('chkPhpClosure').checked == 1) // TODO FIXME false case not working?
			outCodeLines.push('<?php');
		
		outCodeLines.push("require_once('delocalconf.php');"); // TODO make this optional
			
		for(i = 0; i < incFiles.length; i++)
			if($('chk_incFile_' + incFiles[i][0]).checked)
				outCodeLines.push("require_once($DELIBDIR.'/php/" + incFiles[i][1] + "');");
		
		codetype = $('selCodeType').value;
		switch(codetype) {
		case 'mkobj': // TODO
			// TODO
			// TODO can I join as an array? Can I avoid so many push() calls?
			outCodeLines.push("");
			outCodeLines.push("try {"); // TODO make customizeable
			outCodeLines.push("\t$obj = new DecomEntity('CLASSNAME', ID);"); // TODO make customizeable
			outCodeLines.push("\t// TODO Code to deal with the newly created object"); // TODO make customizeable
			outCodeLines.push("}"); // TODO make customizeable
			outCodeLines.push("catch(Exception $e) {"); // TODO make customizeable
			
			if($('chkMkobjErrMsg').checked == 1)
				outCodeLines.push("\tdecom_page_add_error_message($e->getMessage(), 'Error');"); // TODO make customizeable, make sure code works
			else
				outCodeLines.push("\t// TODO"); // TODO make customizeable

			outCodeLines.push("}"); // TODO make customizeable
			
			break;

		default:
			// TODO
		}

		if($('chkPhpClosure').checked == 1) // TODO FIXME false case not working?
			outCodeLines.push('?>');
		
		show_code();
	});
	
	$('selCodeType').dispatchEvent(new Event('change'));
});

// TODO
