<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27
 */

require_once('../include/www-commontop.php');

/* XXX The Control Panel directory ($DELIBDIR/admin) is expected to be
 * linked from $DEPROJDIR/admin of a given project.
 */

/* This config file can be used by projects to inject variables */
if(is_file('../config.php') && is_readable('../config.php'))
	include('../config.php');

if(!isset($DEPROJDIR)) {
	session_start();
	if(isset($_SESSION['projdir'])) {
		$DEPROJDIR = $_SESSION['projdir'];
	
		if(!decom_is_project_dir($DEPROJDIR)) {
			/* Let the user specify the correct project directory */
			header('Location: selproject.php');
			exit;
		}
	}
	else {
		$deprojdirGuess   = '../../';
		if(decom_is_project_dir($deprojdirGuess)) {
			$DEPROJDIR = $deprojdirGuess;
		}
		else {
			/* Let the user specify the project directory */
			header('Location: selproject.php');
			exit;
		}
	}
}
// TODO validate projdir and give the opportunity to enter again. If $_SESSION['projdir'] is not cleared on error, the user might not be able to reset it.

require_once($DELIBDIR.'/php/project.php');
decom_select_project($DEPROJDIR);

if(!isset($DEDBLINK)) {
	$errmsg = 'Database connectivity error. ';	
	if(isset($DEDBCONNERR))
		$errmsg .= $DEDBCONNERR;

	decom_page_add_error_message($errmsg);
}

require_once($DELIBDIR.'/php/nan/table.php');
require_once($DELIBDIR.'/php/navigator.php');
require_once($DELIBDIR.'/php/views/notebook.php');

require_once($DELIBDIR.'/php/nan/form.php');
// TODO REM manual css part?
?>
<style>
html, body {
	padding: 0;
	margin: 0;
	font-family: sans-serif;
}

.pane {
	display: table-cell;
	vertical-align: top;
	padding: 1em;
}

#sidemenuContainer {
	background: #ddd;
}

a {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

.divTable {
	display: table;
}

.divTableRow {
	display: table-row;
}

.divTableCell {
	display: table-cell;
}

label {
	display: inline-block;
	padding-right: 1em;
}

input {
	color: #111;
	padding: 0.3em;
}

input:not([type='submit']) {
	background: white;
	border: 1px solid #333;
	border-radius: 2px;
}

table { border-collapse: collapse }
table th, table td { border: 1px solid #888; padding: 0.5em }
</style>
<?php
// TODO move?
function nan_get_url_with_param($k, $v, $base = '', $moreParams = []) {
	$nav = new DecomNavigator($base, $_GET);
	$nav->setParameter($k, $v);
	
	foreach($moreParams as $p)
		$nav->setParameter($p[0], $p[1]);
	
	return $nav->toUrl();
}

// Shorthand TODO move?
function _ua($v, $moreParams = [], $base = '', $k = 'act') { // u for URL and a for Action
	return nan_get_url_with_param($k, $v, $base, $moreParams);
}

// TODO use _ua() in ../include/*.php also.

// TODO add the current params (like lang) to links?

$breadbar = new DecomBreadbarView();
decom_page_set_breadbar($breadbar);

$PAGEPARAM     = 'act'; // TODO make p?
$PAGEURLPREFIX = '?'.$PAGEPARAM.'=';
$PAGEURLDELIM  = '.';

$pageid = isset($_GET[$PAGEPARAM])? $_GET[$PAGEPARAM]: 'home';

function pageIdAppend($part) {
	global $pageid, $PAGEURLPREFIX, $PAGEURLDELIM;

	$newpid = $PAGEURLPREFIX.$pageid;
// TODO remove the is_array() part if not used
// (But copy for universe control panel; will be useful there)
/*
	if(is_array($part))
		foreach($part as $p)
			$newpid .= $PAGEURLDELIM.$p;
	else */
		$newpid .= $PAGEURLDELIM.$part;

	return $newpid;
}

// TODO remove if not used
// (But copy for universe control panel; will be useful there)
/*
function pageIdFromParts($parts) {
	global $pageid, $PAGEURLPREFIX, $PAGEURLDELIM;

	$pid = $PAGEURLPREFIX;
	foreach($parts as $p)
		$pid .= $PAGEURLDELIM.$p;

	return $pid;
}
*/

/* For the use of included scripts */
$deAdminIncDirIndirect = $INCDIR_MANUAL;

require_once($DELIBDIR.'/php/cms/gateway.php');

// TODO FIXME
$sidemenu = decom_menu_from_incdir($INCDIR_AUTO, $PAGEURLPREFIX, $PAGEURLDELIM); // delim '.' to '/'; TODO cache
decom_page_set_side_menu($sidemenu); // TODO not for home

$TABHEADTAG = 'h2';

$breadbar = new DecomBreadbarView();
$title = '';
$ret = decom_autoinclude($pageid, $INCDIR_AUTO, $PAGEURLDELIM, '.php', ['..'], false, $title, $breadbar, $PAGEURLPREFIX); // TODO can I migrate to '/' instead of '.'?
if(is_a($ret, 'DecomError')) {
	decom_page_add_error_message($ret->getMessageHtml());
}
else {
	decom_page_set_breadbar($breadbar);
	$content .= '<h2>'.$title.'</h2>';

	// TODO

	foreach($ret as $path)
		include($path);
}
?>
	</div>
</div>

<?php
require_once('../include/www-commonbottom.php');
?>
