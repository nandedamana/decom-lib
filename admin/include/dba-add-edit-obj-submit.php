<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

// TODO
// TODO heading
// TODO in edit mode, receive eid via $_POST?

if($editMode) {
	if(!isset($_GET['eid'])) {
		// TODO error
	}
}

$kvp = [];

$errmsg_objmked = _('Error creating/updating entity');

$attribs = $cobj->getAttributes(false);
foreach($attribs as $a) {
	$aname = $a->toAttributeName();
	if(isset($_POST[$aname]) && $_POST[$aname] != '') {
		$kvp[] = [$aname, $_POST[$aname]];
	}
	/* TODO remove this if the lowlevel function handles this */
	else if($a->getCanBeUnset() === false) { // Must be set
		if($a->getCanBeNull()) {               // See if can be NULL
			$kvp[] = [$aname, null];
		}
		else {
			decom_page_add_error_message(_('Required field is not set: ').'<i>'.htmlspecialchars($aname).'</i>', $errmsg_objmked);
			$kvp = null;
			break;
		}
	}
}

if($kvp !== null) {
	if($editMode)
		$ret = decom_edit_entity($class, $_GET['eid'], $kvp);
	else
		$ret = decom_create_entity($class, $kvp);

	if(is_a($ret, 'DecomError')) {
		decom_page_add_error_message($ret->getMessageHtml(), $errmsg_objmked);
	}
	else {
		decom_page_add_message(_('Entity created/updated successfully.'));
		
		$eid = $ret;
		if(isset($_POST['__parent'])) {
			$par = $_POST['__parent'];
			if($par == '')
				$par = null; // enables removal
			
			$ret = decom_entity_set_parent($class, $eid, $par);
			
			if(is_a($ret, 'DecomError'))
				decom_page_add_error_message($ret->getMessageHtml(), _('Error setting object parent'));
		}
	}
}
?>
