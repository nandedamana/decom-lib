<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-16
 */

/**
 * @param formType 'mk' or 'ed'
 * @param rshipName Relationship name (required only for formType = 'edit')
 */
// TODO implement type 'edit'
function deform_add_edit_rship($formType, $className, $rshipName = null) {
	$content = '';	
	$cobj    = new DecomClass($className);
	
	$inputFields = ['name', 'toClass', 'leftCardinality', 'rightCardinality'];
	
	if($formType == 'ed') {
		if($rshipName === null)
			return _('Error: Relationship name not specified.');
/* TODO change
		$aobj = $cobj->getAttribute($attrib);
		
		$defaultValues = [];
		foreach($inputFields as $f)
			$defaultValues[$f] = $aobj->getPropertyValue($f);
*/

		$actionFile = 'ed.submit';
	}
	else if($formType == 'mk') {
		//$defaultValues = ['maxInstances' => '1'];
		$actionFile = 'mk';
	}
	else {
		throw new Exception('Invalid formType.');
	}
	$defaultValues = []; // TODO FIXME

	$inputFieldLabels = ['Name', 'To Class', 'Left Cardinality', 'Right Cardinality'];
	$inputFieldTypes  = ['text', 'custom', 'custom', 'custom'];
	$inputFieldsReq   = [true, true, true, true];
	$action = '?act=dba.class.rship.'.$actionFile.'&class='.$className;
	if($formType == 'ed')
		$action .= '&attrib='.$attrib;

	$method = 'POST';
	
	$cards = decom_get_accepted_rship_cardinalities();
	require_once('utility.php');
	$customInput = [
		nan_classlist_to_html_select('toClass', 'selToClass', null, false, [$className]),
		nan_array_to_html_select($cards, 'leftCardinality', 'selLeftCardinality', null, false),
		nan_array_to_html_select($cards, 'rightCardinality', 'selRightCardinality', null, false)];
	$content .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, $customInput, $defaultValues);
	
	return $content;
}
?>
