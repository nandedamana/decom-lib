<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-16
 */

/**
 * TODO doc
 * @param default Set to null to make no preference.
 */
function nan_array_to_html_select($arr, $selname, $selid, $default = '', $noneAvailable = false) {
	$html = "<select name=\"$selname\" id=\"$selid\" value=\"$default\">";

	if($noneAvailable)
		$html .= "<option value=\"$default\">-- None --</option>";

	foreach($arr as $v) {
		$html .= '<option value="'.$v.'"';
		if($default !== null && $v == $default)
			$html .= ' selected';
		$html .= '>'.$v.'</option>';
	}

	$html .= '</select>';
	
	return $html;
}

function nan_classlist_to_html_select($selname, $selid, $default = '', $noneAvailable = false, $excludeList = []) {
	$arr     = [];
	$classes = decom_db_get_class_names();

	foreach($classes as $c)
		if(!in_array($c, $excludeList))
			$arr[] = $c;

	return nan_array_to_html_select($arr, $selname, $selid);
}
?>
