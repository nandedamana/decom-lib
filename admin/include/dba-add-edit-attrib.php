<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Code separated on 2019-05-16
 */

/**
 * @param formType 'add' or 'edit'
 * @param attrib Attribute name (required only for formType = 'edit')
 */
// TODO implement type 'edit'
function deform_add_edit_attrib($formType, $className, $attrib = null) {
	$content = '';	
	$cobj    = new DecomClass($className);
	
	$inputFields = ['name', 'type', 'description', 'maxInstances', 'requiredness', 'unique', 'displayLabel', 'displayOrder'];
	
	/* Default values */
	
	$tsel = ' selected';
	$isel = '';
	
	for($i = 0; $i < 3; $i++)
		$reqsel[$i] = '';
	
	if($formType == 'ed') {
		if($attrib === null)
			return _('Error: Attribute not specified.');

		$aobj = $cobj->getAttribute($attrib);
		// TODO err
		
		$defaultValues = [];
		foreach($inputFields as $f)
			$defaultValues[$f] = $aobj->getPropertyValue($f);
		
		if($defaultValues['type'] == 'int') {
			$isel = ' selected';
			$tsel = '';
		}
		
		$reqsel[$aobj->getRequiredness()] = ' selected';
		
		$actionFile = 'ed.submit';
	}
	else if($formType == 'mk') {
		$defaultValues = ['maxInstances' => '1'];
		$actionFile = 'mk';
	}
	else {
		throw new Exception('Invalid formType.');
	}

	// TODO FIXME ynrads default value
	/* End of default values */

	$inputFieldLabels = ['Name', 'Type', 'Description', 'Max. no of values allowed (0 for infinite)', 'Requiredness', 'Unique group (0 for none)', 'Display label', 'Display order'];
	$inputFieldTypes  = ['text', 'custom', 'textarea', 'number', 'custom', 'int', 'text', 'int'];
	$inputFieldsReq   = [true, true, false, false, false, true, false, false];
	$action = '?act=dba.class.attrib.'.$actionFile.'&class='.$className;
	if($formType == 'ed')
		$action .= '&attrib='.$attrib;
	
	$method = 'POST';
	$customInput = [
		'<select name="type">
			<option value="text"'.$tsel.'>Text</option>
			<option value="int"'.$isel.'>Integer</option>
		</select>',
		'<select name="requiredness">
			<option value="0"'.$reqsel[0].'>Can be unset</option>
			<option value="1"'.$reqsel[1].'>Must be set, but the value can be NULL</option>
			<option value="2"'.$reqsel[2].'>Must be set and cannot be NULL</option>
			</select>'
	];
	$content .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, $customInput, $defaultValues);
	
	return $content;
}
?>
