<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-05
 */

$INCDIR_MANUAL = '../include';
require_once($INCDIR_MANUAL.'/accesscheck.php');

$INCDIR_AUTO = '../include-auto';
$DELIBDIR    = dirname(__FILE__).'/../..';

require_once($DELIBDIR.'/php/project.php');
require_once($DELIBDIR.'/php/navigator.php');
require_once($DELIBDIR.'/php/views/page.php');

$content = '';
decom_page_init();

$LOCALEDIR = dirname(__FILE__).'/../locale';
$LOCALEDOM = 'libdecom-admin';
textdomain($LOCALEDOM);

// bindtextdomain($LOCALEDOM, $LOCALEDIR); TODO this causes a mysterious error sometimes ('Error setting locale')?
bind_textdomain_codeset($LOCALEDOM, 'UTF-8'); // IMPORTANT

$lang = 'en_IN';
if(isset($_GET['lang'])) {
	$lang = $_GET['lang'];
	setcookie('lang', $_GET['lang']);
}
else if(isset($_COOKIE['lang'])) {
	$lang = $_COOKIE['lang'];
}

// TODO FIXME this loads the local files from $LOCALEDIR, but only if the language is not listed in the `localectl list-locales`. If not, I've to copy the mo files to /usr/share/locale/LANG/LC_MESSAGES/ and run `locale-gen` and then reload Apache.
if(setlocale(LC_MESSAGES, $lang) === false)
	decom_page_add_error_message('Error setting locale: '.htmlspecialchars($lang));

decom_page_set_title(_('libdecom Administration'));
$content = '';
/* Why do this? Because the user might not have setup the CSS serving capability. */
// TODO FIXME not here, but in the header.
$content .= '<style>'.file_get_contents($DELIBDIR.'/css/lalitham/main.css').'</style>';

$langmenu = new DecomMenu();
foreach([['en', 'English'], ['kn', 'ಕನ್ನಡ'], ['ml', 'മലയാളം'], ['ta', 'தமிழ்']] as $lrow) {
	if($lrow[0] != 'en' && !file_exists($LOCALEDIR.'/'.$lrow[0].'/LC_MESSAGES/'.$LOCALEDOM.'.mo'))
		continue;

	$nav = new DecomNavigator('', $_GET);
	$nav->setParameter('lang', $lrow[0].'_IN'); // TODO FIXME not always _IN
	$langmenu->addItem(new DecomMenuItem($lrow[1], $nav->toUrl(), $lrow[1]));
}
$thisPage = decom_get_current_page();
$thisPage->setHtmlBeforeHeaderH1($langmenu->toHtml('langmenu'));
?>
