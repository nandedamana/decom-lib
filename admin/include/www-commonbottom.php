<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-05
 */

$footer = new DecomPageViewFooter();
$footer->setAttributionHtml('Copyright (C) 2018, 2019 Nandakumar Edamana.');

decom_page_set_content($content);
decom_page_set_footer($footer);
decom_page_display();
?>
