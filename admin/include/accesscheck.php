<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27
 */

/* $_SERVER['SERVER_ADDR'] seems better than $_SERVER['REMOTE_ADDR'], because
 * requests from localhost seems to have IPs other than 127.0.0.1 sometimes
 * (happened when I checked this on the public VPS of nandakumar.co.in).
 */
if($_SERVER['SERVER_ADDR'] != '127.0.0.1' && $_SERVER['SERVER_ADDR'] != '::1')
	die('This site can only be accessed from the IP 127.0.0.1 or ::1.');
?>
