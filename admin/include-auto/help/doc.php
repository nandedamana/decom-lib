<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27
 */

$docpath  = $DELIBDIR.'/doc/build';
$docindex = $docpath.'/index.html';
if(file_exists($docindex))
	$content .= "<p>Copy-paste the following link in a new tab or right click on it and then choose 'Open Link in New Tab' to read the documentation (because browsers won't open file:// links from http[s]:// pages): <a href=\"file://$docindex\" target=\"_blank\">file://$docindex</a>"; // TODO FIXME Use Apache alias?
else
	$content .= '<p>You don\'t seem to have built the documentation yet.</p> TODO link to online doc.';
?>
