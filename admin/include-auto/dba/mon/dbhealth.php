<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-05
 */

if(!isset($DEDBLINK)) {
	echo "DBERR"; // TODO clear std. message.
}
else { // Database connection is okay.
	require_once($DELIBDIR.'/php/dbgen.php');
	require_once($DELIBDIR.'/php/class.php');
	require_once($DELIBDIR.'/php/test.php'); // for nb()

	if(isset($_POST['act'])) {
		switch($_POST['act']) {
		case 'mkcls':
			$errttl = _('Error creating class');

			if(!isset($_POST['class'])) {
				decom_page_add_error_message(_('Class name not given.'), $errttl);
			}
			else {
				$ret = decom_db_create_built_in_class($_POST['class']);
				if(is_a($ret, 'DecomError'))
					decom_page_add_error_message($ret->getMessageHtml(), $errttl);
				else
					decom_page_add_message(_('Class created successfully.'));
			}

			break;
		case 'mktbl':
			$errttl = _('Error creating table');

			if(!isset($_POST['table'])) {
				decom_page_add_error_message(_('Table name not given.'), $errttl);
			}
			else {
				$ret = decom_db_create_essential_table($_POST['table']);
				if(is_a($ret, 'DecomError'))
					decom_page_add_error_message($ret->getMessageHtml(), $errttl);
				else
					decom_page_add_message(_('Table created successfully.'));
			}

			break;
		case 'clstblMk':
		case 'clstblAddMissingCols':
			$errttl     = _('Error fixing class implementation table');
		
			if(!isset($_POST['tableType'])) {
				decom_page_add_error_message(_('tableType not given.'), $errttl);
			}
			else {
				if(!isset($_POST['class'])) {
					decom_page_add_error_message(_('Class name not given.'), $errttl);
				}
				else {
					if($_POST['act'] == 'clstblMk') {
						$errttl     = _('Error creating class implementation table'); # without 'implementation', 'class table' will be confusing
						$successmsg = _('Class implementation table created successfully.');
						$ret        = decom_db_create_class_implementation_table($_POST['class'], $_POST['tableType']);
					}
					else if($_POST['act'] == 'clstblAddMissingCols') {
						$errttl     = _('Error adding missing columns');
						$successmsg = _('Missing columns added successfully.');
						$ret        = decom_db_table_add_missing_columns('eav_ameta_'.$_POST['class'], $DE_eavAmetaCols);
					} // There will be no else case, since this is inside a well-defined branch
					
					if(decom_is_errobj($ret))
						decom_page_add_error_message($ret->getMessageHtml(), $errttl);
					else
						decom_page_add_message($successmsg);
				}
			}

			break;
		case 'fixall':
			$ret = decom_db_fix_all();

			if(decom_is_errobj($ret))
				decom_page_add_error_message($ret->getMessageHtml(), _('Error fixing the database'));
			else
				decom_page_add_message(_('Database fixed successfully (NOTE: this feature is experimental and incomplete).')); // TODO rem the incomplete warning once complete.
			
			break;
		default:
			decom_page_add_error_message('Invalid action: '.htmlentities($_POST['act']));
		}
	}

	// TODO move
	$content .= '<style>span.good, span.bad { font-weight: bold }
		span.good { color: #060 }
    span.bad { color: maroon }</style>
<script>
document.addEventListener("DOMContentLoaded", function() {
$("_deChkHideHealthy").addEventListener("change", function() {
unhealthy = $c("_deHealthy");
disp = (this.checked)? "none": "";

for(i = 0; i < unhealthy.length; i++)
	unhealthy[i].style.display = disp;
});

$("_deChkHideHealthy").click();
});
</script>  
  ';

	$content .= '<div><input type="checkbox" id="_deChkHideHealthy" /> <label for="_deChkHideHealthy">'.
		_('Hide healthy components').'</label></div>';

	$ntbk = new DecomNotebookView();

	$etbls = decom_db_get_essential_tables();
	
	$tc  = nan_table_start(); // tc - Tab Content
	$tc .= nan_table_array_to_th(['Table', 'Exists?', 'Actions']);

	foreach($etbls as $tbl) {
		$acts = '';

		$exists = decom_db_table_exists($tbl);
		if(!$exists)
			$acts = '<form method="POST">'.
				'<input name="act" type="hidden" value="mktbl">'.
				'<input name="table" type="hidden" value="'.$tbl.'">'.
				'<input type="submit" value="Create">'.
				'</form>';
		
		$tc .= nan_table_array_to_td([$tbl, nb($exists), $acts]); // TODO actions
	}

	$tc .= nan_table_close();

	$tab = new DecomNotebookViewTab(_('Essential Tables'),
		$tc,
		'h2',
		'tabEssTables');
	$ntbk->appendTab($tab);

	$eclasses = decom_db_get_builtin_classes();
	
	$tc  = nan_table_start(); // tc - Tab Content
	$tc .= nan_table_array_to_th(['Class', 'Exists?', 'Actions']);

	foreach($eclasses as $class) {
		$acts = '';

		$exists = decom_class_exists($class);
		if(!$exists)
			$acts = '<form method="POST">'.
				'<input name="act" type="hidden" value="mkcls">'.
				'<input name="class" type="hidden" value="'.$class.'">'.
				'<input type="submit" value="Create">'.
				'</form>';
		
		$tc .= nan_table_array_to_td([$class, nb($exists), $acts]); // TODO actions
	}

	$tc .= nan_table_close();

	$tab = new DecomNotebookViewTab(_('Built-in Classes'),
		$tc,
		'h2',
		'tabBuiltinClasses');
	$ntbk->appendTab($tab);

	$tc = '';
	$tables         = [];
	$tbls_eav       = [];
	$tbls_eav_ameta = [];
	$tbls_hier      = [];
	$classes = decom_db_get_class_names(); // TODO use decom_db_get_classes_raw() and check parents also.

	switch($DEDBDRIVER) {
	case DEDBDRV_MYSQL:
		$rs = $DEDBLINK->query('select table_name from information_schema.tables where table_schema='.$DEDBLINK->quote($DEDBNAME));
		break;
	case DEDBDRV_SQLITE:
		$rs = $DEDBLINK->query('select name from sqlite_master where type="table"');
		break;
	default:
		throw new Exception('TODO implement db driver support.'); // TODO common erro?
	}
	
	if(!$rs)
		echo 'TODO db query err'; // TODO
	
	foreach($rs as $r)
		$tables[] = $r[0];

	/* Why create separate arrays? Because we have to perform a reverse check as well. */
	foreach($tables as $t) {
		if(strpos($t, 'eav_') == 0)
			$tbls_eav[] = $t;
			
		if(strpos($t, 'eav_ameta') == 0)
			$tbls_eav_ameta[] = $t;

		if(strpos($t, 'hier_') == 0)
			$tbls_hier[] = $t;
	}
	
	$cstat          = [];
	$cactions       = [];
	$corruptClasses = [];
	
	function fix_table_form($className, $tableType, $action) {
		return '<form method="POST">
				<input type="hidden" name="act" value="'.$action.'" />
				<input type="hidden" name="tableType" value="'.$tableType.'" />
				<input type="hidden" name="class" value="'.$className.'" />
				<input type="submit" value="Fix" />
			</form>';		
	}
	
	foreach($classes as $c) {
		// TODO Is storing the info instead of printing immediately useful?
		$cstat[$c] = [];

		$cstat[$c]['has_eav_ameta_tbl'] = in_array('eav_ameta_'.$c, $tbls_eav_ameta);
		if($cstat[$c]['has_eav_ameta_tbl']) {
			$cstat[$c]['eav_ameta_tbl_has_all_columns'] =
				decom_db_table_has_columns('eav_ameta_'.$c, $DE_eavAmetaCols);
			
			if(!$cstat[$c]['eav_ameta_tbl_has_all_columns'])
				$cactions[$c]['eav_ameta_tbl_has_all_columns'] = fix_table_form($c, 'eavAmeta', 'clstblAddMissingCols');
		}
		else {
			$cactions[$c]['eav_ameta_tbl_has_all_columns'] = fix_table_form($c, 'eavAmeta', 'clstblMk');
			// TODO create button
		}

		$cstat[$c]['has_eav_tbl']  = in_array('eav_'.$c, $tbls_eav);
		// TODO check and fix has all cols
		if(!$cstat[$c]['has_eav_tbl'])
			$cactions[$c]['has_eav_tbl'] = fix_table_form($c, 'eav', 'clstblMk');

		// TODO check and fix has all cols
		$cstat[$c]['has_hier_tbl'] = in_array('hier_'.$c, $tbls_hier);
		if(!$cstat[$c]['has_hier_tbl'])
			$cactions[$c]['has_hier_tbl'] = fix_table_form($c, 'hier', 'clstblMk');

		if(!$cstat[$c]['has_eav_ameta_tbl'] || !$cstat[$c]['eav_ameta_tbl_has_all_columns'] || !$cstat[$c]['has_eav_tbl'] || !$cstat[$c]['has_hier_tbl'])
			$corruptClasses[] = $c;
	}

	// TODO merge loops?

	foreach($classes as $c) {
		$clsattr = in_array($c, $corruptClasses)? '': ' class = "_deHealthy"';

		$tc .= '<div'.$clsattr.'><h4>'.$c.'</h4>';
		$tc .= nan_table_start();
		$tc .= nan_table_array_to_th([_('Check'), _('Status'), _('Actions')]);

		foreach($cstat[$c] as $k => $v) {
			$acts = '';
			if(isset($cactions[$c][$k]))
				$acts = $cactions[$c][$k];

			$tc .= nan_table_array_to_td([$k, nb($v), $acts]);
		}

		$tc .= nan_table_close().'</div>';
	}			

	$tab = new DecomNotebookViewTab(_('Class Tables'),
		$tc,
		'h2',
		'tabClassTables');
	$ntbk->appendTab($tab);

	/* BEGIN Fix All tab */
	$tc = '<p>Try to fix everything, but fallback on problematic situations.</p>'.
				'<form method="POST">'.
				'<input name="act" type="hidden" value="fixall">'.
				'<input type="submit" value="Fix">'.
				'</form>';

	$tab = new DecomNotebookViewTab(_('Fix Everything'),
		$tc,
		'h2',
		'tabFixAll');
	$ntbk->appendTab($tab);

	/* END Fix All tab */

	$content .= $ntbk->toHtml();
}
?>
