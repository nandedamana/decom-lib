<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

/* Everything put inside __bottom.php because these things
 * should be shown under the messages from action files
 * like mk.php.
 */

$ntbk = new DecomNotebookView();

$classes = decom_db_get_class_names();

$tc = nan_table_start(); // tc - Tab Content
$tc.= nan_table_array_to_th([_('Class'), _('Actions')]);

foreach($classes as $c) {
	$edlink = '?act=dba.class.index&class='.$c;
	$rmlink = '?act='.$pageid.'&form=rm&class='.$c.'#tabDelClass';

	$tc .= nan_table_array_to_td([$c,
		'<a href="'.$edlink.'">'._('Manage').'</a> | '.
		'<a href="'.$rmlink.'">'._('Delete').'</a>']);
}
$tc .= nan_table_close();

$tab = new DecomNotebookViewTab(_('List of Classes'),
	$tc,
	'h2',
	'tabListClasses');
$ntbk->appendTab($tab);

$inputFields      = ['name', 'parent'];
$inputFieldLabels = ['Name', 'Parent'];
$inputFieldTypes  = ['text', 'custom'];
$inputFieldsReq   = [true, false];
$action = '?act=dba.classes.mk';
$method = 'POST';

require_once($deAdminIncDirIndirect.'/utility.php');
$customInput = [nan_array_to_html_select($classes, 'parent', 'selParent', '', true)];

$tc  = nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, $customInput);
$tab = new DecomNotebookViewTab(_('Create New Class'),
	$tc,
	'h2',
	'tabCreateClass');
$ntbk->appendTab($tab);

/* Delete Class */

$inputFields      = ['action', 'class', 'dropobjs', 'sure'];
$inputFieldLabels = ['', _('Class'), _('Automatically delete objects if any'), _('I\'m sure I want to delete this class')];
$inputFieldTypes  = ['hidden', 'custom', 'checkbox', 'checkbox'];
$inputFieldsReq   = [true, true, false, true];
$defaultValues    = ['__submit' => _('Delete'), 'action' => 'rm', 'dropobjs' => '', 'sure' => ''];
$action = '?act=dba.classes';
$method = 'POST';

$classes = decom_db_get_class_names();

require_once($deAdminIncDirIndirect.'/utility.php');

if(isset($_GET['form'], $_GET['class']) && $_GET['form'] == 'rm')
	$defclass = $_GET['class'];
else
	$defclass = '';

$customInput = [nan_array_to_html_select($classes, 'class', 'selClass', $defclass, true)];

$tc = nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, $customInput, $defaultValues);

$tab = new DecomNotebookViewTab(_('Delete Class'),
	$tc,
	'h2',
	'tabDelClass');
$ntbk->appendTab($tab);

$content .= $ntbk->toHtml();
?>
