<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

if(!isset($_POST['name'])) {
	// TODO error
}
else {
	if(!isset($_POST['parent']) || $_POST['parent'] == '')
		$parent = null;
	else
		$parent = $_POST['parent'];

	$ret = decom_create_class($_POST['name'], $parent);
	if($ret === true)
		decom_page_add_message(_('New class created successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error creating new class'));
}
?>
