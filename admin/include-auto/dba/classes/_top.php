<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-15
 */

/* Files are split in order to improve readability.
 * Including via this file to void pageid in the URL getting appended with parts like rm/submit.
 */
$actions = ['rm'];
if(isset($_POST['action']) && in_array($_POST['action'], ['rm']))
	include('submit/'.$_POST['action'].'.php');
?>
