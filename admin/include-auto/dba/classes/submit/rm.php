<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-15
 */

$errttl = _('Error deleting class');

if(!isset($_POST['class']) || $_POST['class'] == '') {
	decom_page_add_error_message(_('Class name not specified.'), $errttl);
}
else {
	if(!isset($_POST['sure']) || $_POST['sure'] != 1) { // nan_generate_form() uses 1 for checkboxes
		decom_page_add_error_message(_('You were not sure.'), $errttl);
	}
	else {
		$dropobjs = (isset($_POST['dropobjs']) && $_POST['dropobjs'] == 1);
		$ret = decom_remove_class($_POST['class'], $dropobjs);
		if($ret === true)
			decom_page_add_message(_('Class deleted successfully.'));
		else
			decom_page_add_error_message($ret->getMessageHtml(), $errttl);
	}
}

?>
