<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */
if(!isset($DEDBLINK))
	die('<p>There is database connectivity error and we can\'t proceed.</p>'); // TODO avoid sudden page termination

require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
?>
