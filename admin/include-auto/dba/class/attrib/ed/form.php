<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-22
 */

if(!isset($_GET['attrib'])) {
	// TODO error (common error inside this folder? move to _top.php?)
	decom_page_add_error_message(_('Attribute not specified.'));
}
else {
	require_once($deAdminIncDirIndirect.'/dba-add-edit-attrib.php');
	$content .= deform_add_edit_attrib('ed', $class, $_GET['attrib']);
	$skipbottom = true; // To skip ../__bottom.php, which lists all attribs
}
?>
