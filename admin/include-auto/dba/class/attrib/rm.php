<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

if(!isset($_GET['attrib'])) { // TODO pass via $_POST?
	decom_page_add_error_message(_('Attribute not specified.'), _('Error deleting attribute'));
}
else {
	$ignoreUsage = (isset($_GET['ignoreUsage']) && $_GET['ignoreUsage'] == '1');
	$ret = decom_class_remove_attribute($class, $_GET['attrib'], true, !$ignoreUsage);
	if($ret === true)
		decom_page_add_message(_('Attribute deleted successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error deleting attribute'));
}
?>
