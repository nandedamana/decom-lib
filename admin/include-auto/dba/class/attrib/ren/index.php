<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-15
 */

if(!isset($_GET['attribId'])) {
	// TODO error (common error inside this folder? move to _top.php?)
	decom_page_add_error_message(_('Attribute not specified.'));
}
else {
	$aobj = $cobj->getAttribute($_GET['attribId'], false);
	if(!decom_is_value($aobj)) {
		decom_page_add_error_message(_('Invalid attribute.'));
	}
	else {
		$content .= '<p>'._('Enter the new name for the attribute: ').$aobj->getName().'</p>';
	
		$inputFields      = ['class', 'attribId', 'newName'];
		$inputFieldLabels = ['', '', 'New Name'];
		$inputFieldTypes  = ['hidden', 'hidden', 'text'];
		$inputFieldsReq   = [true, true, true, true];
		$defVals = [ 'class' => $class,
			'attribId' => $_GET['attribId'] ];
		$action = pageIdAppend('submit');
		$method = 'POST';

		$content .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, [] ,$defVals);
		
		$skipbottom = true; // To skip ../__bottom.php, which lists all attribs
	}
}
?>
