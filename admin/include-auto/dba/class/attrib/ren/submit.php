<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-15
 */

if(!isset($_POST['class'], $_POST['attribId'], $_POST['newName'])) {
	decom_page_add_error_message(_('Invalid access.'));
}
else {
	$ret = decom_class_rename_attribute(
		$_POST['class'], $_POST['attribId'], $_POST['newName'], false);
	if($ret === true)
		decom_page_add_message(_('Attribute renamed successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error renaming attribute'));
}
?>
