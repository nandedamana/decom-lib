<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

if(!isset($_POST['name'], $_POST['type'])) { // TODO FIXME these are not enough; use a global common array?
	// TODO error
}
else {
	$maxInstances =
		(isset($_POST['maxInstances']))? $_POST['maxInstances']: 1;

	$desc = isset($_POST['description'])? $_POST['description']: '';

	$displayParams = [
		'label' => isset($_POST['displayLabel'])? $_POST['displayLabel']: $_POST['name'],
		'order' => isset($_POST['displayOrder'])? $_POST['displayOrder']: 0
	];

	// TODO desc
	if(isset($iAmInEditAttribSubmitPage) && $iAmInEditAttribSubmitPage) {
		$ret = decom_class_edit_attribute($class, $_GET['attrib'], $_POST['name'], $_POST['type'], $desc, $maxInstances, $_POST['requiredness'], $_POST['unique'], $displayParams);
		if($ret === true)
			decom_page_add_message(_('Attribute updated successfully.'));
		else
			decom_page_add_error_message($ret->getMessageHtml(), _('Error updating attribute'));
	}
	else {
		$ret = decom_class_add_attribute($class, $_POST['name'], $_POST['type'], $desc, $maxInstances, $_POST['requiredness'], $_POST['unique'], $displayParams);
		if($ret === true)
			decom_page_add_message(_('New attribute added successfully.'));
		else
			decom_page_add_error_message($ret->getMessageHtml(), _('Error adding new attribute'));
	}
}
?>
