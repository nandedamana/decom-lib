<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

if(!isset($_POST['name'])) { // TODO check other required fields
	// TODO error
}
else {
	$ret = decom_add_relationship($_POST['name'], $class, $_POST['toClass'], $_POST['leftCardinality'], $_POST['rightCardinality']);
	if($ret === true)
		decom_page_add_message(_('New relationship created successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error creating new relationship'));
}
?>
