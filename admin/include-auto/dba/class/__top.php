<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

$noerr  = true;

$breadbar->appendCrumb(_('Class Management'), _ua('dba.classes.index'));

$class = null;
if(isset($_GET['class']))
	$class = $_GET['class'];

if(isset($_POST['class']))
	$class = $_POST['class'];

if($class === null || decom_validate_class_name($class) === false) {
//  XXX die is the only way to prevent further autoinclude. Or make use of $noerr
//	decom_page_add_error_message(
		die('No class specified, or the specified class name is invalid. Please go back and try again.');
//	$noerr = false;
}
else {
	$cobj = new DecomClass($class); // TODO FIXME err
	
	$breadbar->appendCrumb('<i>'.$class.'</i>', _ua('dba.class.index', [['class', $class]]));	
}

$skipbottom = !$noerr;
?>
