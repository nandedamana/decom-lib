<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

if(!isset($_GET['eid'])) {
	// TODO error
}

require_once($DELIBDIR.'/php/views/entityedit.php');

$v = new DecomEntityEditView($class, $_GET['eid']);
$v->setFormId('frm_edobj');
$v->setShowParentObjectId(true);
// TODO return to the edit page of the same object after updating the db
$v->setFormAction(
	'?act=dba.class.obj.ed.submit&class='.$class.'&eid='.$_GET['eid']); // TODO pass eid via POST[]?

$content .= $v->render();

$skipbottom = true;
?>
