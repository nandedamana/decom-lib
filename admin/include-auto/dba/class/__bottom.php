<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

require_once($DELIBDIR.'/php/nan/utility.php');

if(!$skipbottom) {
	$ntbk = new DecomNotebookView();

	$tc  = nan_table_start(); // tc - Tab Content
	$tc .= nan_table_array_to_th(['Class', 'ID', 'Name', 'Data Type', 'Max. Values Allowed', 'Can be Unset', 'Can be NULL', 'Actions']);

	$cobj = new DecomClass($class); // Reloading is needed since the previous object might be holding data before an attrib addition or something

	$attribs = $cobj->getAttributes();
	if(decom_is_errobj($attribs)) {
		decom_page_add_error_message($attribs->getMessageHtml(), _('Error listing attributes'));
	}
	else {
		foreach($attribs as $a) {
			$aname  = $a->getName();
			$aid    = $a->getId();
			$edlink = '?act=dba.class.attrib.ed.form&class='.$class.'&attrib='.$aname;
			$rnlink = '?act=dba.class.attrib.ren&class='.$class.'&attribId='.$aid;
			$rmlink = '?act=dba.class.attrib.rm&class='.$class.'&attrib='.$aname;
			$rmlinkIgnoreUsage = '?act=dba.class.attrib.rm&class='.$class.'&attrib='.$aname.'&ignoreUsage=1';

			$tc .= nan_table_array_to_td([$a->getClassName(),
				$aid,
				$aname,
				$a->getType(),
				$a->getMaxInstances(),
				nan_bool_to_yesno($a->getCanBeUnset(), false),
				nan_bool_to_yesno($a->getCanBeNull(), false),
				'<a href="'.$edlink.'">Edit</a> | <a href="'.$rnlink.'">Rename</a> | <a href="'.$rmlink.'">Delete</a> | <a href="'.$rmlinkIgnoreUsage.'">Delete even if in use</a>']);
		}
	}				

	$tc .= nan_table_close();

	$tab = new DecomNotebookViewTab(_('List of Attributes'),
		$tc,
		$TABHEADTAG,
		'tabListAttrib');
	$ntbk->appendTab($tab);
	unset($tc);

	require_once($deAdminIncDirIndirect.'/dba-add-edit-attrib.php');
	$tab = new DecomNotebookViewTab(_('Add Attribute'),
		deform_add_edit_attrib('mk', $class),
		$TABHEADTAG,
		'tabAddAttrib');
	$ntbk->appendTab($tab);

	$tc  = nan_table_start(); // tc - Tab Content
	$tc .= nan_table_array_to_th(['From Class', 'To Class', 'ID', 'Name', 'Max. Values Allowed', 'Required', 'Actions']); // TODO desc?

	$rships  = $cobj->getRelationships();
	$irships = $cobj->getInverseRelationships();

	if(decom_is_errobj($rships)) {
		decom_page_add_error_message($rships->getMessageHtml(), _('Error listing outgoing relationships'));
	}
	else if(decom_is_errobj($irships)) {
		decom_page_add_error_message($irships->getMessageHtml(), _('Error listing incoming relationships'));
	}
	else {
		foreach([$rships, $irships] as $attribs) {
			foreach($attribs as $a) {
				$aname  = $a->toAttributeName();
				$rmlink = $rmlink = '?act=dba.class.attrib.rm&class='.$class.'&attrib='.$aname;

				$tc .= nan_table_array_to_td([$a->getFromClassName(),
					$a->getToClassName(),
					$a->getId(),
					$a->getName(),
					$a->getMaxInstances(),
					nan_bool_to_yesno(!$a->getCanBeNull(), false),
					'<a href="'.$rmlink.'">Delete</a>']);
			}				
		}
	}

	$tc .= nan_table_close();

	$tab = new DecomNotebookViewTab(_('List of Relationships'),
		$tc,
		$TABHEADTAG,
		'tabListRships');
	$ntbk->appendTab($tab);
	unset($tc);

	require_once($deAdminIncDirIndirect.'/dba-add-edit-rship.php');
	$tab = new DecomNotebookViewTab(_('Create Relationship'),
		deform_add_edit_rship('mk', $class),
		$TABHEADTAG,
		'tabCreateRship');
	$ntbk->appendTab($tab);

	require_once($DELIBDIR.'/php/views/entityedit.php');

	$v = new DecomEntityAddView($class);
	$v->setFormId('frm_mkobj');
	$v->setShowParentObjectId(true);
	// TODO return to the edit page of the same object after updating the db
	$v->setFormAction(
		'?act=dba.class.obj.mk.submit&class='.$class);

	$tab = new DecomNotebookViewTab(_('Create Entity'),
		$v->render(),
		$TABHEADTAG,
		'tabCreateEntity');
	$ntbk->appendTab($tab);

	$tab = new DecomNotebookViewTab(_('List Entities'),
		'<a href="?act=dba.class.lsobjs&class='.$class.'">Click here</a> to list all objects of this class.',
		$TABHEADTAG,
		'tabLsObj');
	$ntbk->appendTab($tab);

	$content .= $ntbk->toHtml();
}
?>
