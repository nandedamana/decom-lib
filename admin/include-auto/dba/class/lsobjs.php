<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

require_once($DELIBDIR.'/php/view.php');
require_once($DELIBDIR.'/php/views/entitylisttable.php');

$breadbar->appendCrumb(_('List Entities'), _ua('dba.class.lsobjs', [['class', $class]]));

//decom_page_add_message('Only first 100 objects are listed.'); // TODO FIXME, TODO implement

$content .= '<h2 id="tabAddAttrib">Objects of the Class \''.$class.'\'</h2>';

$v = new DecomEntityListTableView($class); // TODO site
$v->setColumns(['name', 'code', 'title']); // TODO used in other views also; make global
$content .= $v->render([_('Actions')], function(&$row, $id) {
	global $class;

	$edlink = '?act=dba.class.obj.ed.form&class='.$class.'&eid='.$id;
	$rmlink = '?act=dba.class.obj.rm&class='.$class.'&eid='.$id; // TODO ask confirmation on click

	$row[] = '<a href="'.$edlink.'">Edit</a> | <a href="'.$rmlink.'">Delete</a>'; // TODO implement
});

/** TODO remove this old code once DecomEntityListView seems satisfactory.
$content .= nan_table_start();
$content .= nan_table_array_to_th(['ID', 'name (if exists)', 'Actions']);

$eids = decom_get_entity_ids($class);

foreach($eids as $eid) {
	$obj = new DecomEntity($class, $eid);

	$name = '';
	if($obj->hasPropertyValue('name', true)) {
		$ret = $obj->getPropertyValue('name');

		if(!is_array($ret))
			$name = $ret;
	}

	$edlink = '?act=dba.class.obj.ed.form&class='.$class.'&eid='.$eid;
	$rmlink = '?act=dba.class.obj.rm&class='.$class.'&eid='.$eid; // TODO ask confirmation on click

	$content .= nan_table_array_to_td([$eid,
		$name,
		'<a href="'.$edlink.'">Edit</a> | <a href="'.$rmlink.'">Delete</a>']); // TODO implement
}

$content .= nan_table_close();

*/

$skipbottom = true;
?>
