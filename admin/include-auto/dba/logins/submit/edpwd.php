<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-29
 */

require_once($DELIBDIR.'/php/people/person.php');

if(!isset($_POST['uname'], $_POST['passwd'], $_POST['passwdConfm'])) {
	echo 'err';
	// TODO err - common errfun for Invalid Action
}
else {
	if($_POST['passwd'] != $_POST['passwdConfm']) {
		// TODO stock msg; use in other places also
		echo "<p>Password confirmation mismatch</p>";
		// TODO reverse-include the edit page (PHP include() isn't enough; think about _top.php, __top.php)
	}
	else {
		$ret = decom_login_set_password($_POST['uname'], $_POST['passwd']);
		if($ret === true) {
			decom_page_add_message(_('Password changed successfully'));
			// TODO reverse-include the edit page (PHP include() isn't enough; think about _top.php, __top.php)
		}
		else {
			decom_page_add_error_message($ret->getMessageHtml(), _('Error changing password'));
			// TODO reverse-include the index page (PHP include() isn't enough; think about _top.php, __top.php)
		}
	}
}
?>
