<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-29
 */

if(!isset($_POST['action']) || !in_array($_POST['action'], ['edpwd'])) {
	echo 'Invalid access';
	// TODO err - common errfun for Invalid Action
}
else {
	include($_POST['action'].'.php');
}
?>
