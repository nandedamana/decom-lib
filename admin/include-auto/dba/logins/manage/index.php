<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-28
 */

if(!isset($_GET['uname'])) {
	echo "TODO err";
	// TODO common err for invalid access
}
else {
	$uname = &$_GET['uname'];
	
	$breadbar->appendCrumb(_('User: ')." <i>$uname</i>", _ua('TODO'));

	$ntbk = new DecomNotebookView();

	/* Begin Change Password */

	// TODO show password feature?
	$inputFields      = ['action', 'uname', 'passwd', 'passwdConfm'];
	$inputFieldLabels = ['', '', _('New Password'), _('Confirm Password')];
	$inputFieldTypes  = ['hidden', 'hidden', 'password', 'password'];
	$inputFieldsReq   = [true, true, true, true];
	$defaultValues    = ['__submit' => _('Change Password'), 'action' => 'edpwd', 'uname' => $uname];
	$action = '?act=dba.logins.submit';
	$method = 'POST';

	$tc = nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, null, $defaultValues); // tc - Tab Content

	$tab = new DecomNotebookViewTab(_('Change Password'),
		$tc,
		'h2',
		'tabChangePwd');
	$ntbk->appendTab($tab);

	/* End Change Password */

	$content .= $ntbk->toHtml();

	// TODO
}
?>
