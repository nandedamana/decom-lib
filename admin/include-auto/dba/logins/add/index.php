<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-08, file separated on 2019-05-18.
 */

require_once($DELIBDIR.'/php/query.php'); // TODO rem if unused

$inputFieldTypes  = ['text', 'password', 'password', 'custom', 'text'];
$action = pageIdAppend('submit');
$method = 'POST';

$sel_owner_class  = '<select name="owner_class">';
$classes = decom_db_get_class_names();
foreach($classes as $c)
	$sel_owner_class  .= "<option value=\"$c\">$c</option>";
$sel_owner_class .= '</select>';

$customInput = [
	$sel_owner_class
];

$content .= nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, $customInput);
?>
