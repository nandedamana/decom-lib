<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-08, file separated on 2019-05-18.
 */

require_once($DELIBDIR.'/php/login/account.php');

$allSet = true;
foreach($inputFields as $i => $f) {
	if($inputFieldsReq[$i] == true && !isset($_POST[$f])) {
		$allSet = false;
		decom_page_add_error_message('Required field not set: '.$f, _('Error creating new user'));
		break;
	}
}

if($allSet) {
	$errttl = _('Error creating new login account');

	if($_POST['passwdConfirm'] != $_POST['passwd']) {
		decom_page_add_error_message(_('Password confirmation mismatch.'), $errttl);
	}
	else {
		try {
			$ret = decom_create_login($_POST['owner_eid'], $_POST['uname'], $_POST['passwd'], $_POST['owner_class']);
			if(decom_is_errobj($ret))
				decom_page_add_error_message($ret->getMessageHtml(), $errttl);
			else
				decom_page_add_message(_('New login added successfully.'));
		}
		catch(Exception $e) {
			decom_page_add_error_message($e->getMessage(), $errttl);
		}
	}
}
?>
