<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-08, file separated on 2019-05-18.
 */

/* Non-recursive common top */
require_once($DELIBDIR.'/php/people/person.php');

$inputFields      = ['uname', 'passwd', 'passwdConfirm', 'owner_class', 'owner_eid'];
$inputFieldLabels = ['Username', 'Password', 'Confirm Password', 'Owner Class', 'Owner Entity ID'];
$inputFieldsReq   = [true, true, true, true, true];
?>
