<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-08, file separated on 2019-05-18.
 */

require_once($DELIBDIR.'/php/test.php'); // TODO FIXME

// TODO verify $DEDBLINK exists
$rs = decom_db_query('select uname, owner_class, owner_eid from login');
if(decom_is_errobj($rs)) {
	decom_page_add_error_message($rs->getMessageHtml());
}
else {
	$content .= nan_pdo_rs_to_html_table($rs, ['uname', 'owner_class', 'owner_eid', '_acts'], [_('Username'), _('Owner Class'), _('Owner Entity ID'), _('Actions')], function(&$r) {
	$r['_acts'] = '<a href="'._ua('dba.logins.manage', [['uname', $r['uname']]]).'">'._('Manage').'</a>';
});
}
?>
