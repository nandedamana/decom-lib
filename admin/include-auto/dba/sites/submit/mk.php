<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-14
 */

require_once($DELIBDIR.'/php/universe.php');

$errttl = _('Error creating new world');
if(!isset($_POST['name'])) {
	decom_page_add_error_message(
		_('Name is required to create a new world.'),
		$errttl);
}
else {
	$name = &$_POST['name'];

	$ret = decom_universe_create_world($name); // TODO db info
	if(is_a($ret, 'DecomError'))
		decom_page_add_error_message($ret->getMessageHtml(), $errttl);

	// TODO success msg

	/* TODO FIXME newuser, newdb creations
	$newuser = (isset($_POST['newuser']) && ($_POST['newuser'] == true));
	$newdb   = (isset($_POST['newdb']) && ($_POST['newdb'] == true));

	if($newuser || $newdb) {
		if(!isset($_POST['dbadmuser'], $_POST['dbadmpwd'])) {
			decom_page_add_error_message(
				_('Username and password of the database admin are necessary to create a new database/user.'),
				$errttl);
		}
		else {
			// TODO
		}
	}
	// TODO
	*/
}
?>
