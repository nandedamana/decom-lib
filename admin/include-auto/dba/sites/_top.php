<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-14
 */

$skipbottom = false;

/* Files are split in order to improve readability.
 * Including via this file to void page id getting appended with parts like mk/submit.
 */
$actions = ['mk'];
if(isset($_POST['action']) && in_array($_POST['action'], ['mk']))
	include('submit/'.$_POST['action'].'.php');
?>
