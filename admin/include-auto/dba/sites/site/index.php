<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-07-02
 */

// TODO FIXME
//FIXME

require_once($DELIBDIR.'/php/site.php');
require_once($DELIBDIR.'/php/utility.php');
require_once($DELIBDIR.'/php/views/entityedit.php');

if(!isset($_GET['sitecode'])) {
	// TODO commonerr invalid access
	echo "ERR";
}
else {
	$sitecode = $_GET['sitecode'];
	// TODO add breadcrumb, set title
	
	$seid = decom_get_entity_ids_by_property_value('site', 'code', $sitecode);
	if(count($seid) != 1) {
		decom_page_add_error_message(_('Site not found or database anomaly.'));
	}
	else {
		$ntbk = new DecomNotebookView();
	
		$seid = $seid[0];
		$sobj = new DecomSite($seid);
		$docroot = $sobj->getDocumentRoot();
		$commonindex = $sobj->getPropertyValue('commonindex', true, -1);
		$hostname = $sobj->getHostName();

		/* BEGIN Details Tab */
		$tc = '';

		$tc .= '<br/>Hostname: '.nan_val_or_none($hostname);
		$tc .= '<br/>Document Root: '.nan_val_or_none($docroot);
		$tc .= '<br/>Common Index File: '.nan_val_or_none($commonindex);
		$tc .= '<br/>Navbar ID: '.nan_val_or_none($sobj->getPropertyValue('__r_navbar-sitemenu', true, -1)); // -1 means search in parents

		$tab = new DecomNotebookViewTab(_('Site Details'),
			$tc,
			$TABHEADTAG,
			'tabSiteInfo');
		$ntbk->appendTab($tab);
		unset($tc);
		
		/* END Details Tab */
		
		/* BEGIN Edit Tab */
		$v = new DecomEntityEditView('site', $seid);
		$v->setFormId('frmEdObjSite');
		$v->setFormAction('?act=dba.class.obj.ed.submit&class=site&eid='.$seid); // TODO make sure passed via post
		// TODO redirect to this page after editing

		$tab = new DecomNotebookViewTab(_('Edit Entity'),
			$v->render(),
			$TABHEADTAG,
			'tabEditEntity');
		$ntbk->appendTab($tab);
		unset($tc);
		
		/* END Edit Tab */

		/* EBGIN a2conf Tab */
		$tc = '';
		
		/* TODO FIXME testing code; show upon request only. */
		$cmd = $DELIBDIR.'/bin/a2genconf'.
			' --port 80'.
			' --delib-project-uuid '.$DEPROJUUID;
		
		if(decom_is_value($hostname) && trim($hostname) != '')
			$cmd .= ' --hostname '.escapeshellcmd($hostname);
			
		if(decom_is_value($docroot))
			$cmd .= ' --docroot '.escapeshellcmd($docroot);
			
		if(decom_is_value($commonindex) && trim($commonindex) != '')
			$cmd .= ' --indexalias '.escapeshellcmd($commonindex);

		$p = popen($cmd, 'r');
		$a2conf = '';
		
		while( ($tmp = fread($p, 4096)) )
			$a2conf .= $tmp;
		
		$a2conftxtHtml = '<p>You can copy the following text to the configuration file of this site:</p>'.
			'<textarea readonly name="a2conftxt" cols=80 rows=20>';
		$a2conftxtHtml .= htmlspecialchars($a2conf);
		$a2conftxtHtml .= '</textarea>';
		
		$tab = new DecomNotebookViewTab(_('Apache Configuration'),
			$a2conftxtHtml,
			$TABHEADTAG,
			'tabA2conf');
		$ntbk->appendTab($tab);
		unset($tc);
		
		/* END a2conf Tab */
		
		$content .= $ntbk->toHtml();
	}
}
?>
