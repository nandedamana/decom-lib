<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-06-13
 */

require_once($DELIBDIR.'/php/views/notebook.php');
require_once($DELIBDIR.'/php/views/entitylisttable.php');

$ntbk = new DecomNotebookView();

$v = new DecomEntityListTableView('site'); // TODO site
$v->setColumns(['code', 'servername']);
$tc = '<p>Adding/editing sites can be done in the Class Management section. Removing can also be done there, but be sure to delete hosts and Apache configurations manually.</p>';
$tc .= $v->render([_('Actions')], function(&$row, $kvp) {
	$manlink = '<a href="'._ua('dba.sites.site', [['sitecode', $kvp['code']]]).'">'._('Manage').'</a>';
	$row[] = $manlink; // TODO
}, ['code']);

$tab = new DecomNotebookViewTab(_('List of Sites'),
	$tc,
	'h2',
	'tabListSites');
$ntbk->appendTab($tab);
unset($tc);

/* TODO FIXME
$inputFields = ['action', 'name', 'dbhost', 'dbname', 'dbuser', 'dbpwd'];	
$inputFieldLabels = ['', _('Name'), _('Database Host'), _('Database Name'), _('Database User'), _('Database Password')];
$inputFieldTypes = ['hidden', 'text', 'text', 'text', 'text', 'text'];
$inputFieldsReq = [true, true, false, false, false, false];
$defaultValues = [
	'action' => 'mk',
	'dbhost' => '127.0.0.1',
	'dbuserhost' => 'localhost'
];
$action = '';
$method = 'POST';
$tc = '<ul><li>Name can only contain English letters except digits in the middle or at the right end. The total length shouldn\'t exceed '.$DEMAX_UNINAME.' characters.</li><li>Currently MySQL is the only database backend supported.</li><li>Please enter the details of an already existing user and database.</li></ul>'.nan_generate_form($inputFields, $inputFieldLabels, $inputFieldTypes, $inputFieldsReq, $action, $method, null, $defaultValues);

$tab = new DecomNotebookViewTab(_('Create World'),
	$tc,
	$TABHEADTAG,
	'tabCreateWorld');
$ntbk->appendTab($tab);

unset($tc);
*/
$content .= $ntbk->toHtml();
?>
