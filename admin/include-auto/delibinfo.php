<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-05
 */

require_once($DELIBDIR.'/php/test.php');
$content .= '<p><b>NOTE: </b>The status report shown here is for libdecom Control Panel only. If you are working on a project that uses libdecom, you have to call <code>delibinfo()</code> from a PHP file situated inside your project directory to get the true status of libdecom with respect to your project.</p>';
$content .= delibinfo_no_echo();
?>
