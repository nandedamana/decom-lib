<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-05-05
 * Rewrote on 2019-06-14
 */

$mnu = decom_menu_from_incdir($INCDIR_AUTO.'/'.str_replace($PAGEURLDELIM, '/', $pageid), '?'.$PAGEPARAM.'='.$pageid, $PAGEURLDELIM, false);
if($mnu)
	$content .= $mnu->toHtml();
?>
