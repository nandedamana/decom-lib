.PHONY: doc admin-locale install-admin-locale remove-admin-locale

doc:
	make -C doc
	
admin-locale:
	make -C admin/locale
	
install-admin-locale:
	make install -C admin/locale
	
remove-admin-locale:
	make remove -C admin/locale
