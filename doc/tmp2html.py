#!/usr/bin/env python
# Nandakumar Edamana
# Started on 2019-05-09

import sys

print('''<style>
.proto {
	font-family: monospace;
	font-weight: bold;
	display: block;
	padding-bottom: 0.5em;
}

.notebox {
	padding: 0.5em;
	border: 1px solid black;
}

.desc, .notebox, .retinfo {
	margin-left: 2em;
}

.paramName {
	font-weight: bold;
	display: inline-block;
	min-width: 8em;
}

body {
	padding-bottom: 1em;
}

h2 {
	padding-top: 1em;
}

hr {
	margin-top: 1.5em;
	margin-bottom: 1.5em;
	background: none;
	color: none;
	border: none;
	border-bottom: 1px solid #aaa;
}

code:not(.codeInline) {
	display: block;
	white-space: pre;
	tab-size: 4;
	margin: 0.5em;
	padding: 0.5em;
	padding-left: 0.75em;
	border: 2px solid black;
	background: #333;
	color: #fff;
	font-family: monospace;
	overflow-x: auto;
}
</style>''')

attribs = {}

while True:
	try:
		l = raw_input()
	except: # Probably EOF
		sys.exit(0)

	ls   = l.strip()
	larr = l.split(' ')
	
	if larr[0] == 'set':
		attribs[larr[1]] = ls[ (len(larr[1]) + 5) : ].strip()

	elif larr[0] == 'clearattribs':
		attribs = {}

	elif larr[0] == 'open':
		tag = larr[1]

		attrstr = ''
		for key in attribs:
			attrstr += key + '="' + attribs[key] + '" '
		
		sys.stdout.write('<' + tag + ' ' + attrstr + '>')
		attribs = {}

	elif larr[0] == 'close':
		sys.stdout.write('</' + larr[1] + '>')

	elif larr[0] == 'oclose':
		sys.stdout.write('<' + larr[1] + '/>')

	elif larr[0] == 'echo':
		txt = raw_input()
		sys.stdout.write(txt) # TODO escape special chars
