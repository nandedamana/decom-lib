# Getting Started - libdecom Documentation

By Nandakumar Edamana  
[nandakumar.org](https://nandakumar.org)

***
[Index](index.html)
***

## TODO FIXME
Changed to project system; Some of the following instructions won't work anymore. Global variables can be still set manually, but that is not recommended.

## How to use libdecom in your project
libdecom works entirely on your server. That means you have to get a clone of libdecom on your machine:
<pre>
git clone https://gitlab.com/nandedamana/decom-lib.git
</pre>

Before cloning, you have to check in which directory you are. A good choice would be your Home. A place inside your web server's Document Root is not recommended. Also, the path should be readable by your web server (make sure the permissions are appropriate).

Once the cloning is complete, you'll get a directory named *decom-lib*. You can find all the PHP, JS and CSS files inside it, usually under several subdirectories. libdecom PHP files can be directly included from your PHP files. But to make use of JS, CSS, etc., you'll have to set-up Apache Directory Aliases or something, since they have to be served directly to the client, yet they are not under the Document Root.

## How to include libdecom PHP files from your PHP files
In order to make use of libdecom API calls, you have to include libdecom PHP files in your PHP files. You can explore the libdecom codebase and do it manually, but following is the standard (and recommended) procedure.

Create a file *delocalconf.php* (name may vary) under your Document Root. The content should be:
<pre>
&lt;?php
$DELIBDIR = 'DECOM-LIB-PATH';
require_once($DELIBDIR.'/config.php');
?&gt;
</pre>
**NOTE:** *DECOM-LIB-PATH* should be replaced with the absolte path to the cloned *decom-lib* directory, say, */home/YOUR-NAME/decom-lib*

Now you can include libdecom from your PHP file like this:
<pre>
&lt;?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
</pre>

To test the above procedure works, follow this after the creation of *delocalconf.php*:

Create a file named *test.php* (name may vary) under your Document Root:
<pre>
&lt;?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/test.php");

decom_say_hello();
?&gt;
</pre>

Now visit *http://localhost/test.php* from a web browser. You should get a message *Hello, this is libdecom.*

## How to use *delibinfo()* to check your configuration
Just like PHP has [phpinfo()](https://secure.php.net/manual/en/function.phpinfo.php)
that displays information about PHP's configuration, libdecom has *delibinfo()* to do
the same. Assuming that you've cloned *decom-lib* and created *delocalconf.php* following the
above sections, this is how you should use it:
<pre>
&lt;?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
delibinfo();
?&gt;
</pre>

## How to enable database connectivity
Currently libdecom supports MySQL only. In order to get MySQL connectivity, you have to let libdecom
know the credentials. Following are the variables (constants) used for this purpose:

* $DEDBHOST
* $DEDBUNAME
* $DEDBPWD
* $DEDBNAME

As their names suggest, you have to store the MySQL host, username, password and the database name in these variables.
There is a standard procedure to do this:

Set the above-mentioned variables in a PHP files located outside the Document Root, say */home/YOUR-NAME/dedbconf.php*.
Example:

<pre>
&lt;?php
$DEDBHOST  = 'example.com';
$DEDBUNAME = 'decomuser';
$DEDBPWD   = 'PASSWORD';
$DEDBNAME  = 'dbdecom';
?&gt;
</pre>

Since you have to put your MySQL password in plaintext here, this file has to be kept safe. That's why we chose a location
outside the web server Document Root. But that might not be enough. This file should only be readable by you and the user
which runs the web server (usually *www-data*). If it is a shared hosting environment, instead of *www-data*,
you'll have to run a web server instance under your user
account or another exclusive user. There are different Apache modules to accomplish this.

### Include via *delocalconf.php*
Now that the variables are set, you can include this file in *delocalconf.php* (located directly under your Document Root,
as per our introduction),
before you include *decom-lib/config.php*. Example:
<pre>
&lt;?php
$DELIBDIR = 'DECOM-LIB-PATH';
require_once('/home/YOUR-NAME/dedbconf.php');
require_once($DELIBDIR.'/config.php');
?&gt;
</pre>

### Include via *decom-lib/dbconf.php*
There is one more recommended way to introduce these variables to libdecom, which might be
useful while you are involved in a project with multiple modules maintained by multiple people.
In such a case, you'd like to set these variables in a single place, and that's what
*decom-lib/dbconf.php* is for. This file might be missing in the original libdecom codebase,
but you can create it by yourself like this:
<pre>
&lt;?php
require_once('/home/YOUR-NAME/dedbconf.php');
?&gt;
</pre>

If you ever happen to redistribute your copy of *decom-lib* directory, make sure that you
delete this file.

## How to serve CSS from libdecom
One way to use CSS from libdecom is to read a CSS file's contents using the PHP function *file\_get\_contents()*
and embed it directly in your HTML output inside *&lt;style&gt;&lt;/style&gt;*. But this will cause bandwidth wastage when you
include the same CSS over and over as part of dynamic pages.

The solution is to include CSS using *&lt;link rel="stylesheet" href="PATH-TO-CSS-FILE"&gt;*.
But this isn't that easy since the recommended location of the directory *decom-lib* is outside your
Document Root, files in which cannot be served by the web server.

Yes, you can copy the CSS files from *decom-lib* into your Document Root manually, but that's not
recommended since the copied files might get inconsistent with libdecom updates.

Then what's the final solution? If you are using a web server like Apache, which supports Directory Aliases,
you can [create an Alias](https://httpd.apache.org/docs/2.4/mod/mod_alias.html#alias)
to point *YOUR-DOCUMENT-ROOT/css* to *decom-lib/css*. But if you are using a Unix-like
system, the easier way to do this is to create a symbolic link:
<pre>
cd YOUR-DOCUMENT-ROOT
ln -s PATH-TO-DECOM-LIB/css css
</pre>

Example:
<pre>
cd ~/www
ln -s ~/decom-lib/css css
</pre>

Now *http://YOUR-DOMAIN/css/controls.css* will serve */home/YOUR-NAME/decom-lib/css/controls.css*,
even if the target file is outside the Document Root (*~/www*).

In order to get Apache to follow symbolic links like the above one, you have to turn on the option
*FollowSymLinks* for the directory that contains the link to *decom-lib/css*. This can be done
either in the global configuration file for your Apache virtual host, on in an htaccess file
(you'll have to set the [AllowOverride Directive](https://httpd.apache.org/docs/2.4/mod/core.html#allowoverride)
in your global configuration file to get htaccess working; *AllowOverride All* can be used if you are sure).

This is how you'll set an htaccess file for this purpose (assuming you want the directory *css* linked
directly under the Document Root):
<pre>
cd YOUR-DOCUMENT-ROOT
echo "Options +FollowSymLinks" >> .htaccess
</pre>

## TODO
* $DEDBDONTCONNECT
* $DECSSBASE, $DEJSBASE
* How to serve JS, etc.
* How to use CSS, JS, etc.
* DecomPage
* Usage of each PHP file
* Usage of table.php
* Bug reporting
* License
* admin panel
