#!/usr/bin/env python2
# Nandakumar Edamana
# Started on 2019-03-19
# 2019-05-09: Started converting to an intermediate format, rather than to HTML directly.

import re
import sys

tocarea   = 'open ul\n'
bottombuf = ''

def output(code):
	global bottombuf

	# empty paragraph removal
	# The `clearattribs` command undoes previous `set` commands so that the upcoming tag is not affacted.
	bottombuf += code.replace('open p\nclose p\n', 'clearattribs\n')

if len(sys.argv) > 1:
	# print('<hr/><p class="howtoinclude">To use the resources from this module, add this line to your code:<br/><code>require_once(' + sys.argv[1] + ');</code></p>')
	output('oclose hr\n')
	output('set class howtoinclude\n')
	output('open p\necho\nTo use the resources from this module, add this line to your code:\n')
	output('oclose br\n')
	output('open code\necho\nrequire_once(' + sys.argv[1] + ');\n')
	output('close code\n')
	output('close p\n')

out  = False
desc = ''
closeCode = ''

tocUlDepth = 1 # Each section (both class and explicit @h2s) starts a UL in tocarea
wasEmptySection = True # To know whether I should close li

funcount = 0

def toc_start_section(title, domid = None):
	global tocUlDepth, wasEmptySection, tocarea
	
	if tocUlDepth > 1: # Another class is open
		tocarea += 'close ul\n'
		tocUlDepth -= 1

		if not wasEmptySection:
			tocarea += 'close li\n'

	if(domid):
		linkOpen  = 'set href #' + domid + '\nopen a\n'
		linkClose = 'close a\n'
	else:
		linkOpen  = ''
		linkClose = ''

	tocarea += 'open li\n' + linkOpen + 'echo\n' + title + '\n' + linkClose + 'open ul\n'
	tocUlDepth += 1
	wasEmptySection = True

# When the prepared desc is printed (flushed)?
# 1 - When a new /**  block starts
# 2 - When a prototype is found without a doc before it (in that case, desc will begin with `open h1 ` or `open h2 `)
# 3 - When the script terminates

for l in sys.stdin.readlines():
	ls = l.strip()
	if len(ls) > 0:
		if ls[0:3] == '/**':
			if desc != '':
				output(desc)

			desc      = 'set class desc\nopen p\n' # '<p class="desc">'
			closeCode = 'close p'                  # '</p>'
			out       = True
		
		elif (ls[-1] == '{' and ('function ' in ls or 'class ' in ls)):
			# TODO FIXME this means the doc can't have {
			
			protoraw = l[:-2].strip()

			setIdSttmt  = ''

			if('function ' in ls):
				res = re.search('^\s*function\s*([a-zA-Z0-9_]+)\s*\(', protoraw)
				if res: # XXX an else case means something wrong
					funname = res.group(1).strip()
					# I'd like to provide a permalink, but there will be duplicate names. Using funcount is all I can do now.
					funid        = 'fun_' + str(funcount) + '_' + funname
					funlinkOpen  = 'set href #' + funid + '\nopen a\n'
					funlinkClose = 'close a\n'
					setIdSttmt   = 'set id ' + funid + '\n'

					tocarea += 'open li\n' + funlinkOpen + 'echo\n' + funname + '\n' + funlinkClose + 'close li\n'
					wasEmptySection = False
					funcount += 1
			else:
				res = re.search('^\s*class\s*([a-zA-Z0-9_]+)', protoraw)
				if res: # XXX an else case means something wrong
					classname = res.group(1).strip()
					domid = 'class_' + classname
					toc_start_section(protoraw, domid)
					idcode = 'set id ' + domid + '\n'
				else:
					toc_start_section(protoraw)
					idcode = ''

				output(idcode + 'open h2\necho\n' + protoraw + '\nclose h2\n')

			proto = 'oclose hr\nset class proto\n' + setIdSttmt + 'open span\necho\n' + protoraw + '\nclose span\n'

			if len(desc) > 8 and desc[:8] == 'open h2\n':
				desc = desc + proto
			else:
				desc = proto + desc

			output(desc)
			desc = ''
				
			out = False
			
		elif out == True:
			lsd = ls[1:].strip() if ls[0] == '*' else ls

			if lsd == '/': # Closing '*/'
				desc += closeCode + '\n'
				closeCode = ''
				out = False
				
			elif len(lsd) > 0 and lsd[0] == '@':
				desc += closeCode + '\n'

				if lsd[1:4] == 'h2 ':
					desc      = 'open h2\necho\n' + lsd[3:] + '\n'
					closeCode = 'close h2'
					toc_start_section(lsd[3:])
				elif lsd[1:8] == 'notice ':
					desc      += 'set class notebox\nopen div\necho\nNOTE: '
					desc      += lsd[8:] + '\n'
					closeCode =  'close div'
				elif lsd[1:] == 'notecompoptrTF':
					desc      += 'set class notebox\nopen div\n'
					desc      += 'echo\nMake sure you use \nset href https://www.php.net/manual/en/language.operators.comparison.php\nset target _blank\nopen a\necho\n=== or !==\nclose a\necho\n for comparison with TRUE or FALSE.\n'
					closeCode =  'close div'
				elif lsd[1:7] == 'param ':
					desc += 'set class notebox\nopen div\n'
					spaceAt = lsd[7:].index(' ')
					# desc += '<span class="paramName">' + lsd[7:][:spaceAt] + '</span> ' + lsd[7:][spaceAt + 1:]
					desc += 'set class paramName\nopen span\necho\n' + lsd[7:][:spaceAt] + '\nclose span\necho\n' + lsd[7:][spaceAt + 1:] + '\n'
					closeCode = 'close div'
				elif lsd[1:22] == 'paramAttribIsNameTrue': # XXX Must be checked before paramAttrib
					desc += 'set class notebox\nopen div\nset class paramName\nopen span\necho\nattribIsName\nclose span\necho\nWhether the parameter attrib is attribute name (default: true)\n'
					closeCode = 'close div'
				elif lsd[1:12] == 'paramAttrib':
					desc += 'set class notebox\nopen div\nset class paramName\nopen span\necho\nattrib\nclose span\necho\nAttribute name or attribute ID\n'
					closeCode = 'close div'
				elif lsd[1:11] == 'reterrtrue': # XXX Must be checked before 'reterr'
					# desc += '<p class="retinfo">Returns true on success, an object of <a href="error.html">DecomError</a> on failure. Make sure you use <a href="https://www.php.net/manual/en/language.operators.comparison.php" target="_blank">=== or !==</a> for comparison with TRUE. You can also use decom_is_errobj() for error checking.</p>'
					desc += 'set class retinfo\nopen p\necho\nReturns TRUE on success, an object of \nset href error.html\nopen a\necho\nDecomError\nclose a\necho\n on failure. Make sure you use \nset href https://www.php.net/manual/en/language.operators.comparison.php\nset target _blank\nopen a\necho\n=== or !==\nclose a\necho\n for comparison with TRUE. You can also use decom_is_errobj() for error checking.\nclose p\n' # TODO can I link to decom_is_errobj()?
				elif lsd[1:7] == 'reterr':
					desc += 'set class retinfo\nopen p\necho\nReturns an object of \nset href error.html\nopen a\necho\nDecomError\nclose a\necho\n on failure. Make sure you use decom_is_errobj() for error checking.\nclose p\n' # TODO can I link to decom_is_errobj()?
				elif lsd[1:13] == 'retfalseobj ' or lsd[1:11] == 'retobjarr ':
					lsplit    = lsd[1:].split(' ')
					className = lsplit[1].strip()

					classLinks = {'DecomAttribute': 'class.html', 'DecomRelationship': 'class.html'} # TODO move to top
					if className in classLinks:
						linkOpen  = 'set href ' + classLinks[className] + '\nopen a\n'
						linkClose = 'close a\n'
					else:
						linkOpen  = ''
						linkClose = ''
					
					if lsplit[0] == 'retfalseobj':
						desc += 'set class retinfo\nopen p\necho\nReturns an object of \n' + linkOpen + 'echo\n' + className + '\n' + linkClose + 'echo\n on success, FALSE on failure. Make sure you use \nset href https://www.php.net/manual/en/language.operators.comparison.php\nset target _blank\nopen a\necho\n=== or !==\nclose a\necho\n for comparison with FALSE.\nclose p\n'
					else: # object array
						desc += 'set class retinfo\nopen p\necho\nReturns an array of objects of the class \n' + linkOpen + 'echo\n' + className + '\n' + linkClose + '.\nclose p\n'
				elif lsd[1:30] == 'retfalseifnovalsincenullisval': # Must check before retfalseifnoval
					desc += 'set class retinfo\nopen p\necho\nReturns FALSE if no value is set, NULL if the set value is NULL\nclose p\n'
				elif lsd[1:16] == 'retfalseifnoval': # Must be checked before retfalse
					desc += 'set class retinfo\nopen p\necho\nReturns FALSE if no value is found.\nclose p\n'
				elif lsd[1:9] == 'retfalse':
					desc += 'set class retinfo\nopen p\necho\nReturns FALSE on failure. Make sure you use \nset href https://www.php.net/manual/en/language.operators.comparison.php\nset target _blank\nopen a\necho\n=== or !==\nclose a\necho\n for comparison.\nclose p\n'
				elif lsd[1:15] == 'retnullifnoval':
					desc += 'set class retinfo\nopen p\necho\nReturns NULL if no value is found.\nclose p\n'
				elif lsd[1:12] == 'nonExisting':
					# desc += '<div class="notebox"><span class="paramName">nonExisting</span> Set true to show that this object is not yet present in the database and existence checks ID shouldn\'t be performed (but the class will be checked). This will also prevent database reads inside getters.</div>';
					desc += 'set class notebox\nopen div\necho\nset class paramName\nopen span\necho\nnonExisting\nclose span\necho\n Set true to show that this object is not yet present in the database and existence checks ID shouldn\'t be performed (but the class will be checked). This will also prevent database reads inside getters.\nclose div\n'

			else:
				desc += 'echo\n ' + lsd + '\n' # Space after echo is important (in case the input is like `LINE1.\nLINE2.`)

if desc != '':
	output(desc)

while tocUlDepth > 0:
	tocarea += 'close ul\n'
	tocUlDepth -= 1

tocarea = tocarea.replace('open ul\nclose ul\n', '')
sys.stdout.write(tocarea + bottombuf)
