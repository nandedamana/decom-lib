#!/usr/bin/env python
# Nandakumar Edamana
# Started on 2019-05-09

import sys

attribs  = {}
verbatim = False
tag      = None

while True:
	try:
		l = raw_input()
	except: # Probably EOF
		sys.exit(0)

	ls   = l.strip()
	larr = l.split(' ')
	
	if larr[0] == 'set':
		attribs[larr[1]] = ls[ (len(larr[1]) + 5) : ].strip()

	elif larr[0] == 'clearattribs':
		attribs = {}

	elif larr[0] == 'open':
		tag = larr[1]
		
		# TODO FIXME use attribs

		if tag == 'br':
			sys.stdout.write('\\\\') # TODO make sure works
		elif tag == 'code':
			sys.stdout.write('\\begin{verbatim}')
			verbatim = True
		elif tag == 'h2':
			sys.stdout.write('\\subsection{')
		elif tag == 'span':
			if attribs['class'] == 'paramName':
				sys.stdout.write('\\textbf{')
			elif attribs['class'] == 'proto':
				sys.stdout.write('\\bigskip\\noindent\\textbf{')
		elif tag == 'ul':
			sys.stdout.write('\\begin{itemize}\n')
		elif tag == 'li':
			sys.stdout.write('\\item ')

	elif larr[0] == 'close':
		tag = larr[1]

		if tag == 'p':
			sys.stdout.write('\n\n')
		elif tag == 'code':
			sys.stdout.write('\\end{verbatim}')
			verbatim = False
		elif tag == 'div':
			sys.stdout.write('\n\n')
		elif tag == 'h2':
			sys.stdout.write('}') # close \subsection{
		elif tag == 'span':
			if attribs['class'] == 'paramName':
				sys.stdout.write('} - ') # close \textbf
			elif attribs['class'] == 'proto':
				sys.stdout.write('}\\\\') # close \textbf
		elif tag == 'ul':
			sys.stdout.write('\\end{itemize}\n')
		
		tag     = None
		attribs = {}

	elif larr[0] == 'oclose':
		()
		#sys.stdout.write('<' + larr[1] + '/>') TODO FIXME

	elif larr[0] == 'echo':
		txt = raw_input()
		
		if verbatim == False:
			for c in ['_', '$', '{', '}']: # TODO more chars
				txt = txt.replace(c, '\\' + c)
		
		sys.stdout.write(txt.replace('&', '\&')) # TODO escape special chars other than &
