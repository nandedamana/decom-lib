#!/usr/bin/env python
# Reads a path and generates the relative path to the index page.
# XXX Not clever; just counts slashes. Avoid ./ and trailing / in the input
# Nandakumar Edamana
# Started on 2019-03-21

import sys

sc = sys.stdin.readline().count('/')
for i in range(1, sc):
	sys.stdout.write('../')

sys.stdout.write('index.html')
