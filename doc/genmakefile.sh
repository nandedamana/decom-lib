#!/bin/bash
# @description Generate temporary makefiles to be invoked by the Makefile
# Nandakumar Edamana
# Started on 2019-07-02

cp Makefile Makefile.tmp

while read f; do
	outfiles="$outfiles $f"
done < <(./lsoutfiles.sh | sort);

echo -e "fromsource: $outfiles" >> Makefile.tmp
