# libdecom Documentation Help

## Generating libdecom Documentation

libdecom has annotated source code, which means the purpose and usage of it's
files, classes, functions, etc. are written inside the source files themselves.
But that doesn't mean you have to read the source code to learn how to use
libdecom. The scripts inside the doc/ directory can automatically generate
the complete and human-readable documentation, consulting the latest source.

Change directory to doc/ and run these commands:

make clean  
make

Please note that you'll have to run the make command to regenerate the
documentation if you have pulled the source code again. It is safe to
run `make clean` before each make so that dangling files will be removed.

## HTML Documentation

Once the generation is complete, you can visit build/index.html to see the
homepage of the documentation.

## TeX Files

TeX files will be present inside build-latex, if you'd like to embed them
somewhere. Feel free to ignore if you are not interested.

## Directory Structure

Following are the main contents of the doc directory:

* Makefile
* Various executables, some directly or indirectly invoked by the Makefile
* build and build-* directories, where the generated output goes
* src/, where supporting files for the build scripts are stored

If you are not an advanced user, all you'd like to do is simply running the make
command and reading the HTML documentation, as explained in the beginning.
If you'd like to make use of the generation scripts, please refer to the
comments inside the scripts themselves to understand their usage.

## tmpfmt

Some scripts inside this files read or write tmpfmt, an intermediate format
exclusively designed for libdecom documentation generation. The only purpose of
this custom format is to make things easier. Feel free to ignore this detail if
you are not interested.
