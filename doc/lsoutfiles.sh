#!/bin/bash
# @description List the output files that will be automatically generated from the source
# Nandakumar Edamana
# Started on 2019-07-02

while read f; do
	echo "$f"|sed -E 's/\.\.\/(.*)\.php$/build\/\1\.html/'
	echo "$f"|sed -E 's/\.\.\/(.*)\.php$/build-latex\/\1\.tex/'
done < <(find ../php -name '*.php')
