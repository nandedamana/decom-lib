#!/bin/bash
# Nandakumar Edamana
# Started on 2019-03-21

echo '<style>
ul li {
	line-height: 150%;
}

/* TODO make common */
a {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}
</style>'

echo '<ul>'
echo '<li><a href="getting-started.html">Getting Started</a></li>'

while read f; do
	if [ "$(echo "$f"|grep 'php/.*/.*.html$')" ]; then
		section="$(echo "$f"|sed -E 's/php\/(.*)\/.*\.html$/\1/g')"
	else
		unset section
	fi
	
	if [ "$section" != "$prvsection" ]; then
		if [ "$prvsection" ]; then # Currently a section is open
			echo "</ul></li>"
			unset section
		fi
		
		if [ "$section" ]; then
			echo "<li>Section: $section<ul>"
		fi
	fi
	
	if [ "$(echo "$f"|grep '^\./php/.*\.html$')" ]; then
		phpfile='../'"$(echo "$f"|sed 's/\.html$/\.php/')"
		h1="$(./gettitle.sh "$phpfile")"
		echo "<li><a href=\"$f\">$h1</a></li>"
	fi
	
	prvsection="$section"
done < <(cd build && find .|sort)

if [ "$section" ]; then # Currently a section is open
	echo "</ul></li>"
fi

echo '</ul>'
