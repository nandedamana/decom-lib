#!/bin/bash
# Nandakumar Edamana
# Started on 2019-04-30

f="$1"

if [ "$2" = 'fromtmp' ]; then
	# The input arg is othe format `build-tmpfmt/php/FILE.tmp`
	phpfile=../"$(echo "$f"|sed 's/^build-tmpfmt\///'|sed 's/\.tmp$/.php/')"
else
	phpfile="$f"
fi

h1line="$(grep '^\s\*\s@h1\s' "$phpfile")"
h1="$(echo "$h1line"|sed -E 's/^\s\*\s@h1\s(.*)/\1/')"
		
if [ "$h1" ]; then
	echo "$h1"
else
	echo "$phpfile"
fi
