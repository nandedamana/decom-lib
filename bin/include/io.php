<?php
/**
 * IO Utility Functions
 * This file is part of libdecom
 * Copyright (C) 2019 Nandakumar Edamana
 */
function askyn($q) {
	echo $q.' (y/n): ';
	return (strtolower(trim(fgets(STDIN))) == 'y');
}

function getln() {
	return rtrim(fgets(STDIN));
}

function promptln($msg) {
	echo "$msg: ";
	return getln();
}

/**
 * Information messages (displayed only if not in quiet mode)
 * $debugOnly - only in verbatim mode
 */
function printinf($msg, $debugOnly = false) {
	global $quiet, $verbatim;

	if(!$quiet) {
		if(!$debugOnly || $verbatim) // If the message is debug-only, then we have to be in the verbatim mode.
			echo $msg."\n";
	}
}

/* Error messages */
function printerr($msg, $fatal = true) {
	echo 'Error: '.$msg."\n"; // TODO print to stderr
	
	if($fatal)
		exit(1);
}

function printerr_noarg($argname) {
	printerr('No value given for the command-line argument '.$argname);
}

function printerr_noval_but_quiet($paramName) {
	printerr('No value given for the parameter '.$paramName.' and I cannot ask you since we are in quiet mode.');
}
?>
